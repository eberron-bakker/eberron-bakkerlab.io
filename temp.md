# 998-05-09
## Sessie 5 - 2020-10-25

- De kalamer suggereerden een zeegraf voor de merrow.

- Het gezelschap besloot om de merrow een openluchtbegravenis te geven aan de
  kust van Breland.

- De kalamer zongen voor de dode merrow.

- Het gezelschap voer verder richting het oosten.

- Het gezelschap vond een paar grote rotsen uit het water steken dichtbij de
  kust.

- Lyvea zag een modron een grot tussen de rotsen in vliegen. De grot had als het
  ware een open plafond.

- Beau en Ionys gingen via een soort achteringang de grot in. De ingang zat
  tussen het riet, en er was een roeibootje achtergelaten bij de ingang.

- Lyvea ging richting het gat in het plafond. Een modron vond haar daar, en ze
  schakelde de modron uit. Daarna schakelde ze een tweede modron uit die haar
  vond.

- Koalinth: "Wat is er aan de hand?"

- Mens (tovenaar): "Wie verstoort mijn onderhandelingen?"

- De tovenaar is Ilad van Huis Cannith gevangen door Ionys.

## Sessie 6 - 2020-12-31

- Lyvea zag één paar benen bij de uitgang van de grot. Ze schoot richting dit
  persoon en miste. Het bleek To-Yi te zijn.

- Ilad is diplomaat naar de Conflux. Er is een territoriaal conflict.

- Hij gaf toe onderzoek te doen naar modrons.

- Ionys stormde de kamers van de arbeiders binnen. Er was allemaal
  werkgereedschap, en een uit-elkaar-gehaalde modron. Ze waren via een
  achterdeur aan het ontsnappen.

- Voorman Berdrus.

- Gevecht.

- Scherf uit grote modron. "Bedankt", en ging weg.

- Beau weg. Ilad onbewust. "Goed gesprek gehad dat slecht afliep." Heeft boek
  naast zich met lijst van namen met korte beschrijvingen. Koppen zijn
  "vrijwilligers voor Operatie [...]".

- Beau had zijn verhaal verteld aan Ilad. Ilad kende een medewerker die iets te
  maken had met Kleine Beek, de geboorteplaats van Beau.

- "Volgens mij heeft het te maken met Operatie Stopwatch."

- Cannith had een link met een ander gebied rond het Zilvermeer waar ze een
  project gaande hadden. Ilad weet niet de ins en outs van alle projecten.

- 
