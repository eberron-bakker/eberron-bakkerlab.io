# Eberron wiki(-ish)

Prerequisites:

- Hugo
- Git
- Visual Studio Code
  + Code Spell Checker extension
  + Markdown All in One extension

## Run local server

`hugo server`, then go to `localhost:1313` to preview.

Use `hugo server -d` to also view drafts.

## Publish

Push to `origin/master`, and it's published to
https://eberron-bakker.gitlab.io/.

## Create new page

`hugo new 'karakter/Character Name Here.md'` creates a page using the template
from the archetypes directory.

## Secrets

Use the following syntax to hide stuff from the readers:

```html
<!-- Hello, world! -->
```

### Hide pages

Put `draft: true` in the yaml front matter. The file won't be converted to HTML
(and thus won't be published). To preview drafted files, run `hugo server -d`.

## Format table

To format a Markdown table, press Ctrl+Shift+I.

## Add word to white list for spell check

To add a word to the white list, select it, press Ctrl+Shift+P, type "Add Word
to Workspace Dictionaries".

## Images

Add images to `static/img/`, and reference them with:

```markdown
{{< figure src="/img/Name-of-image.jpg" width="200px" >}}
```
