---
title: "Rust"
date: 2019-10-30T20:57:01+01:00
draft: false
tags:
- regels
---

## (Shorter) Gritty Realism Resting

Een *short rest* is een periode van langdurige stilstand, minstens 8 uur lang,
waarin een karakter minstens 6 uur slaapt en niet meer dan 2 uur lichte
activiteit uitvoert, zoals lezen, praten, eten of staan op wacht. Als de rust
wordt onderbroken door een periode van inspannende activiteit—ten minste 1 uur
lopen, vechten, het uitspreken van spreuken, of soortgelijke avontuurlijke
activiteiten—moeten de karakters opnieuw beginnen met de rust om er enig
voordeel uit te halen.

Een *long rest* duurt 48 uur. Je moet rusten in een plaats van comfort en
veiligheid. Je kunt lichte taken uitvoeren tijdens een long rest, en moet slapen
als normaal.

### Spell Preparation

Je kunt je *prepared spells* veranderen aan het einde van een short rest in
plaats van aan het einde van een long rest.

### Heroic Rally

De spelers kunnen kiezen om een *heroic rally* te doen. Een heroic rally duurt
24 uur. Tijdens een heroic rally duurt een *short rest* 1 uur, en een *long
rest* 8 uur, als normaal. De *long rest* geeft geen *hit dice* terug, en
ontneemt geen levels van *exhaustion*. Aan het einde van een rally krijg je één
level van *exhaustion*.
