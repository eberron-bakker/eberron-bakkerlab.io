---
title: "Journaal - Eldeen"
date: 2019-10-27T15:56:59+01:00
draft: false
toc: true
---

# 998-01-01

## Sessie 1 - 2019-10-27

- [Grinvor Bairn](/karakter/grinvor), [Donna](/karakter/jes), en
  [Bast](/karakter/bast) hebben elkaar ontmoet in
  [Riviertraan](/locatie/riviertraan) tijdens het Midwinterfeest.

- Het gezelschap heeft meegedaan aan een sneeuwballengevecht waarin ze de vlag
  van de tegenstander moesten stelen. De tegenstanders waren
  [Pinoki](/karakter/pinoki), [Fhazira](/karakter/fhazira), en
  [Adrianne](/karakter/adrianne).

- Het gezelschap heeft het sneeuwballengevecht gewonnen, en ging daarna naar Het
  Gebroken Glas om hun winst te vieren.

- Het gezelschap hoorde op de afstand een regelmatig "boem"-geluid. Toen het
  gezelschap naar buiten ging, zagen ze [Wilgspruit](/karakter/wilgspruit)---een
  ent---richting de rivier lopen vanuit het [Rivierwoud](/locatie/rivierwoud).

- Wilgspruit bleek enkel gefocust op het lopen naar de rivier. Hij antwoordde
  niet op vragen. Wilgspruit plantte zichzelf in de rivier.

- Wilgspruit leek ziek: Hij had de harde, grove bast van een oude, stervende
  boom. Hij had kleine paarsblauwe dotjes in de bladen en ledematen.

- Donna heeft *cure wounds* gebruikt op Wilgspruit, maar het hielp niets.

- [Eenpoot](/karakter/eenpoot) wist ook niet wat er mis was met Wilgspruit. Hij
  vroeg het gezelschap om hulp:

  + Iemand moest [Oalian](/karakter/oalian) in [Groenhart](/locatie/groenhart)
    waarschuwen.
  + Iemand moest het Rivierwoud in om op onderzoek uit te gaan.
  + Iemand moest naar [Varna](/locatie/varna) om een geneesmiddel te vinden van
    [Huis Jorasco](/organisatie/huis-jorasco).

- Het gezelschap koos om het Rivierwoud in te gaan. Ze bereidden zich voor om de
  volgende ochtend het bos in te trekken.

# 998-01-02

## Sessie 2 - 2019-10-31

- Gezelschap werd wakker in een Cyrisch-thematisch Gebroken Glas.

- Swift heeft afscheid gedaan van Donna en haar een maanketting gegeven.

- Pinoki verkocht een dekmantel aan Bast voor 1 gp in de ochtend.

- Gezelschap is naar Wilgspruit gegaan, en Donna kwam erachter dat er een
  Khyberscherf in Wilgspruit zit.

- Het gezelschap heeft Eenpoot ingelicht.

- Het gezelschap is het woud ingetrokken.

- Een paar uur in het woud kwam het gezelschap een beekje tegen waar ze over
  heen moesten.

- Een krokodil viel het gezelschap aan, maar het gezelschap heeft de krokodil
  gedood.

- Bast heeft touw over beekje aan twee bomen vastgemaakt, misschien voor de
  terugreis.

# 998-01-03

- Het begon stevig te sneeuwen in de ochtend.

- Het gezelschap kwam een jong bruin beertje in de sneeuw tegen, en heeft het
  beertje is achtergelaten door het gezelschap.

- ... en toch het beertje meegenomen.

- ... en toch weer neergezet nadat er berenpoten gevonden waren in de sneeuw.

- Het gezelschap zag de Kinderen des Winters door het woud heen trekken.
  [Runor](/karakter/runor), de nieuwe leider, onthoofdde de vorige leider.

- Bast merkte op dat een zwarte raaf het gezelschap al een poosje volgt.

# 998-01-04

- Het sneeuwde nog steeds. Er lag in de ochtend een pak sneeuw tot de heup.

- Het gezelschap is over een bevroren beekje gesprongen/gelopen.

# 998-01-05

- Het gezelschap was aangekomen bij de gaard. Een eland wilde hen in eerste
  instantie niet toelaten, totdat de eland Slablaadje zag zitten op Basts
  schouder.

- Er was een dichte mist in de gaard die het onmogelijk maakte om iets te zien.
  Nadat de eland opzij liep, werd de mist lichter, en zag men vuurvliegjes
  tussen de mist.

- In de gaard heeft het gezelschap het thuis van Wilgspruit gevonden. Daar
  vonden ze fijne *Khyberstof* die in de grond zat, gemengd door alcohol heen.

- Het gezelschap ontmoette [Pririla](/karakter/pririla), de wachter van de
  gaard. Pririla was geschockeerd door de kennis dat Wilgspruit vergiftigd is.
  Pririla liet het gezelschap overnachten in de boomtoppen van de gaard.

- Het gezelschap kwam tijdens hun reis af en toe een raaf tegen. Hoewel Pririla
  toegaf dat de eland van haar was, wist ze niets van de raaf. Ze maakte een gok
  dat de raaf van de Koninklijke Ogen van Aundair zou kunnen zijn.

- Pririla liet het gezelschap weten dat Wilgspruit tijdens het afgelopen
  herfstfeest drank toegediend kreeg. Bij dit feest waren meer gasten aanwezig
  dan normaal, om de vrede gezamenlijk te vieren. De volgende organisaties (met
  leiders) waren aanwezig:

  + [Wachters van het Woud](/organisatie/wachters-van-het-woud) -
    [Eenpoot](/karakter/eenpoot).
  + [Eldeen-wolven](/organisatie/eldeen-wolven) (asgebondenen) -
    [Lifra](/karakter/lifra).
  + [Stam van de schemermaan](/organisatie/stam-van-de-schemermaan) -
    [Palai](/karakter/palai).
  + Tempel van de Negen Soevereinen - [Fhazira](/karakter/fhazira).
  + [Kinderen des Winters](/organisatie/kinderen-des-winters) -
    [Frost](/karakter/frost).
  + [Poortwachters](/organisatie/poortwachters) - [Mumzok](/karakter/mumzok).

# 998-01-06

## Sessie 3 - 2019-12-01

- Het gezelschap overnachtte in de gaard.

- Kort na het wakkerworden heeft het gezelschap de gaard nog iets meer
  doorzocht. Er is geconstateerd dat:

  + Er waren geen andere plekken met Khyberscherven of Khyberstof aanwezig.
  + De drank die aan Wilgspruit gegeven was, zat in een ton die meegenomen werd
    door Eenpoot.
  + De ton was niet meer aanwezig.

- Bast zag de raaf weer in de verte, buiten de gaard.

- Pririla gebruikte *speak with animals* en Donna gebruikte *animal friendship*
  op de raaf die buiten de gaard was. De spreuken werkten allebei niet.

- Het gezelschap vertrok richting Riviertraan

# 998-01-08

- Het gezelschap kwam onderweg een gewonde goblin tegen, genaamd Zizgag (ook wel
  Zigzag). De goblin wilde zijn vriendin, Bloem, opzoeken bij de Stam van de
  Schemermaan. Hij werd echter teruggestuurd---geen bezoekers toegestaan. Hij
  moest door het moeilijke weer terugreizen naar zijn huis in het bos, en werd
  onderweg aangevallen door de Kinderen des Winters.

- Het gezelschap heeft Zizgag teruggeholpen naar zijn woonplek in het bos, een
  klein goblindorpje genaamd [Mucco Mog](/locatie/rivierwoud#mucco-mog). Het
  gezelschap werd daar ontvangen door behoedzame goblins.

- Zizgag werd geheeld door de shaman van het dorp.

- Grinvor heeft verhalen rond het vuur verteld en gezongen.

- Zizgag kwam later terug bij het gezelschap om een cadeau te geven. Hij gaf een
  spreukscherf aan Donna, maar de spreukscherf zat op slot, en hij wist het
  wachtwoord niet.

- Het gezelschap overnachtte in Mucco Mog.

# 998-01-10

- Het gezelschap kwam laat in de middag in Riviertraan aan.

- In de afstand zagen ze activiteit bij de rivier; lichtjes, een boot, en best
  wel wat volk op de brug aan de kant van Aundair.

- Ze gingen eerst bij het Gebroken Glas langs. Daar kwamen ze Swift tegen, die
  de situatie bij de rivier uitlegde: Aundair heeft mensen per boot gestuurd om
  de brug te repareren.

- Het Gebroken Glas had een dwergenthema.

- Donna heeft Rus over de drank van Eenpoot gevraagd. Zij zei dat ze de drank
  samen met hem had gemaakt. Donna vertelde toen dat er Khyberstof in de drank
  zat, en Rus riep dit lacherig door de hele herberg.

- Het gezelschap ondervroeg Bart, een bouwvakker uit Aundair die alleen in het
  Gebroken Glas zat. Hij zei dat hij een magiewerker is die goed moet stenen is.

- Het gezelschap bezocht Eenpoot bij Wilgspruit. Het ging niet veel beter met
  Wilgspruit.

- Toen ondervraagd over de drank heeft Eenpoot de ton aan het gezelschap laten
  zien. Er was geen Khyberstof in de ton.

- Eenpoot stuurde bericht per vogel aan Pririla.

- Het gezelschap dacht dat een Siberysscherf misschien zou kunnen helpen met het
  tegengaan van de Khyberscherf. Eenpoot gaf aan dat Siberysscherven duur zijn,
  en dat er waarschijnlijk geen Siberysscherven in Riviertraan zijn. Misschien
  zou Cédric d'Vadalis een Siberysscherf hebben.

- Eenpoot gaf ook aan dat de Wachters van het Woud 200 gp verschuldigd zijn aan
  Cédric. Cédric had betaald voor de helers van Huis Jorasco.

- Eenpoot stelde voor dat het gezelschap alle "verdachten" langsgaat om hen uit
  te nodigen voor het Lentefeest in Riviertraan.

## Sessie 4 - 2019-12-15

- Het gezelschap bezocht Cédric om te vragen om een Siberys-scherf. Het idee was
  dat de scherf mogelijk als tegenpool zou kunnen dienen tegen de Khyberstof.

- Terwijl ze wachtten in de woonkamer van Cédric vonden ze drakenscherven in de
  vitrinekast. Eén van de scherven was een Siberys-scherf.

- Cédric wilde de scherf niet kwijt. Scherven zijn duur, en men heeft normaliter
  een drakenmerk nodig om de scherven goed te kunnen gebruiken.

# 998-01-12

- De Wachters van het Woud keerden terug uit Varna met een halfling van Huis
  Jorasco. Het heelproces van Eenpoot begon, maar de halfling moest blijven voor
  zorg op lange termijn.

# 998-01-14

- Het gezelschap heeft een lange rust gehad.

- Eenpoot begon training van Donna en Bast in het bos. Bast leerde een simpele
  spreuk, en Donna leerde een beetje shapeshiften.

- Grinvor kreeg "training" van Pinoki. Samen met Pinoki hebben ze kattenkwaad
  uitgehaald met de bouwvakkers.

- Tijdens de training van beide groepen kwamen de Eldeen-wolven voort uit het
  Rivierwoud. De Eldeen-wolven gingen rechtstreeks naar de Tranentempel, fakkels
  in hand, om de tempel te vernietigen.

- Toen het gezelschap weerstand bad, brak er een gevecht uit tussen een van de
  wolfrijders, en Donna en Bast. De wolfrijder hakte de hand van Bast af, en
  Donna trok de wolfrijder van zijn wolf af.

- Eenpoot heelde Eenpoot en de wolfrijder.

- De Eldeen-wolven keerde zich snel van de tempel tot de bouwvakkers toen ze
  zagen dat er aan de brug gewerkt werd. Ze vernielden materieel en duwden
  bouwvakkers de rivier in.

## Sessie 5 - 2019-12-23

- [Lifra](/karakter/lifra) daagde Bast uit voor een soort duel. In het duel
  vertelde ze Bast om toe te geven dat hij voor Aundair werkt, en ze vertelde
  hem om de brug te vernietigen.

- Het duel is niet echt door iemand gewonnen. Bast hielp met het vernietigen van
  de brug.

- De Eldeen-wolven gingen over de rivier en vernietigden delen van het Aundairse
  deel van Riviertraan: De brug, de wachttoren, het materiaal voor de brug, en
  de boot waarop de bouwvakkers sliepen.

- De Eldeen-wolven trokken verder naar het oosten, Aundair in.

- Lifra was tijdens het vertrek nog steeds in bezit van Basts hand.

- Het gezelschap ruste hierna voor een poosje, en ging verder met training.

# 998-01-16

- De Aundairse Spreuk sprak over een oorlogsverklaring van Eldeen aan Aundair.
  Andere kranten spraken over een lokaal conflict.

# 998-01-18

- Donna en Bast waren aan het trainen in het Rivierwoud. Tijdens hun training
  kwamen ze Cédric tegen, die vertelde dat hij zijn knipperhond kwijt was. Als
  het gezelschap de knipperhond terug kon brengen, dan zou hij 50 gp betalen.

- Donna en Bast zijn gaan zoeken naar de knipperhond. Ze vonden de knipperhond
  in de buurt van een boom, en zagen Grinvor plots langsrijden op een eenhoorn.

- Er was een monster in de buurt van de knipperhond. Het gezelschap heeft het
  monster uitgeschakeld.

- Donna werd een hond, en wist zo de knipperhond mee te lokken richting
  Riviertraan.

- Grinvor, ondertussen, was bij Pinoki geweest. Pinoki vertelde dat het tijd was
  om naar [Thelanis](/locatie/thelanis) te gaan, een land waar het bijna altijd
  zomer is. Toen Grinvor naar buiten stapte om richting Thelanis te reizen,
  stond er een eenhoorn voor hem klaar. Met de eenhoorn trok hij het Rivierwoud
  in, totdat de winter veranderde naar de lente.

- Grinvor kwam bij een struik vol met verschillende groente en fruit. Elk stuk
  groente of fruit dat hij proefde, proefde naar een ander stuk groente of
  fruit.

- Terwijl hij bij deze struik stond, kwam er een lente-achtige elf naar hem toe.
  De elf identificeerde zichzelf as de Eerlijke [Hofnar](/karakter/hofnar), de
  opperfee van dit gedeelte van Thelanis, het Feeënhof.

- De Hofnar wilde theeleuten met Grinvor. Grinvor werd tijdelijk een theepot
  tijdens het theeleuten.

- De Hofnar zei dat hij de fee van het geluk is, en aartsvijand van het
  verdriet. Hij zocht volgelingen om het geluk te verspreiden, en vroeg Grinvor
  om een volgeling te worden.

- Grinvor vroeg of de Hofnar misschien Bast zou willen helpen met het
  terugkrijgen van zijn hand.

- Na een redelijk kort gesprek ging de Hofnar weer weg, en keerde Grinvor terug
  naar het Rivierwoud. De eenhoorn maakte plaats voor Dribbel.

# 998-01-22

- Een legerdivisie van Aundair kwam aan in Riviertraan. Een klein gedeelte van
  de divisie ging de rivier over naar de Eldeen-kant van Riviertraan. Daar
  werden ze wachters van de brug.

# 998-01-25

- Grinvor keerde terug in Riviertraan nadat hij meer dan een week geleden
  vertrok. Grinvor ervoer dit niet als een week.

- Het gezelschap ging naar Wilgspruit, die tijdelijk uit zijn winterslaap was.

- Wilgspruit wenkte het gezelschap. Toen ze hun ogen sluitten en openden, waren
  ze in een moeras, in het midden van vier grote stenen.

- De stenen kwamen tot leven, en werden vier gigantische aarde-*elementals*.

- De elementals wezen naar een bepaalde richting.

- Toen het gezelschap die kant op trok, vonden ze naar een poosje drie vervormde
  goblins (*dolgrims*): Vier armen, grijze huid, twee ruggengraten. De goblins
  waren bezig met het vernietigen van een omgehakte wilg.

- Het gezelschap versloeg de goblins, en keerde terug bij het viertal
  elementals.

- De elementals voerden een ritueel uit dat Basts gewonde arm heelde. Bast kreeg
  een magische steen als wapen.

## Sessie 6 - 2019-12-24

- Het gezelschap keerde magisch terug naar Wilgspruit bij Riviertraan.

# 998-01-28

- Na een rust vertrok het gezelschap richting Varna.

# 998-02-03

- Het gezelschap stopte kort in Roodblad.

# 998-02-07

- Het gezelschap arriveerde in Varna.

<!-- Wizard In A Bottle adventure -->

- Het gezelschap kwam naar het *Brugzijde Koffiehuis* in
  [Varna](/locations/varna).

- In het koffiehuis kwam het gezelschap [Ulfgar
  Langhout](/karakter/ulfgar-langhout) tegen. Hij was een voormalige avonturier
  die twintig jaar geleden in de "Rode Compagnie" zat. [Amilya
  Grijshart](/karakter/amilya-grijshart) was een tovenaar die onderdeel was van
  de groep. De groep ging echter uit elkaar.

- Amilya ging verder met een andere groep. Ulfgar kwam erachter dat Amilya
  waarschijnlijk dood is gegaan in een van haar avonturen. Hij vroeg het
  gezelschap om haar lijk op te halen van een tovenaarstoren.

- De tovenaarstoren was van een tovenaar die onsterfelijk wilde worden.

- Ulfgar offerde 200 gp.

# 998-02-08

- Het gezelschap vertrok richting de toren.

# 998-02-09

- Het gezelschap kwam aan bij de toren.

- Er was een ogre bij de toren. Grinvor lokte de ogre tijdelijk weg.

- Na het oplossen van een puzzel kon het gezelschap naar beneden via een
  verborgen trap.

- Het gezelschap kwam diverse obstakels tegen in de ondergrondse tunnels, en
  diverse oude lijken.

- Aan het einde van de tunnels kwam het gezelschap in een soort laboratorium.

- In het laboratorium werden er gevaarlijke potions naar het gezelschap gegooid
  totdat een van de potions---een lichtgevende groene---vernietigd werd.

- Bast vond een blauwe potion met een kleine tovenaar erin. Toen hij de potion
  ontkurkte, manifesteerde Amilya zich uit de potion.

- Amilya was zich niet bewust dat er enige tijd verlopen had, en viel het
  gezelschap meteen aan.

- Het duurde even totdat Amilya rustig werd.

## Sessie 7 - 2019-12-27

- Het gezelschap heeft tijdens het verlaten van de toren een moeilijk gevecht
  met de ogre gehad.

- De voormalige gezelschapsleden van Amilya werden buiten de toren begraven.

- Het gezelschap besloot om te overnachten voordat ze verder gingen reizen.

- Amilya wilde echter wel snel terug naar Varna om haar verloofde weer te zien.

# 998-02-11

- Het gezelschap kwam aan in Varna.

- Het gezelschap ging naar Ulfgar toe om Amilya te brengen. Ze ontvingen geld,
  en Ulfgar beloofde om op Amilya te letten.

- Het gezelschap ging naar de bank om hun geld te storten.

- Ulfgar had ervoor gezorgd dat het gezelschap kon overnachten in *De Gouden
  Appel* bij de haven.

- In het midden van de nacht kwam Ulfgar langs met slecht nieuws. Amilya was
  haar verloofde wezen opzoeken, en werd gearresteerd door de wachters. Ze zit
  nu in het gevang. Hij vroeg of het gezelschap dit voor hem op zou kunnen
  lossen.

## Sessie 8 - 2020-03-01

# 998-02-12

- Tussen dat hij in slaap viel en dat hij wakker werd, ervoer Grinvor dat hij
  in Thelanis was. Hij zag een wandelende slapende kameel met een zingende vogel
  die volgde:

  Daar het voor een dromedaris,\
  Met een mager maandsalaris,\
  Slechts een wensdroom is gebleken,\
  Weg te gaan naar verre streken,\
  Heeft ie zich maar voorgenomen,\
  Dat hij daar-is in zijn dromen.

- Grinvor zag tevens een vliegende pegasus in de verte.

- Grinvor kwam aan bij een beekje, waar hij de Hofnar zag. De Hofnar omhelsde
  een portret, en sprak stilletjes een gedicht:

  ik hou van jou, ik hou van jou\
  een dag, een maand, een eeuw\
  ik weet dat liefde lente is\
  maar ook een beetje sneeuw

- Het begon een beetje te sneeuwen.

- Iedereen was toen 's ochtends in bed.

- Het gezelschap ging naar het gevang waar ze Amilya wilden ophalen. Omdat
  Amilya niet mee wilde werken lieten ze haar nog een dagje in het gevang.

<!-- Amilya pakte dolk van Grinvor af -->

- Het gezelschap ging langs bij de klokkenwinkel van Jex. Jex wilde niets meer
  met Amilya te maken hebben. Hij heeft zijn eigen vrouw en kinderen.

- Het gezelschap ging langs bij Ulfgar, waar ze brainstormden over
  mogelijkheden. Een van de mogelijkheden was om een nieuwe avonturiergroup te
  vinden voor Amilya.

- Het gezelschap ging naar de Zwarte Bar & Gril, waar ze [Raghars
  Rijders](/organisatie/raghars-rijders) zochten. Ze vonden:

  + Shian (mens v, krijger)
  + Azif (mens m, heilige van de Negen Soevereinen)
  + Zifre (khoravar v, troubadour)
  + Nul (dubbelganger m/v, jager)

- Raghars Rijders wilde best een magiër aannemen.

- Het gezelschap heeft geld opgehaald bij de bank.

- Het gezelschap ging richting de tempel om tourist te spelen.

- Onderweg naar de tempel kwam Grinvor erachter dat hij zijn dolk kwijt is.

- Bast ging terug naar het gevang met het vermoeden dat Amilya de dolk van
  Grinvor gestolen had. Hij kon Amilya in de cel bezoeken. Toen Bast best wel
  bot tegen Amilya was viel zij hem aan met de dolk. Bast wist nipt om de dolk
  af te pakken en te verbergen.

- Donna en Grinvor bezochten [de tempel](/locatie/varna#jachtersgaard). Ze
  kwamen erachter dat---diezelfde ochtend---de pegasus er niet meer is. In
  plaats van de pegasus zwemt er nu een hippocampus in het water.

- Het gezelschap eindigde hun dag.

# 998-02-13

- Het gezelschap ging naar het gevang om Amilya te bevrijden met borg.

<!-- - Ulfgar moet Jex vragen voor goed woord bij rechtbank. -->

- Amilya is bevrijd. Ulfgar staat garant voor haar, en ze moet binnenkort
  terugkeren naar de rechtbank.

- Amilya ging mee naar Zwarte Bar & Gril. Ze kon zich aansluiten bij Raghars
  Rijders.

- Amilya is daarna naar Ulfgar gebracht.

- Het gezelschap ging terug naar de tempel.

- Donna probeerde met de hippocampus te praten. Ze kwam erachter dat de
  hippocampus een groot ego heeft, en meer water voor zichzelf wil, omdat het
  zich het belangrijkste dier in de tempel acht.

- Toen Donna de hippocampus beledigde werd ze nat gespetterd.

<!-- - Donna: "Die Hippocampus is een hoogdravende klootzak die meer water wil". -->

- Bast zag ondertussen een raaf op een tak zitten. Hij wierp meteen zijn speer
  naar de raaf, en de raaf verdween in een magisch wolkje toen het geraakt werd.

- Bast werd daarna meteen verteld om mee te komen door een wachter. Hij moest in
  een kamertje zitten. Na een poosje kwam een priester (Sara, mens v) binnen. Ze
  vroeg wat Basts intenties waren. Toen Bast zei dat hij een dalkyr wilde
  verdrijven, geloofde ze hem niet helemaal.

- De priester deed een ritueel om een spreuk uit te voeren. Ze bleek tekens te
  zien in wat botjes en plantjes en dergelijke, en keek Bast vreemd aan. Hij had
  geen slechte intenties, dus hij mocht verlaten.

- Later, nadat het gezelschap naar bed ging, was Grinvor weer in Thelanis, waar
  het nog steeds een beetje sneeuwde. Omdat hij niet zeker wist of dit wel echt
  was of niet, stak hij zichzelf in zijn been. Dit deed zeer.

- De Hofnar verscheen een keek Grinvor vreemd aan. Hij legde uit dat Grinvor in
  Thelanis is, en dat hij zichzelf niet moet steken. De Hofnar begon Grinvors
  wond te behandelen met bladeren.

- Toen Grinvor liet merken dat hij graag weer terug zou komen in Thelanis. De
  Hofnar had hiervoor een raadsel: Grinvor krijgt drie geopende flessen en twee
  kurken. In de flessen zit rottend fruit. Grinvors taak is om de stank op te
  laten houden zonder dat hij iets anders gebruikt dan de twee kurken.

- Voordat Grinvor begon met het oplossen van het raadsel ging hij eventjes aan
  de kant van de weg zitten met de Hofnar. Daar speelde hij een liedje op zijn
  panfluit, sloot zijn ogen, en was weer in zijn bedje.

- Drie flessen waren aanwezig in Grinvors kamer. Hij loste het raadsel op door
  beide kanten van de kurk te gebruiken voor twee flessen.

- Het fruit in de flessen werd plots mooi en stonk niet meer. Grinvor at een
  mango. Poef.

# 998-02-14

- Donna en Bast werden wakker. Ze wisten niet waar Grinvor was, en gingen naar
  zijn kamer. Ze zagen glasscherven op de grond, en twee flessen met fruit die
  een kurk deelden.

- Donna en Bast gingen naar de markt met de twee flessen. In een toverstafwinkel
  vroegen ze de medewerker of hij zou kunnen identificeren of de flessen
  magisch zijn. De medewerker liet weten dat het fruit magisch is, alsof het
  rollen van teleportatie zijn. De medewerker wilde graag het fruit van hen
  kopen.

- Donna en Bast verlieten de winkel. Ze aten het fruit op, en teleporteerden
  naar [Thelanis](/locatie/thelanis).

## Sessie 9 - 2020-03-14

### Dag 1

- Het gezelschap was gezamenlijk in de troonzaal van de Hofnar.

- De Hofnar zei dat er volgende week een feest is in Thelanis, en het gezelschap
  werd uitgenodigd.

- De Hofnar vroeg of het gezelschap misschien wat dingen voor hem kon doen ter
  voorbereiding van het feest.

  + Zijn slippers waren laat; of het gezelschap even kon kijken wat er met de
    slippers aan de hand is.

  + Of het gezelschap een gebraden zwijn wil vangen. Een gebraden zwijn is
    gebraden geboren en moet alleen nog aan het spit.

- Het gezelschap bevond zich in het [Hegbos](/locatie/thelanis#hegbos), een
  soort doolhof waar geen kaart van bestaat, met in het centrum het
  Gelukspaleis.

- De Hofnar vroeg ook aan het gezelschap of---als ze Bel'Ataka zien---dat ze een
  goede indruk op haar achter willen laten. Bel'Ataka is een Tairnadal-elf.

- Het gezelschap ging onderweg naar Frendamus de schoenmaker. Highlights:

  + Een knipperhond volgde het gezelschap.

  + Gezelschap kwam langs een dronken stoet van satyrs en gnomen.

  + Grinvor kwam twee elven tegen die hem juwelen wilden geven voor een nachtje.

  + Bast kocht een panter van een elf voor 100 goud. De verkoper zei dat de
    panter enige problemen heeft met andere panters.

  + Grinvor heeft een uur lang een elf uitgescholden nadat zij hierom vroeg.

  + De Wilde Jacht kwam langs, en ze wilden "de halfling" hebben. Het gezelschap
    ging een winkel in en heeft zich verdedigd tegen enkele krijgers.

## Sessie 10 - 2020-03-15

- Het gezelschap ging verder:

  + Verder onderweg begon het opeens hard te waaien. Iedereen ging naar binnen, en
    het gezelschap was in een hoerenhuis. Het gebouw werd steeds drukker en
    drukker. Zo druk dat er bijna een gevecht uitbrak.

  + Na een paar uren durfde het gezelschap naar buiten.

  + Het gezelschap kwam een groep dieven tegen die een tijger van jade
    probeerden te ontvoeren. Een van de elven probeerde Bast een robijn de
    handen in te drukken met "jullie hebben hier niets gezien". Het gezelschap
    probeerde om de tijger te redden, maar faalde.

  + Het gezelschap kwamen twee ruziemakende pixies tegen. Hun kar was
    omgevallen, en ze gaven elkaar de schuld. Donna zette de kar recht, en het
    probleem was opgelost. Tegen betaling van een paar noten kreeg het
    gezelschap een bessenboombes.

- Het gezelschap kwam aan bij de schoenenwinkel. Het slot van de voordeur was
  weggesmolten.

- Binnen in de schoenenwinkel was een huilende gnoom---Frendamus. Verder was de
  hele schoenenwinkel over de kop gehaald.

- Het gezelschap ruimde de schoenenwinkel op.

- Frendamus hield niet echt op met huilen. Echter werd hij wel door Donna
  geïntimideerd om aan de gang te gaan met nieuwe slippers voor de Hofnar.

- Het gezelschap overnachtte in de schoenenwinkel.

### Dag 2

- Frendamus was bang dat het een maand zou duren voordat de slippers klaar
  zouden zijn.

- De reden dat Frendamus zo bang is, zei hij, is dat de Hofnar de winter
  veroorzaakt als hij verdrietig is.

- Het gezelschap ging buiten zoeken naar de dief. Bij het navragen bij de buren
  werden ze een bepaalde richting opgestuurd. Niet lang daarna kwam het
  gezelschap aan bij een leegstaand warenhuis. De ramen waren dichtgetimmerd.

- Grinvor ging het warenhuis naar binnen. Achter in het warenhuis vond hij een
  dansende elf met roze schoenen. De voeten van de elf bloedden.

- De elf (genaamd Lydia) kreeg de slippers niet uit. Ze gaf toe dat ze de
  slippers betoverd had omdat ze de Hofnar een lesje wilde leren---de Hofnar had
  haar Hippocampus gestolen. De betovering was echter mislukt.

- Het gezelschap nam de elf mee naar de bakker. Daar hebben ze boter gekocht, en
  met de boter kregen ze de slippers van de voeten van de elf.

## Sessie 11 - 2020-04-05

- Het gezelschap ging bij Frendamus langs om de slippers te laten repareren.

- Onderweg naar het paleis kwamen ze het zwijn tegen. Het zwijn rende een
  dierenasiel naar binnen. De eigenaar van het asiel wil echter niet dat het
  zwijn wordt opgegeten tijdens het feest.

- Het gezelschap ging samen met het zwijn en de eigenaar van het asiel naar het
  paleis om de Hofnar te overtuigen dat het feest gevierd kan worden zonder het
  eten van het zwijn.

- Grinvor ontmoette Dribbel bij het paleis.

- Het gezelschap kwam aan bij de troonzaal van de Hofnar. Het gezelschap wilde
  een privé-audiëntie. In de slaapkamer van de Hofnar maakte het gezelschap een
  paar voorstellen:

  + Het gebraden zwijn moet de mascotte worden van het feest, in plaats van
    opgegeten worden.

  + Lydia naar Eldeen sturen met de pegasus en hippocampus.

- De Hofnar ging akkoord met de voorstellen.

- Later begon het Dribbel-Dribbel Dansfeest.

- Een opperfee genaamd Leylandra ging naar Bast toe---de opperfee van de oorlog.

- Leylandra leidde Bast naar Zang, een voormalige kennis van Bast. Ze waren
  allebei [strijdgestegenen](/organisatie/strijdgestegenen).

- Zang dacht dat Eberron vernietigd was door de Rouw. Op de dag van de Rouw
  waren de strijdgestegenen naar Thelanis gebracht, behalve Bast. Nadat Zang
  erachter kwam dat Aundair nog steeds bestaat, voelde ze dat de missie van de
  strijdgestegenen niet voltooid is. Ze wilde terug naar Eberron om de oorlog
  tegen Aundair af te maken.

- Bast vertelde Zang dat ze hem kan bereiken via Eenpoot in Riviertraan.

- Opeens kwam Bel'Ataka binnen, en daagde *n'importe qui* uit voor een duel.

- Bast accepteerde de uitdaging van Bel'Ataka. Hij verloor het duel.

- De Hofnar trok wit weg en vertrok naar de slaapkamer. De sneeuw buiten werd
  een ware sneeuwstorm. De Hofnar ondervond een metamorfose naar een koude
  winterfee.

- Niks kon de Hofnar troosten. De Hofnar vroeg Grinvor om mensen blij te maken,
  en stuurde het gezelschap naar Groenhart.

# 998-03-21

## Sessie 12 - 2020-04-25

- Het gezelschap kwam aan bij een grote mammoetboom genaamd Strar.

- Het gezelschap kwam een centaur genaamd Ĉevalo tegen. De stokoude centaur was
  bezig met het terugbrengen van een drakenhavik-ei naar een nest. De centaur
  wilde het gezelschap best helpen om richting Groenhart te reizen, maar moest
  eerst zijn missie voltooien.

- Het gezelschap hielp Ĉevalo. Bast gebruikte zijn staf van vogelgeluiden om de
  drakenhavik te lokken, en iedereen verstopte zich op afstand.

- De drakenhavik nam het ei en vloog weer weg.

- Het gezelschap ging op reis naar Groenhart samen met Ĉevalo. Ĉevalo was een
  slome reiziger, en het duurde een week. Onderweg heeft het gezelschap verhalen
  verteld.

- Het gezelschap doopte zichzelf als de "Wilgenwachters".

<!-- - Bast mist een amulet met een afbeelding van een bloem. Is blijven liggen in
  een baraks in Aundair. Doorzichtig geel. -->

# 998-03-26

- Het gezelschap kwam aan in Groenhart. Ĉevalo bracht hen naar het boomhuis van
  Faefine. Faefine is het centrum van de khoravar-gemeenschap in Groenhart.

- Het gezelschap kreeg toestemming om te overnachten bij het boomhuis van
  Faefine.

## Sessie 13 - 2020-05-02

- Een khoravar genaamd [Leselor](/karakter/leselor) kwam bij het gezelschap aan
  tafel zitten. Ze kende Adrianne. Ze stelde voor om het gezelschap rond te
  leiden tijdens het Lentefeest.

- Voordat het donker werd ging het gezelschap even naar buiten. Donna ging
  richting Oalian, waar ze langzaam probeerde te communiceren met de oerdruïde.
  Oalian communiceerde in Sylvan met bewegingen, het ritselen van bladeren in de
  wind, et cetera.

- Donna kwam erachter dat Oalian Mumzok als een vriend beschouwt, en dat men
  Mumzok 8 uur richting het westen kan vinden.

# 998-03-27

- Het gezelschap vertrok richting Mumzok.

- Tijdens hun reis kwamen ze Ĉevalo tegen, die liet weten dat er een ontmoeting
  is geregeld met Oalian. 's Ochtends op de dag na het feest.

- Het gezelschap kwam aan bij een stenen cirkel. Ze werden gestopt door een
  halfling, Anita, en later ontvangen door Mumzok.

- Het gezelschap werd uitgenodigd om hun verhaal te doen. Mumzok wilde graag
  Wilgspruit helpen, maar moest eerst een ritueel uitvoeren om de poortzegel te
  vernieuwen.

- Het gezelschap overnachtte om de volgende ochtend het ritueel uit te voeren.

# 998-03-28

- Het gezelschap voerde het ritueel uit. Tijdens het ritueel kwamen er enkele
  dolgrims vrij, en een vliegend hoofd. Het gezelschap versloeg deze monsters
  van Khyber.

- Het gezelschap vertrok richting Groenhart na een korte rust.

## Sessie 14 - 2020-05-10

- Bij aankomst suggereerde Leselor dat het gezelschap meedoet met bloemengroeien.
  Het bloemengroeien had ook een jurylid nodig, en slablaadje werd genomineerd.

# 998-04-01

- De dag van het lentefeest.

- Het gezelschap deed mee met bloemengroeien. Donna won, en kreeg een bag of
  holding.

- Het gezelschap deed mee met een potje hrazhak tegen een groep
  luipaard-shifters. Het gezelschap won.

## Sessie 15 - 2020-05-24

- Grinvor ontving schoenen van snelheid na afloop van het potje hrazhak.

- Het gezelschap deed mee aan een regendans onder leiding van Huis Lyrandar.
  Grinvor en Donna stalen de show.

- Het begon met regenen, en Slablaadje was blij met de regen.

- Na afloop van de dans kwam een jonge man richting de groep om hulp. Hij deed
  aan verrassingszoeken met zijn vriendin, en hij had zeer cryptische hints
  gekregen richting zijn verrassing: Bij het watergevecht, maar niet bij de
  koning die failliet is.

- De oplossing van de pussel was: Aquarel (aqua-rel, watergevecht). Ze vonden de
  vriendin van de man aan het schilderen.

- Het gezelschap kreeg een magische veer als beloning voor hun hulp.

- Het gezelschap ging uitgebreid eten. Leselor gaf het gezelschap een robijn.

# 998-04-02

- De Wilgenwachters gingen 's ochtends vroeg naar Oalian.

  + Grinvor werd geheeld door Oalian.

  + Grinvor werd erelid van de Wachters van het Woud.

  + Donna kreeg de gave om te teleporteren via bomen.

  + Bast kreeg de gave om effectief een boom te worden.

  + Het gezelschap kreeg 5000 soevereinen voor de reis richting Riviertraan, om
    Wilgspruit te helen en het Zomerfeest bij te wonen. Daarna terug naar
    Groenhart.

- Donna---of Jes---vertelde aan haar vrienden dat ze een dubbelganger is.

- Leselor gaf de Wilgenwachters een cheque om per boot te reizen richting
  Riviertraan.

## Sessie 16 - 2020-06-28

# 998-04-03

- Leselor vertrok vroeg de boomhut van Faefine. Een raaf landde op haar hand.
  Bast zag dit.

- Het gezelschap achtervolgde Leselor, en hield haar tegen. Leselor ontkende
  alle beschuldigingen dat ze een spion van Aundair was.

- Leselor probeerde een spreuk te doen, en werd meteen gestopt door Bast, die
  haar vastzette in wortels.

- Leselor werd meegenomen naar Oalian. Ze werd gevangen gezet.

- Het gezelschap vertrok richting Delethorn.

# 998-04-11

- Het gezelschap kwam aan in Delethorn.

- Grinvor nam Fee en Dribbel mee om vis te eten.

- Het gezelschap las in de Korranberg Kroniek:

  + Koningin Aurala van Aundair zegt dat de religieuze vrijheid van de inwoners
    van Thaliost gegarandeerd moet worden. Thrane ontkent onderdrukking.

  + Een lid van ir'Qarin is gearresteerd in Groenhart, maar de omstandigheden
    zijn onbekend.

  + De brug in Riviertraan is zo goed als af.

  + De Eldeen-wolven hebben de tempel naar de Negen Soevereinen in Wyr
    afgebrand.

  + Een opiniestuk over de Heer der Zwaarden in het Rouwland.

# 998-04-12

- Het gezelschap vertrok per schip richting Varna.

# 998-04-15

- Het gezelschap kwam aan in Varna, en reisde door naar Riviertraan.

# 998-04-16

- Het gezelschap kwam aan in Riviertraan.

- Het gezelschap zag dat de brug volledig gerepareerd is.

- Het gezelschap ging meteen naar Wilgspruit. Wilgspruit was stabiel, maar zwak.

- Mumzok speculeerde over de Khyberscherven in Wilgspruit. Hij was ervan
  overtuigd dat de Khyberscherven magisch versterkt waren, en dat een
  soortgelijk versterkte Eberronscherf hem zou kunnen helpen. Mumzok wist alleen
  niet hoe men een scherf magisch versterkt.

- Eenpoot vertelde aan het gezelschap dat iedereen bij het komende Zomerfeest
  komt, met uitzondering op Palai van de Schemermaan. De shifterstam was
  volledig in isolatie geraakt.

- De Ridders Arcane waren aanwezig aan de andere kant van de rivier. Dit zijn
  elitetroepen van Aundair, geleid door Heer Darro.

- Eenpoot liet ook weten dat er Wachters van het Woud in het Rivierwoud
  schuilen.

## Sessie 17 - 2020-07-19

- Het gezelschap ging naar Pinoki om te praten over de Hofnar. Pinoki
  suggereerde dat het misschien slim zou kunnen zijn om Bel'Ataka te overtuigen
  van de kracht van de Hofnar.

- Pinoki is de "leider" van oost-Riviertraan. Hij zei dat hij niets kan doen om
  Aundairse troepen te verwijderen uit het dorp.

- Donna schreef een brief aan Pinoki na het bezoek: Een vraag aan hem om alle
  inwoners van Riviertraan te overtuigen om Wilgspruit te beschermen tegen elke
  aanval.

- Het gezelschap ging bij Cédric langs om hem terug te betalen. Cédric liet hen
  velociraptors zien.

- Wilgspruit vroeg Eenpoot om een identiteitsbewijs. Hij moest daar een dag voor
  wachten.

- Donna teleporteerde Wilgspruit richting Oalian. Het gezelschap was bang dat
  Wilgspruit misschien aangevallen zou kunnen worden door Aundair.

- Het gezelschap overnachtte bij de ouders van Donna.

# 998-04-17

- Wilgspruit ontving een identiteitsbewijs van Eenpoot.

- Fhazira en Heer Darro leidde de brugopeningsceremonie.

- Tijdens Heer Darro's speech ontplofte de brug. Meerdere ridders werden
  weggespoelt door de rivier. Heer Darro gaf meteen het bevel om aan te vallen.

  + Net voor de ontploffing hoorde Bast een vogelgeluidje, en zag Signaal---een
    strijdgestegene---zijn duim omhoog steken.

- Een zware strijd volgde tussen Eldeen en Aundair.

  + Eenpoot vernielde "per ongeluk" een gedeelte van de Tranentempel.

  + De Eldeen-wolven vielen van de andere kant aan.

  + De ridders van Aundair werden constant het water in gestoot.

  + Aundair trok uiteindelijk terug.

  + De Eldeen-wolven trokken ook terug.

## Sessie 18 - 2020-07-25

- Het gezelschap heelde zichzelf na het gevecht.

- Donna stuurde een bericht naar Oalian per Sivis-kiosk:

  + "Strijd geweest in Riviertraan. Brug stuk en gespannen sfeer. Stuur
    vertegenwoordiger naar Wilgspruit zijn grove."

- Een journalist van de Kroniek van Korranberg interviewde het gezelschap over
  het conflict.

  + Aundair heeft Eldeen aangevallen.

  + Er leek een fout te zijn met de brug. Onduidelijk wat er is gebeurd. Het
    leek alsof de overkant gelijk de aanname nam dat het een aanval was van de
    Wachters. Geen van de Wachters heeft een aanval gepland op de brug.

  + De ent (Wilgspruit) is veilig. Gezien de spanningen is de ent vooraf in
    veiligheid gebracht, omdat de veiligheid niet gewaarborgd kon worden.

  + De Wachters weten niet waarom de brug opgeblazen is.

  + De soldaten van Aundair hadden geteleporteerd naar de zijkant van de tempel.
    Ze leken een aanval te plannen op de tempel. Als verdediging werden de
    soldaten het water in geblazen, maar per ongeluk schade aan de tempel.

  + Aundair was stellig op de overname van de Eldeenzijde van Riviertraan.

  + De Wachters weten niet of er burgerslachtoffers zijn geweest aan de kant van
    Aundair. Zoja, dan betreuren de Wachters dan.

  + De Wachters gedogen het vernietigen van een tempel in Wyr door de
    Eldeen-wolven niet.

  + De Wachters beschouwen het conflict als een provocatie van Aundair.

  + De Wachters hopen geen verdere actie te hoeven nemen. Ze willen de vrede
    bewaren.

  + De Wachters zullen niet de grens van Aundair over om een gevecht aan te
    gaan.

  + De Wachters zullen---net als jaren geleden---opkomen als Aundair het
    conflict escaleert.

  + De Wachters hebben geen contacten met andere naties.

  + De Wachters doen een oproep om de vrede te bewaren, om als één volk naast
    elkaar te kunnen bestaan. Ze vragen om solidariteit van de lezers voor de
    vrijheid die aan ieder toebehoort.

  + De Wachters van de Woud hebben tijdens de strijd de soevereiniteit van
    Aundairs grondgebied gerespecteerd.

- Een "vlotte boodschapper" kwam bij het gezelschap langs. De mechanische
  libelle had dit bericht: "Ontmoet in grot in Rivierwoud. Bevestig bericht."
  Het gezelschap bevestigde het bericht.

- Het gezelschap overtuigde Cédric om tijdelijk te zorgen voor een gevallen
  drakenhavik. Hij vroeg 2500 soevereinen, en een dier met gelijke waarde als
  Fee.

- Het gezelschap wilde een gevangenenruil doen met Aundair. Fhazira probeerde
  dit te onderhanhandelen, maar faalde. Daarna is het één van de Ridders Arcane
  gelukt om te onderhandelen.

  + Heer Darro wilde 12 Ridders Arcane ruilen voor 6 Eldeen-wolven, maar wilde
    de wolven zelf slachten.

- Net na de gevangenenruil ging Donna naar de overkant. Daar vond ze één wolf in
  de paardenstal. De bevrijdde de wolf, maar werd geraakt door een speer toen ze
  wegvloog.

# 998-04-18

- Het gezelschap ging op reis richting de grot. Mumzok en de goblins gingen mee,
  omdat Mumzok richting de gaarde van Wilgspruit wilde reizen.

- Het gezelschap stopte in Mucco Mog. De goblins brachten hun gesneuvelde
  kameraden mee naar dit goblindorp.

- Zizgag gaf Donna een blauwe ring. Donna wilde de ring eerst niet aannemen,
  maar beloofde om het te geven aan Bloem. <!-- ring of swimming -->

- Het gezelschap overnachtte in Mucco Mog.

# 998-04-19

- Het gezelschap reisde langs een beekje. Ze kwamen drie kreeftmonsters tegen
  die een krokodil aan het aanvallen waren. Het gezelschap won een gevecht tegen
  de kreeftmonsters, en bevrijdde de krokodil.

  + Het gezelschap had de indruk dat de kreeftmonsters monsters van Khyber
    waren.

## Sessie 19 - 2020-07-30

- Het gezelschap kwam aan bij de grot van de strijdgestegenen. De Eldeen-wolven
  en de strijdgestegenen leken de grot tijdelijk te delen.

- In het midden van de grot zag het gezelschap drie personen rond een tafel:
  Muiter, Bel'Ataka, en Lifra.

- De drie leiders kondigden aan dat ze een oorlog willen beginnen tegen Aundair,
  en dat ze de oorlog willen winnen.

- Ze noemden Thrane als mogelijke "bondgenoot" in de strijd tegen Aundair. Geen
  enkel van de aanwezige leiders had enige liefde voor Thrane, maar als vijanden
  tegen vijanden strijden, dan is dat een gewin. Ze suggereerden dat de
  spanningen in Thaliost gebruikt zouden kunnen worden om een oorlog uit te
  lokken.

- Bel'Ataka suggereerde dat ze bondgenoten zou kunnen vinden onder de Tairnadal
  van Valenar. Ze is een legende onder haar bevolking. Ze heeft samen met twaalf
  grifioenriders de hoofdstad van Aerenal verdedigd tegen drie draken.

- Leylandra, de opperfee van Bel'Ataka en Muiter, is de opperfee van overwinning
  onder onmogelijke omstandigheden.

- Bel'Ataka en de strijdgestegenen zijn gearriveerd op Eberron via een gaarde
  van Groenzangers in het Zangwoud. Lifra was al enige tijd in contact met deze
  Groenzangers.

- Bel'Ataka is een hoge fee---een stap tussen een gewone fee en een opperfee.
  Hoge feeën komen normaal niet in Eberron.

- Het plan van de partijen was dit:

  + Bel'Ataka gaat naar Valenar om Tairnadal te verzamelen.
  + De Eldeen-wolven blijven op de grens tussen Eldeen en Aundair.
  + De strijdgestegenen gaan naar Thaliost om daar onrust te veroorzaken.

- Bel'Ataka vroeg aan Grinvor of hij haar legendes zou willen verspreiden.
  Grinvor wilde dit wel doen, maar als tegenprestatie wilde hij dat Bel'Ataka de
  Hofnar blijmaakt.

- Grinvor beloofde:

  + Hij zal de bewoners van Khorvaire vertellen van de legendes van Bel'Ataka.
  + Hij zal zijn best doen in de strijd tegen Aundair. Deze belofte eindigt als
    Koningin Aurala geen koningin meer is.

- Bel'Ataka beloofde dat ze haar best zal doen om de Hofnar op te vrolijker.

- Bel'Ataka zou ook zien of ze Fee weer mee kan nemen uit Thelanis. Bast gaf een
  takje aan Bel'Ataka om haar te helpen met het lokken van Fee.

- De Wilgenwachters maakten de volgende plannen:

  + Drakenhavik trainen.

  + Naar Arcanix gaan om de Eberronscherf magisch te laten bewerken.

  + Naar Passage, en met de trein naar Thaliost.

  + Drakenhavik terug naar Riviertraan sturen vanuit Passage.

## Sessie 20 - 2020-08-08

# 998-04-22

- Het gezelschap kwam terug in Riviertraan.

- Bij aankomst las het gezelschap een krantenartikel over de gebeurtenissen in
  Riviertraan.

- Het gezelschap had een ontmoeting met Faena Grauwmorgen, de rechterhand van
  Oalian die als diplomaat richting Riviertraan gereisd is.

- Faena was van plan om de volgende dingen te doen:

  + Eldeense troepen naar de grens halen.

  + Varna overtuigen om in solidariteit met Eldeen te blijven.

  + Contact leggen met Thrane voor een mogelijke alliantie.

  + Aundair dreigen met een handelsembargo.

- Faena wilde ook eisen richting Aundair opstellen:

  + Officiële verklaring dat de onafhankelijkheid van Eldeen gerespecteerd
    wordt.

  + Demilitarisatie van het grensgebied.

  + Staken met de bouw van de brug.

# 998-04-23

- Donna begon met de training van de drakenhavik.

- Grinvor begon optredens door Riviertraan om geld bij te verdienen.

- Bast begon voorbereidingen om een uilbeer te vangen in het Rivierwoud.

# 998-04-27

- Bast ving een uilbeer en bracht de uilbeer naar Cédric.

# 998-05-01

- Einde van de training van de drakenhavik.

# 998-05-02

- Het gezelschap begon haar reis richting Varna, en dan met de boot naar
  Arcanix.

# 998-05-11

- Aankomst in Arcanix.

- Het gezelschap ging De Vrolijke Hippogrief binnen, en vond daar Paulus, een
  voormalige inwoner van Riviertraan.

- Paulus was niet zo blij. Zijn vriendin, Diana, antwoordde al enige weken niet
  op zijn brieven. Er waren geruchten dat ze verliefd was op Heer Garius Lagan,
  een Ridder Arcane en veteraan van de oorlog.

- Diana is de dochter van [Adal](/karakter/adal), Eerste Generaal van Aundair.

- Paulus had plots een idee, en ging spoedig weg.
  
- Eenmaal buiten zag het gezelschap een ridder op een fantoompaard wegvliegen
  met een vrouw.

## Sessie 21 - 2020-08-09

- Het gezelschap volgde Paulus naar zijn schuurtje in Arcanix. Hij was daar
  bezig met het werken aan een magisch brouwseltje. Toen het gezelschap op de
  deur klopte schrok Paulus, en dronk hij per ongeluk van het drankje.

- Kort na het drinken van zijn brouwsel begon Paulus met groeien. Hij zei dat
  het drankje hem groter en sterker zou moeten maken, om imposanter over te
  komen en indruk te maken op Diana. Hij had ingrediënten voor het drankje
  gekregen van Madam Wortel in het woud.

- Paulus begon toen heel snel te groeien, en rende richting de torens van
  Arcanix.

- Het gezelschap vond in het schuurtje van Paulus wat documentatie: Hij had
  ingrediënten verzameld van Professor Abalaba (gnoom, v) en van Madam Wortel.

- Het gezelschap volgde de aanwijzingen richting Madam Wortel om een "tegengif"
  te vinden.

- In het woud werd het gezelschap aangesproken door een feeëngeest. De
  feeëngeest (Libelle) wilde een verhaal van eenzaamheid van ieder. Toen de
  verhalen verteld waren, leidde de feeëngeest het gezelschap naar Madam Wortel.

- Madam Wortel was een dryade die voor het gezelschap een nieuw brouwseltje
  maakte.

- Het maken van het brouwseltje ging enigszins mis, en het gezelschap moest
  vechten tegen een tot-leven-gekomen blob.

- Toen de blob neer was, had het gezelschap het tegengif voor Paulus.

- Terug in Arcanix: Paulus was een reus die probeerde op één van de torens te
  klimmen. Garius was aan het proberen om de reus te bevechten.

- Donna gooide wat tegengif in de mond van Paulus. Hij kromp zeer snel terug
  naar normale grootte.

- Toen Paulus kromp, sloeg hij Garius uit de lucht van zijn vliegende
  fantoompaard. Garius' helm viel af, en het bleek dat hij een strijdgesmedene
  was.

- Toen het gezelschap Paulus terug naar zijn schuurtje bracht, zagen ze Garius
  langsvliegen met Diana achterop zijn fantoompaard. Donna ging er meteen
  achterna, en ze landden allebei ietsjes verderop.

- Garius liep richting Donna terwijl Donna riep dat Garius nep was, dat hij een
  strijdgesmedene was.

- Een gevecht ontstond, en het gezelschap won.

- Het gezelschap bracht iedereen richting de schuur van Paulus.

## Sessie 22 - 2020-08-16

- Paulus werd gearresteerd door de wacht.

- Voordat de wacht kon binnentreden, gaf Donna een krimpdrankje aan Garius.
  Garius werd snel heel klein.

- Het gezelschap stelde vragen aan Garius' schildknaap, Tom. Tom wist niet dat
  Garius een strijdgesmedene was. Hij wist enkel dat Garius ernstig verwond werd
  in een strijd tegen Thrane.

- "Garius" gaf toe dat hij niet Garius was. Zijn naam was Wachter. Hij was
  aanwezig bij de strijd tegen Thrane als heler and soldaat. Toen Garius ernstig
  verwond werd, was Garius bang dat het moreel bij de troepen weg zou vallen.
  Dus Garius vroeg aan Wachter om zijn bepantsering aan te doen en in zijn
  plaats te vechten. De laatste twee jaar ging Wachter als mens door het leven,
  en hij ervoer dit als bevrijdend. Wachter had Garius' dagboek, en wist dat
  Garius een oogje had op Diana. Om Garius zo goed mogelijk te emuleren,
  probeerde hij Diana te verlijden.

- Wachter/Garius vroeg Diana om een herkansing. Diana sloeg hem met een plank
  bewusteloos.

- Bast naam Wachter mee naar het woud en hielp hem tot bewustzijn. Hij
  overtuigde hem om naar Groenhart te reizen voor een beter, vrijer bestaan.
  Donna teleporteerde Wachter naar Groenhart.

- Ondertussen kwamen de wachters terug naar de hut om Garius op te halen.
  Grinvor was alleen aanwezig, en hij deed alsof hij van niets wist. Grinvor
  werd opgepakt en in een cel gestopt. Na een paar uur ondervraging kwam hij
  vrij.

- Het gezelschap overnachtte in de Vrolijke Hippogrief.

# 998-05-12

- Het gezelschap---sans Bast---reisde naar het vliegende kasteel Nocturnas voor
  een ontmoeting met [Adal](/karakter/adal). De ontmoeting werd geregeld door
  Diana.

- Diana bracht het gezelschap naar een voorkamer. Adal liet het gezelschap later
  zijn kantoor binnen.

- Donna vroeg Adal om hulp met de Eberronscherf. Toen ze de Khyberstof liet
  zien, zag ze een kleine vorm van verbazing in het starre gezicht van Adal.
  Donna zei dat het de bedoeling was om Wilgspruit te helpen.

- Adal was bereid om te helpen: Hoe sneller de problemen in Eldeen opgelost
  worden, hoe beter. Adal zei echter dat het zeer veel zou kosten. Omdat hij
  wist dat het gezelschap niet genoeg geld zou hebben om de onkosten te betalen,
  vroeg hij om hulp met een ander probleem.

- Adal legde uit dat er een aantal spreuken uit de hand zijn gelopen in een
  laboratorium. De spreuken moeten verslagen worden. Als het gezelschap Adal
  hiermee helpt, dan wilde hij de Eberronscherf wel bewerken.

- Donna vroeg of Bast zou mogen komen, op voorwaarde dat er niets met Bast zou
  gebeuren. Adal stemde toe, en teleporteerde Bast naar het kasteel.

## Sessie 23 - 2020-08-23

- Het gezelschap ging naar het laboratorium met hulp van drie 4e-jaars
  studenten.

- Toen de deur openging, vond het gezelschap enkel chaos in het laboratorium.
  Vier levende spreuken waren het laboratorium langzaam aan het vernietigen. De
  spreuken waren gekarakteriseerd door:

  + Vuur.
  + Water (helen).
  + Bliksem.
  + Donder.

- Na het gevecht met de spreuken vond het gezelschap notities in een la. De
  notities geven informatie over de militaire effectiviteit van deze levende
  spreuken tegen de troepen en magie van Thrane.

## Sessie 24 - 2020-09-27

- Het gezelschap vond nieuwe (magische) voorwerpen in het laboratorium:

  + Ring van Afwijking.
  + Onderzoek naar het creëren van levende spreuken.
  + Adelaarsbril.
  + Staf van Kiezelstenen.
  + Dolk van de Lafaard.
  + Sending Stones.

- Het gezelschap keerde terug naar Adal. Adal zei dat hij binnen een week een
  bewerkte Eberronscherf zou hebben voor het gezelschap.

- Adal gaf de Wilgenwachters "gangpassen" voor vrije toegang tot de faciliteiten
  van Arcanix voor anderhalve week.

- Het gezelschap ging naar de bibliotheek om onderzoek te doen.

  + Grinvor zocht naar Bel'Ataka, maar vond eigenlijk niets.
  + Bast zocht naar nieuws in het krantenhoekje. Bast leerde dat Aundair
    begonnen is met de reparaties van de brug in Riviertraan.

- Het gezelschap ging naar Professor Abalaba in Amberwall. Donna vroeg naar
  nabije druïdische gaardes. Abalaba wees richting de Oranjegaarde, zo'n 100
  kilometer het Eldritchwoud in. Het oranjewoud is een gaarde vol
  bloedsinaasappels, maar de druïdes die daar ooit woonden zijn daar al een
  lange tijd niet meer.

- Professor Abalaba vroeg het gezelschap om de gaarde op te zoeken en nieuws (en
  een bloedsinaasappel) terug te brengen.

- Professor Abalaba zei dat het gezelschap mogelijk meer over de gaarde zou
  kunnen leren in een herberg iets buiten Arcanix, de Citrinevonk. Abalaba wilde
  daar echter zelf niet naartoe gaan. De Citrinevonk staat bekend om honingwijn
  waardoor je oranje gaat zien.

- Het gezelschap kwam aan bij de Citrinevonk, een herberg-bordeel.

- Het gezelschap betaalde Madame Chère om over de Oranjegaarde te vertellen. Ze
  vertelde de legende van IJzerkaak.

<details>
    <summary>De Legende van IJzerkaak</summary>

De feeën... sommigen noemen ze heksen. Sommigen noemen ze kakelende oude dames
gebogen over verwarmde potten, een kokende bouillon aan het karnen. Proef het
brouwsel, en proef hun toorn. Of zijn het gevleugelde feeën die over de
weilanden sprinten? Pluche klootzakken zo ondeugend als vossen en zo vervelend
als muggen? Deze verhalen zijn slechts fantasieën---een mensgericht begrip om
beter te begrijpen hoe een fee-monster in wisselwerking staat met de materiële
wereld.

En deze fantasieën zijn dwaas.

De feeën spelen hun eigen spelletjes, en als ze zonder komen te zitten, reiken
ze naar onze wereld om te herbevoorraden. Ze zijn als goden in die zin: het
duwen van stervelingen naar hun eigen willekeur. Maar goden spelen geen
spelletjes in de hemel waar ze leven. Ze spelen in onze wereld. Feeën spelen
spelletjes met ons, maar ze slepen onze verwarde geesten terug naar hun rijk. De
goden huren om te bezitten. De feeën stelen.

Maar in tegenstelling tot de goden, kan de fee bloeden, wat ons naar dit
tandeloze wonder brengt:

Yanley de IJzerkaak verloor zijn tanden jaren geleden. Zijn tanden besloten om
een gevecht te beginnen met de vuist van een orc, en de vuist van de orc had een
natuurlijk voordeel. Als een bekwame smid maakte Yanley voor zichzelf een nieuwe
set ijzeren tanden die hem zijn bijnaam verdiende. Naast zijn vechtpartij met
een orc en zijn vaardigheden in de smederij, leidde IJzerkaak een zelden
onbewogen leven, bijna net zo onbewogen als een zinloze onderdaan. Maar toen hij
paden kruiste met een bosje bloedsinaasappelbomen, scheurde Yanley een gat in
het land van de feeën met gevolgen die het best als vreemd kunnen worden
omschreven.

Dit gat vol vreemde gevolgen begon met Skix, een bemoeizuchtige feeëngeest die
zijn kicks kreeg door te leven met sinaasappelbomen. Sinaasappelbomen trokken
Skix naar hun bosjes met een feromantische allure, en plaagden de geest van de
natuur met rijpend fruit en open huidmondjes. Er was een intimiteit tussen de
geest en de bomen, en feeën---instinct in haar zuiverste vorm---handelen nooit
met voorzichtigheid als er iets is dat hun verlangens prikkelt.
Feeënsensualiteit is geen broedende verleiding, zoals hoe succubi en incubi hun
prooi verleiden. Feeënsensualiteit is pure, schaamteloze adrenaline.

Dus Skix leefde met de bloedsinaasappels, en kreeg uiteindelijk de vorm van een
bloedsinaasappel. Net als in een sterfelijk huwelijk, plantte Skix zijn wortels
en vestigde zich met het bos. Onroerend, maar toch met elkaar verbonden, weefde
Skix zijn wortels en levenskracht in de naburige bomen, elke lente trillend in
extase wanneer de bijen en vlinders zwermden in hun wenkende bloemen. Zijn
bewustzijn rolde terug toen de dopamantische vloed van het leven morste in elke
spleet van het bloeiende bos, en bracht zaad en stuifmeel en nectar en nieuwe
geboorte de wereld in. Skix zwom in het paradijs, en de sinaasappels zwollen in
hitte jaar na jaar.

Na jaar...

Maar toen sijpelde de BRAND binnen! Een heet, harkend, oorverdovend schaafsel
dat Skix uit zijn botanische harem scheurde. Het ijzer BRANDde zijn ziel! Een
mens was zijn gaarde binnengedrongen! Elke sinaasappelboom was verbonden met het
bestaan van Skix, en elke seconde werd er een dodelijk stuk ijzer in Skix'
vleselijke vesting gebeten. Het was een man met ijzeren tanden die de chaos
veroorzaakte---het ijzer, een altijd vergiftigend metaal dat de gruwelijke dood
tot gevolg had---wat is erger dan de luxe te krijgen om het alleen maar te laten
BRANDen?

Voor Yanley de IJzerkaak was dit gewoon een andere boomgaard van fruitbomen. De
bloedsinaasappels hier smaakten bijzonder zoet, dus Yanley bleef zijn vulling
opeten, en de citrussappen morsten over zijn kin, en besprenkelden de grond met
pulp en zoetwater. Hij liet de bomen bij elke hap bloeden, en het gras onder
zijn voeten stolde van afschuw. Skix vocht om te ontsnappen aan de hel die
IJzerkaak onbewust had veroorzaakt met zijn middagsnack, maar Skix had zich te
diep opgerold. In voor- en tegenspoed kon zijn huwelijk met het bos niet worden
verbroken.

IJzerkaak, zijn maag nu gevuld, verliet het gemangde bosje. Skix kon alleen nog
maar schreeuwen in vergiftigde angst, voor altijd bloedend, maar nooit genoeg om
te sterven.

Hij schreeuwt en bloedt nog steeds.

Op dit moment.
</details>

## Sessie 25 - 2020-09-29

# 998-05-13

- Het gezelschap deed onderzoek in de bibliotheek van Amberwall.

- Donna vond ongerelateerde informatie over het vormen van een nauwe bond tussen
  tovenaar en dier. Ze vond een student-begeleider om haar te helpen met het
  vormen van zo'n bond met Hoop. Hoop werd een familiar van Donna.

- Grinvor vond de legendes van Bel'Ataka.

## Sessie 26 - 2020-10-02

- Het gezelschap ging winkelen. Donna liet hier de ring die ze kreeg van Zizgag
  identificeren. De ring laat iemand beter zwemmen.

- Bast zocht iemand---niet van Huis Cannith---om zijn magische bril te
  integreren. Er werd hem verteld dat dit misschien mogelijk zou zijn in Sharn.

- De Wilgenwachters begonnen hun reis naar de Oranjegaarde.

# 998-05-14

- Onderweg naar de gaarde kwam het gezelschap een groep Aundairiërs tegen die
  een kappot wiel hadden. De Aundairiërs reisden met twee karren vol magisch
  hout naar Passage. Het hout was bestemd voor het repareren van de brug in
  Riviertraan. Ze vroegen om hulp.

- Het gezelschap gebruikte een illusie om de groep bang te maken en weg te laten
  rennen. Bast deed alsof hij een boze ent uit het woud was, en vernietigde de
  karren.

- Na het vernietigen van de karren verdwenen de karren en de Aundairiërs alsof
  ze illusies waren. Het gezelschap verloor ieder 200 soevereinen.

## Sessie 27 - 2020-10-03

- Het gezelschap reisde verder naar de Oranjegaarde.

# 998-05-18

- Het gezelschap kwam dichtbij de gaarde. In een radius rond het centrum van de
  gaarde was een dood bos.

- Onderweg naar het centrum van de gaarde ervoer men vreemde dingen:

  + Grinvor: Rook een geur van pittige sinaasappels.
  + Donna: Zag visioenen van lijdende feeën dansen. "Hij had ijzer in zijn
    tanden, en hij liet de fee branden", zeiden ze. "Hij bloedt nog steeds. Hij
    schreeuwt nog steeds. Hij heeft de glimlach uit de wind gedreven en heeft
    hem met angst gestuit. O, vreemdeling, hoe sterft het levenloze als de pijn
    dat niet wil?"
  + Bast: "Dood het, als het moet", hoorde hij de bomen spreken. "Dood het, als
    het moet!" hun kreten werden sterker. "Dood het, als het moet!" Allemaal
    tezamen: "DOOD HET! HET MOET!" en alles wat overbleef was stilte, en
    visioenen van een zielloze mond.

- Donna vloog de lucht in op Fuego en zag een gezonde gaarde in het midden van
  het dode stukje bos.

- Het gezelschap ging de gaarde in. Ze vonden daar een gat in de vorm van een
  mond in de grond. Als ze dichtbij kwamen, hoorden ze gedempt geschreeuw.

- In de gaarde waren de bomen in volle bloei. "De witte bloei is zo helder als
  zijden doek en de vruchten zwellen op als bubbelbellen die op het punt staan
  te barsten."

- Bast ging het gat in op onderzoek uit. Hij zag bloed in de mond, en een laag
  roest op het hout.

- Het gezelschap wist twee oplossingen: Het vernietigen van de mond, of het
  helen van de mond. Donna probeerde de mond te helen door geconcentreerd zuur
  over de mond te smeren.

- Dit hielp, en de ingesloten feeëngeest kwam naar buiten. Zijn naam was Skix,
  en hij was dankbaar voor het verlossen van zijn lijden.

- Skix wilde terug naar Thelanis. Als gunst vroeg het gezelschap om naar de
  Hofnar te gaan en Fee terug te sturen naar het gezelschap.

- Kort na het vertrek van Skix verscheen Fee in de gaarde.

- Slablaadje merkte dat er niemand meer in de Oranjegaarde was om het te
  beschermen. Hij opperde zichzelf als ent van de gaarde. Toen hij zichzelf
  worteldin de gaarde, gaf Bast hem een nieuwe naam: Slakrop.

## Sessie 28 - 2020-10-08

# 998-05-19

- Bast suggereerde om Abalaba en studenten naar de gaarde te sturen.

- Het gezelschap transporteerde snel naar Groenhart om het nieuws over de gaarde
  te brengen naar Oalian. Ze gingen weer terug met een groep druïdes die de
  gaarde voor een poos zouden bewaken.

- Het gezelschap verliet richting Arcanix.

# 998-05-23

- Eenmaal in Arcanix werd het gezelschap uitgenodigd naar de kamer van Adal in
  Nocturnas.

- Eenmaal bij de kamer van Adal werd het gezelschap verwelkomd door Leselor.
  Leselor was vrijgekomen op losgeld. Ze betreurde de omstandigheden, en gaf de
  bewerkte Eberronscherf aan het gezelschap.

- Leselor liet ook weten dat ze het mysterie omtrent Wilgspruit opgelost heeft,
  en dat ze die informatie graag met het gezelschap deelt: Fhazira werd gevolgd
  door een andere agent van de Koninklijke Ogen, en er werd een ondergrondse
  kerker gevonden. Fhazira leek daar Khyber te aanbidden, en had een voorraad van
  bewerkte Khyberscherven.

- Echter... Aundair heeft nu deze voorraad, en Aundair zou niet anders kunnen
  doen dan de middelen gebruiken die ze heeft. Aundair (Leselor) dreigde om de
  nieuwe scherven gebruiken tegen Eldeen als ze niet akkoord gaan met een
  diplomatieke ontmoeting over de soevereiniteit van Eldeen.

- Bast bewoog plots en trok de la vol Khyberscherven uit Leselors bureau. Een
  gevecht tussen het gezelschap en Leselor begon. Adal en een paar tovenaars
  waren snel aanwezig. Bast stopte de scherven in Donnas tas, en Donna
  veranderde in een vogel om te ontsnappen.

## Sessie 29 - 2020-10-11

- De Wilgenwachters vluchtten van Leselor en Adal. Donna verliet Nocturnas. Bast
  en Grinvor gingen verder het kasteel in, en stopten kort in Adals kantoor.

  + Adal en Leselor gingen allebei buiten westen.

## Sessie 30 - 2020-10-DD

- TODO

<!--
# 998-07-01

- Zomerfeest.

- Ontmoeting tussen de gasten van het winterfeest waar Wilgspruit vergiftigd is.
-->

<!--
# 998-07-XX

- De partner van [Swift](/karakter/swift) bevalt.
-->
