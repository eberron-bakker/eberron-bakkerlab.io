---
title: "Huisregels - Eldeen"
date: 2019-10-27T15:56:59+01:00
draft: false
---

# Setting Book

[Wild Cards' Compendium to
Khorvaire](https://carmenbianca.gitlab.io/compendium-to-khorvaire/).

[Wild Cards' Expanded Compendium to
Khorvaire](https://carmenbianca.gitlab.io/compendium-to-khorvaire/expanded/).

# Setting Rules

## Heroes Never Die

Heroes in movies very rarely die. When they do, they go down fighting or perform
one last, epic act of heroism.

With this rule in play, Wild Cards who are Incapacitated from a damage roll make
a Vigor roll as usual but treat Critical Failures as regular failures and ignore
the rules for Bleeding Out.

How a hero might survive what *should* be certain death is a chance to get
creative. An adventurer who falls from a towering cliff, for example, might land
in a pool of water or crash through the branches of a forest far below.

If the situation is particularly heroic or if it serves as a major story point,
the GM and player can decide the character perishes. A hero who confronts a
massive demon on a crumbling bridge, for example, might take the fiend with him
with his final blow.

**Villains:** The reverse is also true --- villains rarely die either! Heroes
should play this in the spirit it's intended --- they shouldn't attempt to cause
some sort of gruesome and undeniable death to a villain who falls into their
hands, for example. They should instead turn the captive over to the authorities
--- even though they know full well he will eventually escape to plague them
once again.

## More Skill Points

Thanks to technology and improved education, characters in modern and futuristic
settings have 15 skill points at character creation rather than 12. This helps
them take Driving, Electronics, and other skills common in the modern world.

It's up to the GM if this makes sense in her particular setting. A futuristic
but "savage" world of planetary romance probably doesn't need it, but it works
well for almost anything set in the developed world from about 1950 on, hard
scifi, or "exploration" scifi like *The Last Parsec*.
