---
title: "Sessie 25"
date: 2020-09-29T14:06:58+02:00
draft: false
---

- Begin sessie: Empathy terug in papa's sheet. Charm is nu een modifier.

- Tijdens terugkomst naar bibliotheek: Donna helpt Grinvor waarschijnlijk met
  onderzoek naar Bel'Ataka. Terwijl ze onderzoek doet, vindt ze informatie over
  het vormen van een immens sterke band met een dier.

  + Het is een ritueel waar 100 sp aan houtskool, wierook en kruiden verbrand
    moeten worden in een bronze kom. De spreuk die Donna in een boek kan vinden
    gebruikt magische notatie waar ze niet bekend mee is. Ze kan hulp vragen van
    een student. De student wil een tegenprestatie in de vorm van *heling* van
    haar salamander-familiar.

- Grinvor vindt informatie over Bel'Ataka.
