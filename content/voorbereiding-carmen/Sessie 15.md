---
title: "Sessie 15"
date: 2020-05-23T14:20:58+01:00
draft: false
---

- Geef het gezelschap schoenen van snelheid:

> While you wear these boots, your Pace increases by 1, and your running die
> increases by one die type.

# Oalian

- Oalian is niet blij met het nieuws dat er Khyber-dingen gebeurd zijn.

- Oalian is ook niet blij met Aundairs invloed.

- Oalian wil de Wilgenwachters desondanks bedanken.

- Grinvor wordt uitgenodigd om Wachter van het Woud te worden. Zo niet, dan
  wordt hij erkend eervolle burger van Eldeen. Oalian heeft geen druïdische
  magie die hij kan verlenen aan een niet-druïde.

- Donna krijgt de gave om te reizen door het bomennetwerk van Khorvaire. Dit
  begint met de gave om van elke wilg in Khorvaire te reizen naar Oalian.

  + Oalian vraagt of Donna wil terugreizen na het zomerfeest, om te kijken wat
    er gedaan kan worden tegen Aundair.

- Bast krijgt "shifter"-vaardigheden. Oalian erkent dat Bast uniek is in Eldeen.
  De origine van Bast is speciaal. Bast put zijn leven uit zijn kern van hout.
  Maar hout is niet alles wat hij is. Hij heeft aspecten van magie en van
  oorlog.

- Het gezelschap krijgt gezamenlijk 5000 sp voor de reis.
