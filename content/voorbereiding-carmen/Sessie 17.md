---
title: "Sessie 17"
date: 2020-07-12T13:27:26+02:00
draft: false
---

- Pinoki maakt zich zorgen over de Hofnar. Hij legt aan Grinvor uit dat dit
  vaker gebeurd is:

  + Tijdens de zomer is de Hofnar blij, en brengt hij blijfschap naar ons.
  + Tijdens de winter is de Hofnar verdrietig, en moeten haar volgelingen haar
    blijdschap teruggeven.
  + De lengtes van de zomer en winter staan nooit vast.

  + De Hofnar moet weer blijdschap krijgen.

    + Oplossing: Maak alsnog indruk op Bel'Ataka.

- Er staan nog steeds soldaten van Aundair aan de Eldeenzijde van de rivier. De
  brug is gerepareerd.

- De Ridders Arcane staan paraat aan de andere kant van de rivier. 25 jaar
  geleden gingen de Ridders Arcane over de brug, en op dat moment blies de brug
  op.

# De brugopeningsceremonie

- Een dag na de aankomst van het gezelschap is er een ceremonie voor het openen
  van de brug. [Heer Darro ir'Lain](/karakter/darro) is aanwezig voor de
  ceremonie.

- Eenpoot wilde de ceremonie niet houden voor de westerhelft. Fhazira leidt de
  ceremonie voor Eldeen.

  + "Zoals de Negen Soevereinen één zijn, zo zal ook Riviertraan weer één uit
    meerdere delen zijn."

- Tijdens ceremonie:

  + Bast hoort vogelfluijte.

  + In de verte richting het noorden ziet Bast Signaal een duim omhoog doen.

  + Boem, de brug blaast op.

- Gevecht:

  + Darro is *fucking boos*. Dit is de tweede keer dat dit gebeurt. Hij ordert
    de troepen om aan te vallen.

  + De ridders arcane zwemmen naar Eldeen.

  + De ridders te paard staan nog aan de andere kant, en worden geteleporteerd.

  + De Eldeen-wolven komen van de Aundair-kant.

  + Aaaaaaaaa!

# Gevecht

## Eldeen

- Bast (WC).
- Jes (WC).
- Grinvor (WC).
- Mumzok (WC).
- Eenpoot (WC).
- 3 elanden.
- 6 goblins.
- 5 archers.
- 5 wachters van het woud.
- Lifra (WC).
- 6 berserkers.

## Aundair

- Heer Darro (WC).
- 2 Dragonhawks.
- 2 Wizards.
- ~30 ridders arcane.


# Knight Arcane

**Attributes:** Agility d8, Smarts d6, Spirit d8, Strength d8, Vigor d6\
**Skills:** Athletics d6, Common Knowledge d6, Fighting d8, Intimidation d6,
Notice d6, Persuasion d6, Riding d8, Shooting d6, Spellcasting d8, Stealth d4\
**Pace:** 6; **Parry:** 8; **Toughness:** 8 (3)\
**Edges:** ---\
**Gear:** Wand (Range 6/12/24, Damage 2d6), arming sword (Str+d8), splint armor
(+3), medium shield (Parry +2, Cover -2)\
**Special Abilities:**

- **Spellcasting:** 15 Power Points, can cast *blast*, *bolt*, *burst*,
  *detect/conceal arcana*, and *protection*.

# Phantom horse

**Attributes:** Agility d8, Smarts d4 (A), Spirit d6, Strength d12, Vigor d8\
**Skills:** Athletics d8, Fighting d4, Notice d6\
**Pace:** 20; **Running Die:** d8; **Parry:** 4; **Toughness:** 8\
**Edges:** ---\
**Special Abilities:**

- **Size 2**
- **Kick:** Str+d4.

# Warforged

**Attributes:** Agility d6, Smarts d4, Spirit d6, Strength d8, Vigor d6\
**Skills:** Athletics d8, Fighting d8, Intimidation d6, Notice d6, Persuasion
d4, Stealth d4\
**Pace:** 6; **Parry:** 9; **Toughness:** 10 (4)\
**Edges:** ---\
**Gear:** Sword (Str+d8), large shield (Parry +3, Cover -4)\
**Special Abilities:**

- **Size 1**
- **Integrated Protection:** Warforged gain Armor +4.
- **Living Construct:** Warforged add +2 to recover from being Shaken, ignore
  one level of Wound modifiers, do not need to breathe, eat, or drink, and are
  immune to poison and disease. Instead of sleeping, warforged recharge in an
  inactive, motionless, but conscious state. Warforged cannot heal naturally.
  Healing requires the Repair skill, which takes one hour per current Wound
  level per attempt and is not limited to the "**Golden Hour**". Alternatively,
  the powers healing and repairing can heal a warforged.

# Berserker

**Attributes:** Agility d6, Smarts d4, Spirit d6, Strength d8, Vigor d8\
**Skills:** Athletics d6, Common Knowledge d4, Fighting d8, Intimidation d8,
Notice d6, Persuasion d4, Riding d8, Stealth d6\
**Pace:** 6; **Parry:** 6; **Toughness:** 7 (1)\
**Edges:** ---\
**Gear:** Battle axe (Str+d8), leather armor (+1)

# Dire Wolf

**Attributes:** Agility d8, Smarts d4 (A), Spirit d6, Strength d8, Vigor d8\
**Skills:** Athletics d8, Fighting d8, Intimidation d8, Notice d6+2, Stealth d8\
**Pace:** 10; **Parry:** 6; **Toughness:** 8\
**Edges:** Alertness\
**Special Abilities:**

- **Size 2**
- **Bite:** Str+d6.
- **Speed:** d10 running die.

# Goblin

**Attributes:** Agility d8, Smarts d6, Spirit d6, Strength d4, Vigor d6\
**Skills:** Athletics d6, Common Knowledge d10, Fighting d6, Notice d6,
Persuasion d4, Shooting d8, Stealth d10, Taunt d6\
**Pace:** 6; **Parry:** 6 (1); **Toughness:** 5 (1)\
**Edges:** ---\
**Gear:** Spear (Str+d6, Reach 1, Parry +1, Range 3/6/12), shortbow (Range
12/24/48, Damage 2d6), leather armor (+1)\
**Special Abilities:**

- **Size -1**
- **Low Light Vision:** Ignore penalties for Dim and Dark Illumination.

# Eland

**Attributes:** Agility d8, Smarts d6 (A), Spirit d8, Strength d8, Vigor d10\
**Skills:** Athletics d8, Fighting d6, Notice d8\
**Pace:** 10; **Parry:** 5; **Toughness:** 9\
**Edges:** ---\
**Special Abilities:**

- **Size 2**
- **Antlers:** Str+d6.
- **Hooves:** Str+d4.
- **Speed:** d8 running die.

# Archer

**Attributes:** Agility d8, Smarts d6, Spirit d6, Strength d6, Vigor d6\
**Skills:** Athletics d6, Common Knowledge d4, Fighting d6, Notice d8,
Persuasion d4, Shooting d8, Stealth d6\
**Pace:** 6; **Parry:** 5; **Toughness:** 6 (1)\
**Edges:** ---\
**Gear:** Longbow (Range 15/30/60, Damage 2d6), dagger (Str+d4, Range 3/6/12),
leather armor (+1)

# Wizard

**Attributes:** Agility d6, Smarts d12, Spirit d10, Strength d4, Vigor d6\
**Skills:** Academics d10, Athletics d4, Common Knowledge d10, Cosmology d8,
Fighting d4, Notice d10, Persuasion d6, Research d10, Shooting d8, Spellcasting
d12, Stealth d4\
**Pace:** 6; **Parry:** 4; **Toughness:** 7 (2)\
**Edges:** ---\
**Gear:** Wand (Range 6/12/24, Damage 2d6), magical armor (+2)\
**Special Abilities:**

- **Spellcasting:** 25 Power Points, can cast *blast*, *bolt*, *burst*,
  *detect/conceal arcana*, *havoc*, *dispel*, and *stun*.

# Warden of the Wood

**Attributes:** Agility d6, Smarts d6, Spirit d8, Strength d6, Vigor d6\
**Skills:** Athletics d6, Common Knowledge d6, Druidism d8, Fighting d6,
Notice d6, Persuasion d6, Shooting d6, Stealth d6\
**Pace:** 6; **Parry:** 5; **Toughness:** 6 (1)\
**Edges:** ---\
**Gear:** Sword (Str+d6), leather armor (+1)\
**Special Abilities:**

- **Spellcasting:** 10 Power Points, can cast *beast friend*, *havoc*, and
  *healing*.
