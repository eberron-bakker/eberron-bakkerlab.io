---
title: "Sessie 2"
date: 2019-10-30T20:37:23+01:00
draft: false
---

- Het gezelschap overnacht in Het Gebroken Glas. Zodra ze wakker worden zijn de
  ijsmeubels weg. Het interieur is omgebouwd naar het *Cyre*-thema.

- *Laurent* dient het gezelschap als butler.

- Pinoki verkoopt een dekmantel aan Bast voor 1 gp in de ochtend.

- Voordat het gezelschap vertrekt wil Swift gedag zeggen tegen Jes. Hij zegt dat
  hij hoopt dat de manen haar de weg zullen wijzen. Ter herinnering geeft hij
  Jes een halsketting met een halve maan. De halsketting is van een verre
  voorouder geweest; een maanspreker (v) die tijdens de tijd van de Zilveren
  Kruistocht vermoord is.

- Het gezelschap gaat op pad richting de gaard in het Rivierwoud.

- 100 mijl (4 dagen) reizen door het woud.

- Eerste reisdag: Party komt langs een stromend beekje van 15ft / 4.5m breed. De
  party moet eroverheen. Met een DC 20 Perception check zien ze twee krokodillen
  (*crocodile*) in het water. Als de party het water verstoort, zullen de
  krokodillen aanvallen. De krokodillen zullen de party niet volgen. De party
  kunnen het beekje trotseren door:

  + te springen.
  + te zwemmen.
  + een touw met *grappling hook* naar een boom aan de overkant te spannen.
  + een gevallen boom te gebruiken als vlot. Versloomt de reis van die dag.

- Op de tweede reisdag: Het begint enorm te sneeuwen (p. 110 DMG) voor twee
  dagen lang. DC 10 Con save elke vier uur of krijg één level exhaustion. Als de
  party gaat kamperen hoeft dit niet. Als de party warme kleren heeft: Doe een
  Con save aan het einde van de dag. Kan exhaustion niet kwijt raken totdat de
  sneeuw opgehouden is.

- Tijdens de sneeuwstorm komt de party een klein bruin beertje tegen dat
  verlaten lijkt. Na een uur komt mama beer (*brown bear*) haar welp redden, en
  zal de party aanvallen.

- Op de derde reisdag: Tijdens de sneeuwstorm komt de party een groep Kinderen
  des Winters tegen. De Kinderen zijn aan het dansen en zingen om de geesten van
  de dood te wekken. Dit is het grootste feest van de Kinderen. Op het moment
  dat de party de groep tegenkomt, vechten twee leden tot de dood in een heilig
  ritueel om de nieuwe leider te worden. De Kinderen zijn een dozijn mensen en
  shifters, gebruik *orc* statsheet. De leider zal Runor (*thug*, mens m) zijn.

  De party heeft de mogelijkheid om te vluchten, te schuilen, of te
  confronteren. Bij confrontatie zullen drie leden vechten met de party om hun
  kracht te demonstreren en bewijzen.

- Op en na de derde reisdag: Met een DC 20 Perception check merkt de party dat
  een raaf (*raven*, fey) hen al een poosje volgt. De raaf is een familiar van
  de Koninklijke Ogen van Aundair.

- Bij aankomst bij de gaard staat een eland (*elk*) in de weg. De eland houdt
  stand voor de gaard, en probeert het gezelschap te hinderen om de gaard toe te
  treden. Het gezelschap moet bewijzen aan de eland dat ze goede bedoelingen
  hebben. Ze kunnen dit doen door:

  + Druïdische magie te gebruiken.
  + Een DC 20 Animal Handling check.
  + Slablaadje te laten zien.
  + Een uur lang passief in de aanwezigheid van de eland te blijven.

- Totdat de eland toegang geeft is de gaard onder effect van *druid grove*
  (XGE). Er is een dikke mist van 10 voet hoog. De grond heeft grijpend onkruid
  en doornstruikgewas (*spike growth*). Vier bomen (*awakened tree*) in de gaard
  ontwaken als iemand toetreedt zonder toestemming.

- Als het gezelschap de eland niet respecteert, dan zullen andere boswezens de
  eland komen te helpen. Als het gezelschap tot geweld overgaat, dan mogen ze de
  gaard niet meer in.

- De eland is een druïde genaamd [Pririla](/karakter/pririla). Ze blijft gewoon
  in elandvorm behalve als er een goede reden voor is.

- In de gaard is een duidelijke plek aan de rand waar een boom "geplant" was en
  sindsdien ontaard is. Dit is het thuis van [Wilgspruit](/karakter/wilgspruit).
  Met een DC 15 Investigation of Survival check rond deze plek kan een karakter
  ontdekken dat er op een kleine afstand van de plek---net iets onder de
  grond---een mix van alcohol en een fijne paarsblauwe stof is.

- Het gezelschap weet het herfstritueel waarbij Oalian alcohol toegediend
  krijgt.

- De fijne paarsblauwe stof is gemaakt van super kleine kristallen. Met een DC
  15 Arcana check weet een karakter dat het een gemalen Khyberkristal is.
  Khyberkristallen komen uit Khyber, de ondergrondse wereld onder Eberron. Deze
  kristallen worden vaak gebruikt in rituelen om wezens te *binden*. De meest
  bekende applicatie is de trein van Huis Orien die lucht-elementairen gebruikt
  om de trein aan te drijven.

- Zodra het gezelschap de Khyberkristalstof ontdekt komt Pririla naar het
  gezelschap toe. Haar hele gezicht vertrekt wanneer ze leert dat het niet zo
  goed gaat met Wilgspruit. Ze valt stil en trekt zich terug naar de rand van de
  gaard, waar ze water in haar gezicht spat. Ze neemt een bosbes (*goodberry*)
  uit haar buidelzakje en bied het gezelschap ook aan.

- Pririla vertelt het gezelschap over het afgelopen Herfstfeest. Het thema was
  verzoening na het einde van de Laatste Oorlog, dus mensen buiten de Wachters
  van het Woud waren ook uitgenodigd. Ze is bang dat iemand de alcohol
  vergiftigd heeft.

- Het gezelschap kan rusten in de gaard. Als men rust in de gaard, krijgt men
  1d6 extra hit points terug aan het einde van een short rest. Donna krijgt een
  spell slot terug.
