---
title: "Sessie 12"
date: 2020-04-25T23:14:56+01:00
draft: false
---

- Bast heeft twee Wonden.

- Gezelschap begint dichtbij Groenhart in het Torenhoge Woud. De bomen hier zijn
  gigantisch. De boom waarvoor het gezelschap staat is een mammoetboom die vele
  malen groter is dan de omringende bomen. De bovenste boomlaag is dertig
  kilometer in elke richting, en de stam is makkelijk honderd meter in diameter.

- Dim Light in het Torenhoge Woud.

- Niet lang nadat het gezelschap in het Torenhoge Woud gearriveerd is, komt een
  stokoude centaur ([Ĉevalo](/karakter/ĉevalo)) hen tegemoet. Ĉevalo is aardig
  richting het gezelschap, en wil hen best helpen om naar Oalian te reizen. Hij
  heeft echter de hulp van het gezelschap nodig.

  + Hij heeft een ei bij zich dat ongeveer een kilo weegt. Het ei werd gevonden
    in bezit van een bandiet, en Ĉevalo wil het ei terugbrengen naar de moeder.
    Hij heeft achterhaald dat de moeder een drakenhavik is, ergens in de boomtop
    van de gigantische mammoetboom. Zijn vraag is of het gezelschap dit voor hem
    kan regelen.

  + De drakenhavik is vijandig naar iedereen die in bezit is van haar ei.

- Ĉevalo neemt het gezelschap mee richting Groenhart. Ze komen ongeveer op 03-26
  aan.

- Het nieuws van Wilgspruit is al bekend bij Groenhart. De krant was de
  entourage uit Riviertraan voor. Andere krantenkoppen:

  + Herdenking van Narath: negen jaar sinds bloedbad. Huis Deneith alsnog
    doodstil.
  + Moord op Orien-expres: Huis Cannith-lid vermoord, echtgenoot verdacht.
  + Lentefeest in aantocht: seizoensfeesten steeds populairder in Oost-Eldeen.

- Als gezelschap vraagt over Riviertraan: Laatste kennis is dat er aan de brug
  gewerkt wordt, en dat er geen nieuwe voorvallen geweest zijn.

- Er is geen commercie in Groenhart. Alles is gratis voor het gezelschap, zolang
  ze respect hebben voor de druïdische tradities. Ĉevalo weet een
  khoravar-familie die mogelijk ruimte heeft voor het gezelschap.

  + Faefine: khoravar (v) die de wekelijkse *eenheidsmaaltijden* organiseert in
    Groenhart. Ze heeft een grote boomhut-woning tussen de stevige takken van
    een grote eikenboom. Het is momenteel druk omdat er veel khoravar zijn voor
    het lentefeest.
