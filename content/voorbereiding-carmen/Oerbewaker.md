---
title: "Oerbewaker"
date: 2020-05-02T16:16:11+01:00
draft: false
---

# Oerbewaker (Primeval Guardian)

Primeval Guardians follow an ancient tradition rooted in powerful druidic magic.
These rangers learn to become one with nature, allowing them to channel the
aspects of various beasts and plants in order to overcome their foes.

A Primeval Guardian gains the ability to temporarily grow and take on the
appearance of a treelike person, covered with leaves and bark. As an Action,
they assume this guardian form which lasts until ended as an Action, or until
they are rendered unconscious.

The guardian form has the following effects:

- **Claws:** The guardian has vine-like claws as a Natural Weapon that does
  Str+d8 damage.

- **Plant:** Ignores 1 point of Wound penalties; doesn't breathe; immune to
  disease and poison.

- **Reach +1:** The guardian has Reach +1.

- **Size +4 (Large):** The guardian form is approximately five yards tall. They
  do not get an extra Wound for being Large, but do gain extra Toughness. Like
  all Large creatures, they are subject to **Scale** (p. 106 *Savage
  Worlds*)---Normal creatures add +2 to their attack rolls against the guardian.

- **Slow Pace:** The guardian has a Pace of 1" and a d4 running die.

- **Strong:** The guardian's Strength is increased to d12+2.

- **Swat:** The guardian ignores up to 2 points of Scale penalties when making
  attacks.
