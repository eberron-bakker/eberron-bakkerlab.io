---
title: "Grinvor Fey"
date: 2019-12-15T12:21:47+01:00
draft: false
---

Grinvor krijgt training van Pinoki. De volgende beats:

- Pinoki heeft een schoolbank in zijn winkel neergezet. Als Grinvor gaat zitten,
  dan gaat hij op een scheetzak zitten.

- Pinoki vraagt Grinvor om notities te maken. Als Grinvor zijn tas opendoet, dan
  komt er een scheetgeluid uit.

- Grinvor hoeft helemaal geen notities te maken.

- Pinoki vraagt waar Grinvor allemaal geweest is. Hij luistert naar de verhalen
  van Grinvor.

- Pinoki vraagt of Grinvor uit *buiten* Khorvaire geweest is. Als hij "nee"
  antwoordt, dan heeft Pinoki een geniepige glimlach.

- Pinoki wil Grinvor meenemen naar een plek van puur geluk, maar om daar te
  kunnen komen, moeten eerst een paar dingen gedaan worden. Pinoki neemt Grinvor
  mee.

- Pinoki wijst naar de bouwvakkers. Hij wil grapjes met hen uithalen:

  + Vervang iemands hamer met een vis. Gebruik *phantasmal force* om de
    bouwvakker te laten doen denken dat het een hamer is. De bouwvakker probeert
    een minuut lang spijkers te slaan met een vis.

  + Een van de bouwvakkers is een hek aan het timmeren. Hij heeft af en toe een
    nieuwe plank nodig. Elke keer wanneer de bouwvakker een nieuwe plank nodig
    heeft, ligt de plank aan een andere kant. (*unseen servant*)

  + Cast *sleep* op een bouwvakker. Scheer de helft van de baard weg, en teken
    de baard weer in.

  + Cast *minor illusion* op het einde van de brug. Bouwvakkers stappen op de
    illusie en vallen het water in.

- Als ze klaar zijn met grappen, dan zijn ze klaar voor de dag. Pinoki vraagt
  Grinvor om zijn beste grap voor te bereiden en morgen weer terug te komen.

--- SKIP ---

- Pinoki komt elke dag terug om kattenkwaad uit te halen.

- Na een poosje komt Grinvor bij Pinoki langs. Als hij weer naar buiten komt is
  Dribbel niet meer buiten, maar een eenhoorn. De eenhoorn neemt hem mee naar
  het Feeënhof.

- Als Grinvor langs Bast rijdt, dan plakt hij een briefje met "niks zeggen!" op
  z'n rug. Een *unseen servant* begint Basts tas langzaam te vullen met stenen,
  en decoratieve lianen om zijn takken te hangen.

- Daarna blijft hij doorgaan op de eenhoorn. De winter maakt ruimte voor de
  zomer, en Grinvor komt Thelanis---het Feeënhof---binnen.

- De eenhoorn stopt bij een boom waar allemaal groente en fruit aan hangt:
  Bananen, wortels, broccoli, mandarijnen, aardappels, aardbeien, komkommers,
  tomaten, peren, kersen, et cetera. Elk stuk groente of fruit smaakt naar heel
  iets anders.

- Grinvor krijgt kort de mogelijkheid om met de Hofnar te spreken.

- De Hofnar haalt een grap uit: Hij vraagt of Grinvor wil theeleuten. Als hij ja
  zegt, dan verandert Grinvor in een theepot, en de Hofnar gaat theeleuten.
  Grinvor kan nog steeds praten.

- De Hofnar vraagt de leukste vraag van Grinvor.

- De Hofnar zegt dat het heel leuk geweest is, en zegt "we moeten dit nog een
  keer doen". Hij verlaat, en Grinvors eenhoorn neemt hem terug naar de echte
  wereld. Tijdens de overgang naar de echte wereld wordt de eenhoorn Dribbel.
