---
title: "30 Stats"
date: 2020-10-16T15:02:07+02:00
draft: false
---

### Aurala

**Attributes:** Agility d6, Smarts d10, Spirit d12, Strength d6, Vigor d6\
**Skills:** Academics d12, Athletics d4, Common Knowledge d10, Fighting d4,
Intimidation d8, Notice d10, Persuasion d12, Riding d6, Shooting d4, Stealth d6,
Taunt d10\
**Pace:** 6; **Parry:** 4; **Toughness:** 8 (3)\
**Edges:** Alertness, Aristocrat, Attractive, Charismatic, Connections\
**Gear:** Amulet of protection (+3), glamerweave

### Darro

**Attributes:** Agility d8, Smarts d10, Spirit d10, Strength d8, Vigor d8\
**Skills:** Academics d6, Athletics d6, Common Knowledge d6, Fighting d10,
Intimidation d6, Notice d6, Persuasion d8, Riding d10, Shooting d6,
Spellcasting d10, Stealth d4\
**Pace:** 6; **Parry:** 7; **Toughness:** 10 (4)\
**Edges:** Aristocrat, Soldier\
**Gear:** Wand (Range 6/12/24, Damage 2d6), arming sword (Str+d8), plate armor
(+4)\
**Special Abilities:**

- **Spellcasting:** Darro has 20 Power Points and knows the following powers:
  *Blast*, *bolt*, *burst*, *detect/conceal arcana*, *dispel*, *intangibility*,
  *protection*, *slumber*, *stun*.

\clearpage

### Faena

**Attributes:** Agility d4-1, Smarts d10, Spirit d12, Strength d4-1, Vigor d6-1\
**Skills:** Academics d10, Athletics d4, Common Knowledge d8, Cosmology d8,
Druidism d12, Fighting d4, Notice d10, Persuasion d10, Riding d6, Science d8,
Shooting d8, Stealth d6, Survival d10\
**Pace:** 5; **Parry:** 5; **Toughness:** 6 (1)\
**Hindrances:** Elderly\
**Edges:** ---\
**Gear:** Staff (Range 12/24/48, 2d6, Parry +1), soft leather armor (+1)\
**Special Abilities:**

- **Low Light Vision:** Half-elves ignore penalties for Dim and Dark
  Illumination.
- **Spellcasting:** Faena has 25 Power Points and knows the following powers:
  *Banish*, *beast friend*, *burrow*, *detect/conceal arcana*, *divination*,
  *entangle*, *environmental protection*, *farsight*, *havoc*, *healing*,
  *light/darkness*, *mind link*, *shape change*, *slumber*, *summon ally*, and
  *teleport* (via trees).

### Kor

**Attributes:** Agility d8, Smarts d8, Spirit d10, Strength d8, Vigor d8\
**Skills:** Academics d6, Athletics d6, Common Knowledge d6, Fighting d8,
Intimidation d6, Notice d10, Persuasion d8, Riding d6, Shooting d6, Stealth d6\
**Pace:** 6; **Parry:** 6; **Toughness:** 10 (4)\
**Edges:** Alertness, Aristocrat, Soldier\
**Gear:** Sword (Str+d8), plate armor (+4)

### Leselor

**Attributes:** Agility d8, Smarts d8, Spirit d8, Strength d6, Vigor d6\
**Skills:** Academics d8, Athletics d6, Common Knowledge d8, Fighting d6, Healing d4,
Intimidation d6, Notice d10, Persuasion d10, Riding d6, Shooting d8,
Spellcasting d10, Stealth d10, Survival d4, Taunt d6, Thievery d6\
**Pace:** 6; **Parry:** 5; **Toughness:** 8 (3)\
**Edges:** Combat Reflexes (+2 to recover from Shaken or Stunned)\
**Gear:** Wand (Range 6/12/24, Damage 2d6), dagger (Range 3/6/12, Damage
Str+d4), ring mail armor (+3, chest), shiftweave\
**Special Abilities:**

- **Low Light Vision:** Half-elves ignore penalties for Dim and Dark
  Illumination.
- **Spellcasting:** 15 Power Points, can cast *blind*, *detect/conceal arcana*,
  *disguise*, *dispel*, *illusion*, *lock/unlock*, *invisibility*, and
  *scrying*.

\clearpage

### Moranna

**Attributes:** Agility d6, Smarts d10, Spirit d8, Strength d12, Vigor d6\
**Skills:** Academics d10, Athletics d4, Common Knowledge d8, Gambling d4,
Intimidation d10, Notice d10, Persuasion d12, Research d8, Riding d6,
Spellcasting d10, Stealth d6, Taunt d6\
**Pace:** 6; **Parry:** 2; **Toughness:** 7\
**Hindrances:** Ruthless, Ugly\
**Edges:** Aristocrat, Connections, Menacing\
**Special Abilities:**

- **Bite:** Str+d4.
- **Change Form:** As an action, a vampire can change into a bat with a Smarts
  roll at -2. Changing back into humanoid form requires a Smarts roll.
- **Charm:** Vampires can use the *puppet* power on those attracted to them
  using their Smarts as their arcane skill. They can cast and maintain the power
  indefinitely, but may only affect one target at a time.
- **Invulnerability:** Vampires can only be slain by sunlight or a stake through
  the heart (see those **Weaknesses**, below). They may be Shaken by other
  attacks, but never Wounded.
- **Mist:** Vampires have the ability to turn into mist. Doing so (or returning
  to human form) requires an action and a Smarts roll at -2.
- **Spells:** Moranna has 30 Power Points and can cast: *Bolt*, *darkness*,
  *drain power points*, *fear*, *lower trait*, *puppet*, *sloth*, *zombie*.
- **Sire:** Anyone slain by a vampire has a 50% chance of rising as a vampire in
  1d4 days.
- **Undead:** +2 Toughness; +2 to recover from being Shaken; no additional
  damage from Called Shots; ignores 1 point of Wound penalties; doesn't breathe;
  immune to disease and poison.
- **Weakness (Holy Symbol):** A character may keep a vampire at bay by
  displaying a holy symbol. A vampire who wants to directly attack the victim
  must bear her in an opposed Spirit roll.
- **Weakness (Holy Water):** A vampire sprinkled with holy water is Fatigued. If
  immersed, she combusts as if it were direct sunlight.
- **Weakness (Invitation Only):** Vampires cannot enter a private dwelling
  without being invited. They may enter public domains as they please.
- **Weakness (Stake Through the Heart):** A vampire hit with a Called Shot to
  the heart (-4) must make a Vigor roll versus the damage total. If successful,
  it takes damage normally. If it fails, it disintegrates to dust.
- **Weakness (Sunlight):** Vampires burn in sunlight. They take 2d4 damage per
  round until they are ash. Armor protects normally.

### Sasik

**Attributes:** Agility d8, Smarts d6, Spirit d8, Strength d6, Vigor d6\
**Skills:** Academics d6, Athletics d8, Common Knowledge d8, Fighting d6, Focus
d8, Notice d8, Persuasion d8, Riding d10+1, Shooting d6, Stealth d6\
**Pace:** 6; **Parry:** 6; **Toughness:** 5\
**Edges:** Aristocrat, Beast Master, Connections\
**Gear:** Wooden staff (Range 12/24/48, 2d6, Parry +1)\
**Special Abilities:**

- **Spells:** Sasik has 10 Power Points and knows the following powers: *Beast
  friend*, *boost trait* (animals only), *empathy* (animals only), *slumber*
  (animals only).

### Shaeras

**Attributes:** Agility d12, Smarts d6, Spirit d12, Strength d10, Vigor d8\
**Skills:** Athletics d12, Battle d12, Common Knowledge d6, Fighting d12,
Intimidation d8, Notice d8, Persuasion d6, Riding d10, Shooting d10, Stealth d8,
Taunt d8\
**Pace:** 6; **Parry:** 10; **Toughness:** 10 (4)\
**Edges:** Ambidextrous, Aristocrat, Combat Reflexes, Frenzy (Imp), Killer
Instinct, Nerves of Steel (Imp), Soldier, Trademark Weapon (Imp), Two-Fisted\
**Gear:** Dual scimitars (Str+d8+1), plate armor (+4)\
**Special Abilities:**

- **Fey Ancestry:** Elves receive a +2 bonus to resist magical effects that
  alter their mood or feelings, and magic cannot put them to sleep.
- **Low Light Vision:** Elves ignore penalties for Dim and Dark
  Illumination.
- **Trance:** Elves don't need to sleep. Instead, they meditate deeply,
  remaining semi-conscious. This trance lasts approximately three hours.

### Taya

**Attributes:** Agility d6, Smarts d6, Spirit d10, Strength d6, Vigor d6\
**Skills:** Academics d8, Athletics d4, Common Knowledge d8, Cosmology d8,
Faith d8, Fighting d6, Healing d4, Notice d6, Persuasion d8, Stealth d4\
**Pace:** 6; **Parry:** 5; **Toughness:** 5\
**Edges:** ---\
**Special Abilities:**

- **Spells:** Taya has 20 Power Points and knows the following powers:
  *Healing*, *relief*, *smite*

\clearpage

### Zarud

**Attributes:** Agility d6, Smarts d10, Spirit d6, Strength d4, Vigor d4\
**Skills:** Academics d6, Athletics d4, Common Knowledge d6, Fighting d4, Notice
d8, Persuasion d6, Research d8, Stealth d6, Thievery d6\
**Pace:** 6; **Parry:** 4; **Toughness:** 5 (1)\
**Edges:** Alertness, Investigator\
**Gear:** Light armor (+1), sword (Str+d4)\
**Special Abilities:**

- **Low Light Vision:** Half-orcs ignore penalties for Dim and Dark
  Illumination.
