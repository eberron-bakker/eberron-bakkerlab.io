---
title: "Sessie 11"
date: 2020-04-05T10:21:57+01:00
draft: false
---

- De slippers kunnen naar Frendamus gebracht worden. Het duurt hem een paar uur
  om de slippers gereed te maken voor de Hofnar. Anders kunnen de slippers naar
  de Hofnar gebracht worden, en de Hofnar heeft wel een persoon aanwezig die de
  vloek ongedaan kan maken.

- Gezelschap keert terug richting het paleis.

- Ergens onderweg naar het paleis: Een zwijn komt uit de heg stormen. Het zwijn
  heeft een speer in zich. Het zwijn stormt een winkel (dierenasiel) binnen. Een
  moment later stormt de Wilde Jacht uit het heg.

  + Stealth check voor spelers. Als succesvol, dan gaat de Wilde Jacht gewoon
    verder. Zo niet, dan ziet de Wilde Jacht de spelers. Drie jagers gaan op de
    spelers af, en de rest gaat door.

- De eigenaar van het dierenasiel wil niet dat het gezelschap het zwijn
  meeneemt.

- Het gezelschap kan ander voedsel verzinnen, of 
