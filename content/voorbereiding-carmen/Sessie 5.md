---
title: "Sessie 5"
date: 2019-12-22T15:31:28+01:00
draft: false
---

- Begin met recap van afhakken van Basts hand.

- De Eldeen-rijders zijn bezig met het veroorzaken van chaos. Sommigen zwemmen
  de rivier over om de andere kant ook aan te vallen.

- Lifra keert zich naar Bast om Eenpoot te ondervragen over de strijdgesmedene.

- Lifra pakt de hand van Bast op, gebruikt het misschien als wapen in duel tegen
  Bast. Haar aanvallen doen 1 damage.

- Lifra forceert Bast om te zeggen dat hij een spion voor Aundair is.

- Lifra neemt de hand van Bast mee.

- Uiteindelijk trekken de Eldeen-wolven verder Aundair in.

# Bast

- Bast heeft geen hand meer.

- De Jorasco-halfling wil Bast best wel helen, maar verwacht 150 gp, directe
  betaling. Cédric wil dit niet ophoesten, en Eenpoot kan deze schuld niet
  waarborgen. Daarom suggereert Eenpoot om dit zelf te doen.

- Eenpoot neemt Bast en Donna mee naar Wilgspruit, en vraagt om een gunst,
  ondanks dat Wilgspruit een winterslaap doet. Eenpoot krijgt echter geen
  antwoord, en besluit om over een week opnieuw te proberen.

- Na een week: Eenpoot krijgt Wilgspruit heel tijdelijk weer bij bewustzijn.
  Wilgspruit kijkt Bast met compassie aan, en wenkt de groep om dichterbij te
  komen.

- Na een kort meditateritueel wordt de groep wakker in een moeras (Lamannia).

- Iets verderop ziet de groep en cirkel van grote mossige stenen. In
  werkelijkheid zijn de stenen aarde-elementals.

- De aarde-elementals komen na een poosje tot leven. Ze bestuderen de groep, en
  wijzen na "goedkeuring" allemaal een richting op.

- Als de groep die kant opgaan, dan komen ze een zwaar vervormde goblin
  (*dolgrim*) tegen, die met een goedendag langzaam een boom probeert om te
  hakken.

- De dolgrim is agressief en zal de groep aanvallen.

- Als de groep terugkeert bij de elementals na het gevecht, dan wordt er een
  ritueel uitgevoerd met Bast in het midden. Zijn afgehakte hand vervormt tot
  lianen, en een magische steen verplaatst zich door de lucht richting Bast.

- Geef magic item aan Bast.

# Donna

- Donna is aan het oefenen in het bos met Eenpoot.

- ... Grinvor komt langs op een eenhoorn.

- Na een poosje komt Cédric langs, zoekend door het bos. Hij is verrasd wanneer
  hij Donna (en Bast) ziet.

- Cédric is zijn knipperhond kwijt. De knipperhond rende het bos in, en hij is
  momenteel aan het zoeken.

- Cédric vraagt om hulp voor het zoeken naar de knipperhond. Hij offert 25 gp,
  en kan omhoog naar 50 gp.

- De groep kan de knipperhond vinden ongeacht wat ze rollen op hun survival
  check. Met een 15 of hoger vinden ze de knipperhond voordat het in gevecht is
  met een *choker*. Als lager, dan is de knipperhond in gevecht, en heeft het 11
  hp over.

- De knipperhond wil niet meekomen. Donna kan de knipperhond overtuigen in
  hondenvorm of met de juiste spreuk.
