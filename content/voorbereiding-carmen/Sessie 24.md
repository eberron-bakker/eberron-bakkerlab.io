---
title: "Sessie 24"
date: 2020-09-26T12:35:42+02:00
draft: false
---

# Items

- Meer vondsten in het laboratorium.

## Ring van Afwijking

De drager wordt verdedigd tegen aanvallen met fysieke projectielen. Deze
aanvallen hebben een -2 penalty.

## Onderzoek naar het creëren van levende spreuken.

Geschreven in magische notatie die alleen door ervaren tovenaars gelezen kan
worden.

## Adelaarsbril

De drager krijgt een +2 bonus voor Notice rolls om dingen te zien. Door een
tweede lens voor de bril te schuiven kan de drager kleine details waarnemen op
een afstand van één kilometer. Het lijkt alsof de drager komisch grote ogen
heeft terwijl de bril gedragen wordt.

## Staf van Kiezelstenen

Rond de kop van de staf zweven maximaal tien kiezelstenen. De kop heeft een
dradenkruis om mee te richten. Als men schiet met de staf, vliegt een van de
kiezelstenen weg met hoge snelheid. Het is een ranged weapon: Range 50/100/200,
Damage 2d6, AP 2, ROF 1, Shots 10, Min Str. d4. De staf heeft nieuwe
kiezelstenen nodig om te herladen.

## Dolk van de Lafaard

Dolk met +1 Damage & +1 Fighting/Athletics. Als de dolk raakt, komt er een
afschuwelijke schreeuw uit de dolk, en wordt *fear* gecast op het doelwit met
een d10 arcane skill.

## Sending Stones

Sending stones come in pairs, with each smooth stone carved to match the other
so the pairing is easily recognized. While you touch one stone, you can use an
action to cast the sending spell from it. The target is the bearer of the other
stone. If no creature bears the other stone, you know that fact as soon as you
use the stone and don't cast the spell.

Once sending is cast through the stones, they can't be used again until the next
dawn. If one of the stones in a pair is destroyed, the other one becomes
nonmagical.

# Prep

## Onderzoek naar Bel'Ataka

- Grinvor wil naar bibliotheek in Arcanix.

- Research roll. Mogelijk support als Grinvor om hulp vraagt. Faal = geen
  progressie. Succes = enkele paragraaf over Bel'Ataka als redder van Shae
  Cairdal. Raise = alle informatie die papa heeft.

- Kan dag later terugkeren om opnieuw te rollen. Drie succes = volledige
  verhaal.

## Onderzoek naar nabije gaarde

- Donna wil met professor van Paulus/Diana praten over nabije druïdische gaarde.
  Professor Abalaba (gnoom, v).

- 100 km naar het oosten, in het midden van het Eldritch Woud, is een druïdische
  gaarde. Sinds het midden van de oorlog ontvangt de gaarde geen enkele
  onderhoud.

- De professor is een beetje angstig dat de achterstand van de gaarde negatieve
  effecten zou kunnen hebben op het woud. Ze vraagt Donna of ze de gaarde wil
  inspecteren, en enige onderhoud te doen.

- De gaarde staat bekend als de "oranjegaarde", vol met bloedsinaasappels.
  Druïdes waren er ooit aanwezig.

- Er is een herberg/bordeel (de Citrinevonk) iets buiten Arcanix die bekend
  staat voor honingwijn die "zo sterk is dat je enkel oranje zult zien voor een
  dag". Abalaba wil al enige tijd onderzoek doen naar deze honingwijn, maar zegt
  dat ze niet dood gevonden wil worden in zo'n dergelijke plek door een student.

- De madame van de Citrinevonk (Chère, changeling v, vaak duistere elf) kent de
  legende van IJzerkaak. Voor een prijs (50 sp) wil ze de legende wel vertellen.
  Ze merkt op dat ze denkt dat haar honingbijen richting de gaarde reizen voor
  hun nectar.

### De legende van IJzerkaak

De feeën... sommigen noemen ze heksen. Sommigen noemen ze kakelende oude dames
gebogen over verwarmde potten, een kokende bouillon aan het karnen. Proef het
brouwsel, en proef hun toorn. Of zijn het gevleugelde feeën die over de
weilanden sprinten? Pluche klootzakken zo ondeugend als vossen en zo vervelend
als muggen? Deze verhalen zijn slechts fantasieën---een mensgericht begrip om
beter te begrijpen hoe een fee-monster in wisselwerking staat met de materiële
wereld.

En deze fantasieën zijn dwaas.

De feeën spelen hun eigen spelletjes, en als ze zonder komen te zitten, reiken
ze naar onze wereld om te herbevoorraden. Ze zijn als goden in die zin: het
duwen van stervelingen naar hun eigen willekeur. Maar goden spelen geen
spelletjes in de hemel waar ze leven. Ze spelen in onze wereld. Feeën spelen
spelletjes met ons, maar ze slepen onze verwarde geesten terug naar hun rijk. De
goden huren om te bezitten. De feeën stelen.

Maar in tegenstelling tot de goden, kan de fee bloeden, wat ons naar dit
tandeloze wonder brengt:

Yanley de IJzerkaak verloor zijn tanden jaren geleden. Zijn tanden besloten om
een gevecht te beginnen met de vuist van een orc, en de vuist van de orc had een
natuurlijk voordeel. Als een bekwame smid maakte Yanley voor zichzelf een nieuwe
set ijzeren tanden die hem zijn bijnaam verdiende. Naast zijn vechtpartij met
een orc en zijn vaardigheden in de smederij, leidde IJzerkaak een zelden
onbewogen leven, bijna net zo onbewogen als een zinloze onderdaan. Maar toen hij
paden kruiste met een bosje bloedsinaasappelbomen, scheurde Yanley een gat in
het land van de feeën met gevolgen die het best als vreemd kunnen worden
omschreven.

Dit gat vol vreemde gevolgen begon met Skix, een bemoeizuchtige feeëngeest die
zijn kicks kreeg door te leven met sinaasappelbomen. Sinaasappelbomen trokken
Skix naar hun bosjes met een feromantische allure, en plaagden de geest van de
natuur met rijpend fruit en open huidmondjes. Er was een intimiteit tussen de
geest en de bomen, en feeën---instinct in haar zuiverste vorm---handelen nooit
met voorzichtigheid als er iets is dat hun verlangens prikkelt.
Feeënsensualiteit is geen broedende verleiding, zoals hoe succubi en incubi hun
prooi verleiden. Feeënsensualiteit is pure, schaamteloze adrenaline.

Dus Skix leefde met de bloedsinaasappels, en kreeg uiteindelijk de vorm van een
bloedsinaasappel. Net als in een sterfelijk huwelijk, plantte Skix zijn wortels
en vestigde zich met het bos. Onroerend, maar toch met elkaar verbonden, weefde
Skix zijn wortels en levenskracht in de naburige bomen, elke lente trillend in
extase wanneer de bijen en vlinders zwermden in hun wenkende bloemen. Zijn
bewustzijn rolde terug toen de dopamantische vloed van het leven morste in elke
spleet van het bloeiende bos, en bracht zaad en stuifmeel en nectar en nieuwe
geboorte de wereld in. Skix zwom in het paradijs, en de sinaasappels zwollen in
hitte jaar na jaar.

Na jaar...

Maar toen sijpelde de BRAND binnen! Een heet, harkend, oorverdovend schaafsel
dat Skix uit zijn botanische harem scheurde. Het ijzer BRANDde zijn ziel! Een
mens was zijn gaarde binnengedrongen! Elke sinaasappelboom was verbonden met het
bestaan van Skix, en elke seconde werd er een dodelijk stuk ijzer in Skix'
vleselijke vesting gebeten. Het was een man met ijzeren tanden die de chaos
veroorzaakte---het ijzer, een altijd vergiftigend metaal dat de gruwelijke dood
tot gevolg had---wat is erger dan de luxe te krijgen om het alleen maar te laten
BRANDen?

Voor Yanley de IJzerkaak was dit gewoon een andere boomgaard van fruitbomen. De
bloedsinaasappels hier smaakten bijzonder zoet, dus Yanley bleef zijn vulling
opeten, en de citrussappen morsten over zijn kin, en besprenkelden de grond met
pulp en zoetwater. Hij liet de bomen bij elke hap bloeden, en het gras onder
zijn voeten stolde van afschuw. Skix vocht om te ontsnappen aan de hel die
IJzerkaak onbewust had veroorzaakt met zijn middagsnack, maar Skix had zich te
diep opgerold. In voor- en tegenspoed kon zijn huwelijk met het bos niet worden
verbroken.

IJzerkaak, zijn maag nu gevuld, verliet het gemangde bosje. Skix kon alleen nog
maar schreeuwen in vergiftigde angst, voor altijd bloedend, maar nooit genoeg om
te sterven.

Hij schreeuwt en bloedt nog steeds.

Op dit moment.


