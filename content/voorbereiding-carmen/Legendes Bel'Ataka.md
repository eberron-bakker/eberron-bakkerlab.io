---
title: "Legendes Bel'Ataka"
date: 2020-08-06T18:15:21+02:00
draft: false
---

> Meer dan 3.000 jaar geleden heeft Bel'Ataka met een dozijn andere
> griffioenrijders de hoofdstad van Aerenal, Shae Cairndal, verdedigd tegen drie
> draken.

Als een Tairnadal-elf volwassen wordt, dan wordt zij gekozen door een voorouder.
Er vormt dan een bond tussen de elf en haar voorouder. Vanaf dat moment volgt de
elf in de voetstappen van haar voorouder, en leeft ze zoals haar voorouder deed.
Op deze manier wordt de elf de verpersoonlijking van de dode voorouder, en leeft
de voorouder voort.

Bel'Ataka werd echter nooit gekozen door een voorouder. Dit is een grote
schaamte onder de Tairnadal, omdat het aangeeft dat de elf niet waardig is voor
het herleven van haar voorouder. Bel'Ataka keek hier anders tegen aan. Zij
stelde dat ze bestemd was voor een groter lot dan het naleven van de eerbare
doden. Ze wilde haar eigen legende worden.

Bel'Ataka reisde naar Xen'Drik om een waarzegger te vinden. Diep in de jungle
vond ze een reus die haar toekomst zou kunnen vertellen, maar de reus vond haar
niet waardig.

Om haarzelf te bewijzen reisde ze naar een stam van duistere elven die
normaliter op reuzen jagen. Ze daagde de leider uit tot een duel. Te paard vocht
ze tegen de leider die op een reuzenschorpioen zat, en won.

Als beloning voor het winnen van het duel kreeg Bel'Ataka een grote spreukenrol
die de duistere elven van een reus gestolen hadden. De spreuk was onvolledig en
werd als waardeloos geacht.

Bel'Ataka bracht de spreukenrol naar de waarzeggerreus, en kreeg een profetie:
Bel'Ataka zou een van de meest legendarische elven van haar generatie worden,
maar zou op het hoogtepunt van haar legende verdwijnen. De reus voegde toe dat
de profetie afhankelijk was van een reis naar Thelanis, en een nieuwe oorlog
tussen Aerenal en Argonessen. Het hoogtepunt van Bel'Ataka's legende, volgens de
profetie, was het zelfstandig redden van Aerenal van volledige ondergang.

Bel'Ataka reisde naar Thelanis via Pylas Pyrial in Zilargo. In Thelanis vond ze
de opperfee van overwinning onder onmogelijke omstandigheden: Leylandra.
Leylandra was geïnteresseerd in de profetie van de reus, en ondersteunde
Bel'Ataka in haar voorbereidingen om de legende uit te voeren. Ze gaf Bel'Ataka
twee magische zwaarden. Tovenaars die de zwaarden zouden inspecteren zouden zien
dat de zwaarden immense magische potentie hadden. In feite was de magie een
illusie: De zwaarden hadden geen verdere magische eigenschappen.

Leylandra liet ook weten dat Bel'Ataka enige ondersteuning nodig zou hebben van
mede-elven. Toen Bel'Ataka Thelanis verliet, ging ze terug naar Xen'Drik om
duistere elven te rekruteren. Op weg naar Aerenal rekruteerde ze zee-elven, en
eenmaal in Aerenal rekruteerde ze elven van zowel de Tairnadal als het
Onsterfelijke Hof. Uiteindelijk had Bel'Ataka een twaalftal elven gerekruteerd
voor haar missie.

Met dit twaalftal elven ging Bel'Ataka op avonturen door heel Eberron: Ze
vochten tegen ijsmonsters in het verre noorden, versloegen zeemonsters in de
Donderzee, jachtten op demonen in de Demoonwoestenij, beëindigden wezens uit
Khyber in ondergrondse tunnels, en daagden draken uit in Argonessen.

Dat laatste was de profetie die zichzelf vervulde. Tijdens een avontuur in
Argonessen hadden Bel'Ataka en haar elven een draak verslagen, en de drie
kinderen van de draak wilden wraak. Om de dood van de draak te wreken, keerden
deze drie draken zich richting Aerenal.

Aerenal was niet voorbereid op een drakenaanval, en Bel'Ataka en haar twaalftal
konden net op tijd aanvliegen om de hoofdstad te verdedigen. In een fantastisch
luchtgevecht boven de hoofdstad bevochten de elven de drie draken, en vandaag
nog vertelt men verhalen over dit spektakel.

Net toen de laatste draak neerstortte op de grond, verdween Bel'Ataka. Ze werd
dood geacht, en niet een paar weken later waren er Tairnadal die haar als
voorouder wilden eren. In werkelijkheid was Bel'Ataka in Thelanis als hoge fee.
