---
title: "Sessie 3"
date: 2019-11-30T23:48:11+01:00
draft: false
---

- De sneeuw is gaan liggen tegen de tijd dat het gezelschap vertrekt. Er ligt
  nog een pak sneeuw (*difficult terrain*) voor een dag lang.

- Op de weg terug richting Riviertraan komt het gezelschap langs:

  + Een gewonde goblin (Zizgag). De goblin werd gewond toen hij aangevallen werd
    door Kinderen des Winters. Hij werd niet gedood, als uitdaging om sterker te
    worden. De goblin woont in een kleine nederzetting (Mucco Mog) van goblins
    een paar kilometer naar het zuiden. Hij was in het bos om op bezoek te gaan
    bij een vriendin (Bloem, *shifter* v) in de Stam van de Schemermaan. De
    laatste keer dat hij haar zag was al weer maanden geleden. Maar de Stam van
    de Schemermaan liet geen bezoekers toe, en hij moest terugkeren door de
    sneeuwstorm.

    Als het gezelschap de goblin heelt, dan nodigt hij de gezelschap uit om te
    overnachten bij de goblinnederzetting. Daar zal hij het gezelschap belonen
    met een *spreukscherf*, een gepolijste Eberronscherf die ook als boek dient.
    Hij heeft er zelf niets aan.

- Bij aankomst in Riviertraan is er een verandering: Er is meer activiteit aan
  de overkant van de rivier. Bouwers uit Passage zijn gearriveerd om de brug te
  repareren. Ze kwamen met zeilboten met materiaal (steen, hout). Tussen de
  bouwvakkers zitten magiewerkers, die *mold earth* gebruiken om blokken van
  steen langzaam te bewegen en te vervormen. Een boot is achtergebleven om als
  woonboot te dienen voor de bouwers. De andere boten zijn teruggekeerd.

- Het gezelschap vindt de krant: De Kroniek van Korranberg heeft als zijkop:
  "Eldeen-ent ziek, onbekende oorzaak". De Aundairse Spreuk, daarentegen, heeft
  dit als krantenkop: "Corruptie in Rivierwoud: zijn de Wachters van het Woud
  wel bekwame regeerders?"

- Het gezelschap komt erachter van Eenpoot dat Cédric d'Vadalis 200 gp betaald
  heeft voor het herstel van Wilgspruit. Dit is een lening.
