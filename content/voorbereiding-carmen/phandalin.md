---
title: "Phandalin"
date: 2020-10-05T13:04:21+02:00
draft: false
---

# Goblin ambush

- 4 goblins from the bushes.

- First attack is net attack (success means Entangled, Hardness 10, Athletics to
  escape)

# the rest

Honestly just play the rest as in the book.

## Goblin

**Attributes:** Agility d8, Smarts d6, Spirit d6, Strength d4, Vigor d6\
**Skills:** Athletics d6, Common Knowledge d10, Fighting d6, Notice d6,
Persuasion d4, Shooting d8, Stealth d10, Taunt d6\
**Pace:** 6; **Parry:** 5; **Toughness:** 5 (1)\
**Edges:** ---\
**Gear:** Shortsword (Str+d4), shortbow (Range 12/24/48, Damage 2d6), leather
armor (+1)\
**Special Abilities:**

- **Low Light Vision:** Ignore penalties for Dim and Dark Illumination.
- **Size -1**

## Wolf

**Attributes:** Agility d8, Smarts d6 (A), Spirit d6, Strength d6, Vigor d6\
**Skills:** Athletics d8, Fighting d6, Notice d10, Stealth d8\
**Pace:** 8; **Parry:** 5; **Toughness:** 4\
**Edges:** Alertness\ 
**Special Abilities:**

- **Bite:** Str+d4.
- **Size -1**
- **Speed:** d10 running die.

## Bugbear

**Attributes:** Agility d6, Smarts d4, Spirit d6, Strength d10, Vigor d8\
**Skills:** Athletics d10, Common Knowledge d4, Fighting d8, Intimidation d6,
Notice d6, Persuasion d6, Shooting d4, Stealth d10, Taunt d6\
**Pace:** 6; **Parry:** 6; **Toughness:** 9 (2)\
**Hindrances:** Arrogant\
**Edges:** Command\
**Gear:** Morningstar (Str+d6, AP 1), hide armor (+2)\
**Special Abilities:**

- **Low Light Vision:** Ignore penalties for Dim and Dark Illumination.
- **Reach 1**
- **Size 1**
