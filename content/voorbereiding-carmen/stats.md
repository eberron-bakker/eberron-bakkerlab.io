---
title: "Stats"
date: 2020-08-07T14:54:55+02:00
draft: false
---

### Ochre Jelly

**Attributes:** Agility d4, Smarts d4, Spirit d6, Strength d8, Vigor d8\
**Skills:** Athletics d6, Fighting d6, Notice d4, Stealth d4\
**Pace:** 2; **Parry:** 5; **Toughness:** 8 (2)/6 (0)/4 (-2)/2 (-4)\
**Edges:** ---\
**Special Abilities:**

- **Amorphous:** Can maneuver through any non-solid surface, pass through
  cracks in doors or windows, bubble through water, etc.
- **Ooze:** No additional damage from Called Shots; ignores 1 point of Wound
  penalties; cannot be blinded, deafened, or grappled; doesn't breathe; immune
  to disease, poison, lightning, and slashing.
- **Pseudopod:** Str+d10.
- **Size 2:** Ochre jellies are blobs the size of horses.
- **Split:** Instead of being damaged by lightning or slashing, the ochre jelly
  splits into two new jellies. New jellies are two Sizes smaller than the
  original.
- **Wall Walker:** Can move at its full pace on walls and ceilings.
