---
title: "Savage Worlds conversion"
date: 2020-04-06T19:53:25+01:00
draft: false
---

# Bast

## Race

Warforged

## Hindrances

- Wanted (Major)
- Vow (Minor)
- Slow (Minor)

## Attributes

- Smarts I (d6)
- Spirit I (d6)
- Strength II (d8)
- Vigor II (d8)

## Skills

- Athletics II (d8)
- Druidism II (d6)
- Fighting III (d8)
- Intimidation II (d6)
- Notice II (d6)
- Survival IIX (d8)

## Edges

- Brute (modified)

## Advances

1. Arcane Background (Druidism)
2. Improve Strength.

## Stat block

**Attributes:** Agility d4, Smarts d6, Spirit d6, Strength d10, Vigor d8\
**Skills:** Athletics d8, Druidism d6, Fighting d8, Intimidation d6, Notice d6,
Survival d8\
**Pace:** 5; **Parry:** 6; **Toughness:** 13 (6)\
**Gear:** Mossy stone (Str+d8, always hits with a raise), wand of birdcalls\
**Hindrances:**

- **Bulky:** Warforged subtract 2 from Trait rolls when using equipment that
  wasn't specifically designed for warforged. Equipment costs double the listed
  price for warforged. <!-- -2 -->
- **Factory Settings:** Warforged were designed, built, and trained for a single
  purpose. Their default knowledge and abilities outside of that purpose is
  limited. They do not have Common Knowledge, Notice, Persuasion, and Stealth as
  core skills. <!-- -4 -->
- **Loyal (Minor):** Warforged are trained to protect their allies in combat,
  and will always be the first to come to their rescue. They have the Loyal
  Hindrance. <!-- -1 -->
- **Outsider (Major):** With a long history of being dedicated soldiers of war,
  warforged have difficulty blending into post-war Khorvaire. Most non-warforged
  strongly dislike the sight of warforged as they stand as living reminders of
  the horror that was the Last War. As such, warforged have –2 to Persuasion
  rolls when interacting with people who look down on them. Furthermore,
  although the Treaty of Thronehold grants the warforged rights, not all places
  equally abide by that provision of the treaty. <!-- -2 -->
- **Quirk (Minor):** Warforged often display an odd personality trait or two,
  given how new they are to the world. They have the Quirk Hindrance. <!-- -1
  --> (Answers binary questions with a singular binary answer.)
- **Slow (Minor):** Bast's pace is reduced by 1, and his Running Die is a d4.
- **Vow (Minor):** Bast has sworn a vow of allegiance to the Wardens of the
  Wood.
- **Wanted (Major):** Aundair considers Bast a traitor from the Last War.

**Edges:**

- **Brawny:** Warforged tower over their human makers, granting them the Brawny
  Edge. Their Size (and therefore Toughness) increases by +1, and they treat
  their Strength as one die type higher when determining **Encumbrance** and
  **Minimum Strength** to use armour, weapons, and equipment without a penalty.
  <!-- +2 -->
- **Integrated Protection:** A warforged is covered in tough plating made of
  magically-treated woods and metals. Warforged gain Armor +6. Warforged are
  unable to wear additional armor. <!-- +2 -->
- **Living Construct:** Warforged are living constructs made from organic and
  inorganic matter and are capable of emotions and conscious thought. They do
  not need to breathe, eat, or drink, and are immune to poison and disease.
  Instead of sleeping, warforged must spend at least six hours in an inactive,
  motionless state. In this state, a warforged appears inert, but it doesn't
  render them unconscious, and they can see and hear as normal. Warforged add +2
  to recover from being Shaken and ignore one level of Wound modifiers. Wounds
  must be mended via the Repair skill or via magical means. Mending a warforged
  ignores the "Golden Hour". Each Repair attempt takes one hour per current
  Wound level. The powers *healing* and *repairing* both work on warforged. <!--
  +8 -->
- **Brute (modified):** Athletics and Fighting are linked to Strength instead of
  Agility.
- **Arcane Background (Druidism):** 10 power points, knows *entangle*,
  *healing*, and *smite*.

# Jes

## Race

Changeling

## Hindrances

- Curious (Major)
- Pacifist (Minor)
- Vow (Minor)

## Attributes

- Agility I (d6)
- Smarts II (d8)
- Spirit III (d10)

## Skills

- Common Knowledge I (d6)
- Druidism III (d8)
- Fighting I (d4)
- Healing III (d8)
- Notice I (d6)
- Persuasion II (d8)
- Shooting II (d6)
- Survival II (d6)

## Edges

- Arcane Background (Druidism)

## Advances

1. New Powers
2. Power Points

<!--
- Animal handling Y
- Charisma Y
- Survival Y
- Changeling Y
- Wild shape Y
- Hoop Y
- Heal Y
- Fog cloud Y
- Ice knife M
- Speak with animals Y
- Druidcraft Y (A World of Magic)
- Detect poison/disease M/Y
-->

## Stat block

**Attributes:** Agility d6, Smarts d8, Spirit d10, Strength d4, Vigor d4\
**Skills:** Athletics d4, Common Knowledge d6, Druidism d8, Fighting d4, Healing
d8, Notice d6, Persuasion d8, Shooting d6, Stealth d4, Survival d6\
**Pace:** 6; **Parry:** 5 (1); **Toughness:** 4\
**Gear:** Kythrian manchineel staff (Str+d4, Parry +1, Reach 1, two hands; Range
12/24/48, Damage 2d6+1), shiftweave\
**Hindrances:**

- **Outsider (Minor):** When it is discovered that someone is a changeling, they
  are sometimes shunned. Changelings subtract 2 from Persuasion rolls with
  people who do not trust changelings, provided that their identity as a
  changeling is known. <!-- -1 -->
- **Curious (Major):** Jes wants to know everything.
- **Pacifist (Minor):** Jes only fights in self-defence.
- **Vow (Minor):** Jes has sworn a vow of allegiance to the Wardens of the Wood.

**Edges:**

- **Change Appearance:** Changelings can alter physical appearance as an Action.
  They can assume the appearance of another person of the same Size and shape,
  although clothing and gear is not affected. If unconscious or dead, they
  return to their natural form. <!-- +3 -->
- **Arcane Background (Druidism):** 15 power points, knows *beast friend* (speak
  and control a beast), *detect/conceal arcana*, *healing* (cure wounds, cure
  disease/poison), *light/darkness* (light, fog cloud), and *shape change* (wild
  shape).

# Hoop

**Attributes:** Agility d8, Smarts d6 (A), Spirit d6, Strength d4-3, Vigor d6\
**Skills:** Athletics d6, Notice d10, Stealth d8, Survival d4\
**Pace:** 3; **Parry:** 2; **Toughness:** 1\
**Special Abilities:**

- **Bite/Claws:** Str.
- **Flight:** Hoop flies at a Pace of 36".
- **Low Light Vision:** Hoop ignores penalties for Dim and Dark Illumination.
- **Size -4 (Tiny)**

# Grinvor

## Race

Halfling

## Hindrances

- Mild Mannered (Minor)
- Vow (Minor)

## Attributes

- Agility I (d6)
- Smarts I (d6)
- Spirit II (d10)
- Vigor I (d6)

## Skills

- Common Knowledge I (d6)
- Fighting I (d4)
- Focus III (d8)
- Notice I (d6)
- Performance IIII (d10)
- Persuasion II (d8)
- Riding I (d4)
- Taunt II (d6)

## Edges

- Arcane Background (Gifted)

## Advances

1. New Powers
2. New Powers

## Stat block

**Attributes:** Agility d6, Smarts d6, Spirit d10, Strength d4, Vigor d6\
**Skills:** Athletics d4, Common Knowledge d6, Fighting d4, Focus d8, Notice d6,
Performance d10, Persuasion d8, Riding d4, Stealth d4, Taunt d6\
**Pace:** 5; **Parry:** 4; **Toughness:** 5\
**Gear:** Shortsword (Str+d4)\
**Hindrances:**

- **Reduced Pace:** Decrease the character's Pace by 1 and their running die one
  die type. <!-- -1 -->
- **Size -1:** Halflings average only about three feet tall, reducing their Size
  (and therefore Toughness) by 1. <!-- -1 -->
- **Mild Mannered (Minor):** Grinvor is far too friendly. He subtracts 2 when
  making Intimidation rolls.
- **Vow (Minor):** Grinvor has vowed to make people happy.

**Edges:**

- **Luck:** Halflings draw one additional Benny per game session. <!-- +2 -->
- **Spirited:** Haflings are generally optimistic beings. They start with a d6
  in Spirit instead of a d4. This increases maximum Spirit to d12 + 1. <!-- +2
  -->
- **Arcane Background (Gifted):** 15 power points, knows *confusion* (vicious
  mockery), *empathy*, *illusion*, *stun* (hideous laughter), and *unseen
  servant*.
