---
title: "Lhazaar"
date: 2020-01-03T18:45:29+01:00
draft: false
---

## Achtergrond

De piraten van de Lhazaar Prinsdommen willen al heel lang toegang tot de
luchtschepen van Huis Lyrander. Huis Lyrander, op zijn beurt, wil niets liever
dan het uitschakelen van de concurrentie vanuit de Prinsdommen.

Onlangs is een luchtschip van Lyrander neergestord in Valenar. Het luchtschip
was onderweg van Schoonhaven richting Aerenal. Aan boord waren ongetwijfeld
mensen die zo'n trip konden betalen, en misschien hun rijkdommen. Maar veel
belangrijker: aan boord van het schip is een magisch stuurwiel die het maken van
een eigen luchtschip mogelijk kan maken.

Zowel de Zeedraken van Prins Ryger uit Vorstenpoort als de Wolkenwrekers van
Prinses Mika uit Poort Krez willen zich het neergestorte wrak toeeigenen.

De leden van het gezelschap zijn piraten aan boord van het Vliegende Fortuin.

## Begin

Het gezelschap is in een bar in Poort Krez. Ze hebben onlangs een magisch
apparaat uit Zilargo gestolen dat als telegraaf dient. Het vangt magische
signalen op van de Kroniek van Korranberg, en schrijft de signalen langzaam op
een papier. De signalen gebeuren een keer per dag.

Begin het spel met een gevecht. Niet lang geleden had k'Lung valsgespeeld in een
kaartspel. Drie piraten komen de bar binnen met kromsabels. Eén roept: "Daar is
die glipperige kikker! Je bent me 10 goudstukken tegoed, kwak!"

Intentie: Simpel gevecht tussen k'Lung en de drie piraten om het systeem te
introduceren.

Na het gevecht haalt Silvia de telegraaf op. Ze leest voor:

> Conflict op grens tussen Eldeen en Aundair. Spanningen stijgen na
> vergeldingsactie van druïdische extremisten. Aundair stuurt troepen om grens
> te verdedigen. Eldeen weigert commentaar.

Fernando is ongeïnteresseerd in dit nieuws.

Volgende artikel:

> Lyrander-luchtschip neergestord in Valenar. Een schip van Schoonhaven richting
> Aurenal is neergestord in de zuiderse jungles van Valenar. Het is onbekend
> hoeveel passagiers aan boord waren. Huis Lyrander houdt vol dat haar schepen
> uiterst veilig zijn, maar zal een onderzoek beginnen naar de oorzaak. Huis
> Tharashk begint expeditie om wrak te vinden.

Fernando is meteen geïnteresseerd in dit nieuws. Hij wil het wrak vinden voordat
Huis Tharashk het kan vinden, zodat hij zelf een luchtschip zou kunnen hebben.
Fernano gaat naar Prinses Mika Steengezicht, en vertrekt daarna meteen met de
bemanning.

# Onderweg

- Het schip moet stoppen in Nieuwtroon voor voedsel.

- Onderweg naar Nieuwtraan: Aanval door 4 sahuagin, 1 sahuagin sorcerer (WC), 1
  haai (WC). De sahuagin willen de rijkdommen van het schip, omdat het schip
  zonder toestemming door hun territorium gaat. Als de sorcerer dood is trekken
  ze terug.

- Het duurt 1 maand om in Nieuwtroon te arriveren.

- In Nieuwtroon neemt de bemanning een dag om te rusten.
  + Bepaal hoeveel ieder drinkt in een bar in Nieuwtroon. Als gemiddeld/veel,
    dan Fatigued/Exhausted.
  + Als bemanning terugkomt van de bar, dan worden ze overvallen vanuit hun
    eigen schip. Droop, die achter gebleven was, is in touwen gewikkeld.
  + Ze worden aangevallen door de bemanning van de Sirene. Zes piraten + Regen.

- Het duurt 20 dagen om van Nieuwtroon naar de zuiderse jungle van Valenar te
  reizen.

- ... Door de jungle reizen. Veel kleine beekjes met water in de jungle.

- Gezelschap stopt ergens bij een beekje in de jungle. k'Lung heeft dit nodig.

- Eenmaal aangekomen bij het neergestorte wrak:
  + Veel van het wrak is zwart van roet en as. Een gedeelte van de bebossing om
    het wrak heen is weggebrand.
  + Er liggen lijken rond het wrak. Als het gezelschap de lijken onderzoekt,
    vinden ze één half-orc met het merk van vondst (Huis Tharashk). De half-orc
    heeft derde-graads brandwonden.
  + Andere lijken zijn ook verbrand, maar het gezelschap kan wel waardevolle
    voorwerpen vinden.
  + Als het gezelschap het wrak onderzoekt, vinden ze een fire elemental. De
    fire elemental doet niet veel anders dan dingen in de vik zetten. Wil ook
    het gezelschap in de vik zetten.
  + Gevecht met fire elemental + 8 smoke mephits.
  + Er is een beekje op 20 meter afstand van het wrak!

# Stats

## Pirate

**Attributes:** Agility d6, Smarts d4, Spirit d6, Strength d6, Vigor d6\
**Skills:** Athletics d6, Boating d8, Common Knowledge d4, Fighting d6, Gambling
d6, Intimidation d4, Notice d6, Persuasion d4, Shooting d6, Stealth d4, Taunt
d6, Thievery d6+1\
**Pace:** 6; **Parry:** 5; **Toughness:** 6 (1)\
**Hindrances:** Wanted (Major)\
**Edges:** Thief (+1 to Athletics to climb and Stealth in urban environments)\
**Gear:** Leather armor (+1), Scimitar (Str+d8)

Some pirates have Repair d6, Healing d6, and/or Survival d6.

## (Wild Card) Pirate Captain

**Attributes:** Agility d8, Smarts d6, Spirit d10, Strength d6, Vigor d6\
**Skills:** Athletics d8, Boating d10, Common Knowledge d6, Fighting d8,
Gambling d6, Intimidation d8, Notice d6, Persuasion d10, Shooting d8, Stealth
d6, Taunt d8, Thievery d6\
**Pace:** 6; **Parry:** 7 (1); **Toughness:** 7 (2)\
**Hindrances:** Wanted (Major)\
**Edges:** Natural Leader, Command (+1 to Shaken rolls)\
**Gear:** Studded leather armor (+2), rapier (Str+d4, Parry +1)

## (Wild Card) Regen

**Attributes:** Agility d8, Smarts d6, Spirit d10, Strength d6, Vigor d6\
**Skills:** Athletics d8, Boating d10, Common Knowledge d6, Fighting d8, Focus
d8, Gambling d6, Intimidation d8, Notice d6, Persuasion d10, Shooting d8,
Stealth d6, Taunt d8, Thievery d6\
**Pace:** 6; **Parry:** 6; **Toughness:** 7 (2)\
**Hindrances:** Wanted (Major)\
**Edges:** Natural Leader, Command (+1 to Shaken rolls)\
**Gear:** Studded leather armor (+2), ijszwaard (Str+d6)\
**Special Abilities:**

- **Magic:** 20 Power Points, and knows *burst* (2), *damage field* (4),
  *entangle* (2), and *smite* (2). Trapping is ice.

## Sahuagin

**Attributes:** Agility d6, Smarts d4, Spirit d4, Strength d6, Vigor d6\
**Skills:** Athletics d8, Common Knowledge d4, Fighting d6, Notice d8,
Persuasion d4, Shooting d6\
**Pace:** 6; **Parry:** 6 (1); **Toughness:** 5\
**Edges:** ---\
**Gear:** Spear (Str+d6, Reach 1, Parry +1)\
**Special Abilities:**

- **Aquatic:** Pace 8.
- **Bite:** Str+d4.
- **Claws:** Str+d6.
- **Low Light Vision:** Ignore penalties for Dim and Dark Illumination (but not
  Pitch Darkness).
- **Blood Frenzy:** Sahuagin add +2 damage when attacking creatures that have a
  Wound.
- **Shark Telepathy:** Sahuagin can magically command any shark within 120 feet
  of it, using a limited telepathy.

Some sahuagin can cast spells. They have 10 Power Points, Spellcasting d6, and
know *blast* (3), *havoc* (2), and *healing* (3). Trapping is water.

## Fire Elemental

**Attributes:** Agility d12+1, Smarts d8, Spirit d8, Strength d4, Vigor d6\
**Skills:** Athletics d8, Fighting d10, Notice d6, Shooting d8, Stealth d6\
**Pace:** 6; **Parry:** 7; **Toughness:** 7\
**Edges:** ---\
**Special Abilities:**

- **Size 2**
- **Elemental:** No additional damage from Called Shots, ignores 1 point of
  Wound penalties, doesn't breathe, immune to disease and poison.
- **Fiery Touch:** Str+d6, chance of catching **Fire** (page 127; roll 1d6; on
  6, on fire; 1d6 damage; roll d6 after damage to determine whether damage
  increases).
- **Flame Strike:** Can project a searing blast of flame using the Cone Template
  and their Shooting skill. Characters within take 3d6 damage and may catch
  **Fire** (page 127; see above). May be Evaded.
- **Immunity:** Fire elementals are immune to fire and heat-based attacks.

## Smoke Mephit

**Attributes:** Agility d8, Smarts d4, Spirit d6, Strength d4-2, Vigor d4\
**Skills:** Athletics d4, Fighting d6, Notice d4, Stealth d8\
**Pace:** 3; **Parry:** 5; **Toughness:** 1\
**Edges:** ---\
**Special Abilities:**

- **Size -3 (Very Small)**
- **Flight:** Mephits fly at a Pace of 8".
- **Elemental:** No additional damage from Called Shots, ignores 1 point of
  Wound penalties, doesn't breathe, immune to disease and poison.
- **Fiery Touch:** Str+d6, chance of catching **Fire** (page 127; roll 1d6; on
  6, on fire; 1d6 damage; roll d6 after damage to determine whether damage
  increases).
- **Fire Resistance:** Smoke mephits receive a +4 bonus to resist fire. Damage
  from fire is also reduced by 4.
- **Death Burst:** When the mephit dies, it leaves behind a cloud of smoke using
  the Small Burst Template. The smoke provides Medium Cover (-4), and lasts for
  1 minute.
