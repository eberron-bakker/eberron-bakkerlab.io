---
title: "Notities Korranberg"
date: 2020-07-25T14:53:30+02:00
draft: false
---

- Ze zijn Wachters van het Woud.

  + Grinvor.
  + Anoniem.

- Aundair heeft Eldeen aangevallen.

- Er leek een fout te zijn met de brug. Onduidelijk wat er is gebeurd. Het leek
  alsof de overkant gelijk de aanname nam dat het een aanval was van de
  Wachters. Geen van de Wachters heeft een aanval gepland op de brug.

- De ent (Wilgspruit) is veilig. Gezien de spanningen is de ent vooraf in
  veiligheid gebracht, omdat de veiligheid niet gewaarborgd kon worden.

- De Wachters weten niet waarom de brug opgeblazen is.

- De soldaten van Aundair hadden geteleporteerd naar de zijkant van de tempel.
  Ze leken een aanval te plannen op de tempel. Als verdediging werden de
  soldaten het water in geblazen, maar per ongeluk schade aan de tempel.

- Aundair was stellig op de overname van de Eldeenzijde van Riviertraan.

- De Wachters weten niet of er burgerslachtoffers zijn geweest aan de kant van
  Aundair. Zoja, dan betreuren de Wachters dan.

- De Wachters gedogen het vernietigen van een tempel in Wyr door de
  Eldeen-wolven niet.

- De Wachters beschouwen het conflict als een provocatie van Aundair.

- De Wachters hopen geen verdere actie te hoeven nemen. Ze willen de vrede
  bewaren.

- De Wachters zullen niet de grens van Aundair over om een gevecht aan te gaan.

- De Wachters zullen---net als jaren geleden---opkomen als Aundair het conflict
  escaleert.

- De Wachters hebben geen contacten met andere naties.

- De Wachters doen een oproep om de vrede te bewaren, om als één volk naast
  elkaar te kunnen bestaan. Ze vragen om solidariteit van de lezers voor de
  vrijheid die aan ieder toebehoort.

- De Wachters van de Woud hebben tijdens de strijd de soevereiniteit van
  Aundairs grondgebied gerespecteerd.
