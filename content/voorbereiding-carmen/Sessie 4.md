---
title: "Sessie 4"
date: 2019-12-15T11:58:44+01:00
draft: false
---

- Cédric wil iets terug voor de lening van 200 gp. Hij wil toegang hebben tot
  een exotische diersoort: Een hippogrief, een groot zeepaard, een pegasus, een
  grote schilpad, een Valenarpaard, een uilbeer.

- Als Cédric om een Siberysscherf gevraagd word, dan moet hij eventjes lachen.
  Siberysscherven zijn best wel duur; van 100 gp tot duizenden goudstukken. Hij
  heeft wel Siberysscherven, maar wil ze niet wegdoen. Daarnaast voegt hij toe
  dat Siberysscherven alleen effectief gebruikt kunnen worden door mensen met
  drakenmerken.

- Eenpoot suggereert dat het gezelschap zo veel mogelijk gasten van het
  herfstfeest ondervraagt. Hij wil tijdens het lentefeest (1 Eyre) in gesprek
  gaan met alle aanwezigen van het herfstfeest. Het is aan het gezelschap om de
  gasten opnieuw uit te nodigen, en om ondertussen onderzoek te doen. Het
  lentefeest zal plaatsvinden in Riviertraan.

- Eenpoot stelt voor om het gezelschap te trainen ter voorbereiding van hun
  avontuur. Omdat de reis lang zal zijn langs de verscheidene partijen---en niet
  altijd even veilig---zal het gezelschap veel hebben aan wat extra
  vaardigheden:
  
  + Lin krijgt te leren hoe ze in dieren kan veranderen.
  + Bast krijgt te leren hoe hij enkele simpele magiespreuken kan uitvoeren. Hij
    zal waarschijnlijk niet alles onthouden, maar goed: Alarm, Animal
    Friendship, Cure Wounds, Ensnaring Strike, Hunter's Mark, Speak with
    Animals, 
  + Grinvor krijgt les van Pinoki.

- De training duurt 2 weken (tot ongeveer 998-01-24).

- De training wordt twee keer onderbroken:
 
  + Eén keer wanneer de wachters uit Varna terugkeren met een Jorasco-halfling.
    De heling van Wilgspruit is succesvol, maar langzaam. Wilgspruit begint een
    winterslaap te doen.

    + De halfling, Kella (v), moet blijven om Wilgspruit te helpen. Dit gaat nog
      meer goud kosten.

  + Later wanneer de Eldeen-wolven arriveren in Riviertraan.

    + Ze willen de Tranentempel afbranden en vernietigen.

    + Wanneer Eenpoot probeert in te grijpen, maken ze hem uit voor verrader. Ze
      vallen Eenpoot echter niet aan.

    + Wanneer Bast probeert in te grijpen roept iemand "monster!" en valt hem
      aan. Het is een critical hit. Zijn hand wordt eraf gehakt.

    + Wanneer ze de bouwvakkers zien, is alles opeens de schuld van Aundair, die
      Eldeen wil aanvallen. Ze duwen de bouwvakkers en hun materiaal de rivier
      in en beginnen de brug te slopen.
