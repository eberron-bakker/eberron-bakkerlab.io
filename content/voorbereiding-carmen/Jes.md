# Jes

**Attributes:** Agility d6, Smarts d8, Spirit d10, Strength d4, Vigor d4\
**Skills:** Athletics d4, Common Knowledge d6, Druidism d8, Fighting d4, Healing
d8, Notice d6, Persuasion d8, Riding d4, Shooting d6, Stealth d4, Survival d6\
**Pace:** 6; **Parry:** 5 (1); **Toughness:** 4\
**Gear:** Kythrian manchineel staff (Str+d4, Parry +1, Reach 1, two hands; Range
12/24/48, Damage 2d6+1), dagger (Range 3/6/12, Damage Str+d4)\
**Hindrances:**

- **Curious (Major):** Curiosity killed the cat, and it might kill your
  character as well. Curious characters want to check out everything and always
  want to know what's behind a potential mystery or secret.
- **Outsider (Minor):** When it is discovered that someone is a changeling, they
  are sometimes shunned. Changelings subtract 2 from Persuasion rolls with
  people who do not trust changelings, provided that their identity as a
  changeling is known. In some communities, when a changeling's identity is
  revealed, the changeling is harshly---sometimes violently---ousted.
- **Pacifist (Minor):** Your character despises violence. She will only fight
  when given no other choice, and never allows the killing of prisoners or
  defenceless victims. Evil creatures are fair game, however.
- **Vow (Minor):** Jes has sworn a vow of allegiance to the Wardens of the Wood.

**Edges:**

- **Arcane Background (Druidism)**
- **Change Appearance:** Changelings can alter physical appearance as an Action.
  They can assume the appearance of another person of the same Size and shape,
  although clothing and gear is not affected. If unconscious or dead, they
  return to their natural form.
- **Changeling Instincts:** Changelings start with a d6 in Persuasion instead of
  a d4. This increases maximum Persuasion to d12 + 1.

**Powers:**

- **Beast friend (p. 155):** PP Size; Range Smarts; Duration 10 minutes; Speak
  and control animals. Cost is cumulative Size, 1 per animal.

  This spell allows an individual to speak with and guide the actions of nature's beasts. The cost to cast is equal to the sum of their Size (min. 1 per creature).

  Success means the creatures obey simple commands, like a well-trained dog. A raise on the arcane skill roll means the beasts are more obedient.

  **Modifiers:**

  - **Mind rider (+1):** The caster can communicate and sense through any of the
    beasts she has befriended.
- **Detect/conceal arcana (p. 158):** PP Size; Range Smarts; Duration 5 (detect)
  / 1 hour (conceal); Detect or conceal arcana.

  Detect arcana allows the recipient to see and detect all magical persons,
  objects, or effects in sight. This includes invisible foes, enchantments, and
  so on. With a raise, the caster knows the general type of enchantment.

  Detect arcana allows the character to ignore 4 points (or all points with a
  raise) of penalties when attacking foes hidden by magic.

  Conceal arcana prevents detection of arcane energies one one being or item of
  Normal Scale (p. 179).

  Detect vs. Conceal: Detecting arcana against something that has been concealed
  is an opposed roll of arcane skills. If the concealment wins, the character
  cannot see through the ruse with this casting, but can attempt again on a next
  turn.

  **Modifiers:**

  - **Additional Recipients (+1):** The power may affect more than one viewer
    for detect, or item for conceal, for 1 additional Power Point each.
  - **Area of Effect (+1/+2):** Conceal only. The power affects everything in a
    Medium Blast Template for +1 points, or a Large Blast Template for +2
    points.
  - **Strong (+1):** Conceal only. Detection rolls to see through the
    concealment are made at -2.
- **Healing (p. 162):** PP 3; Range Touch; Duration Instant; Remove one Wound,
  or two with a raise. Victim's Wounds penalty to Druidism.

  Healing removes Wounds less than an hour old. The penalty to the caster's
  arcane skill roll is the victim's Wounds (max -3). A success removes one
  Wound, and a raise removes two. The power may be cast additional times to
  remove additional Wounds.

  **Modifiers:**

  - **Greater Healing (+10):** Can restore any Wound, including those more than
    an hour old.
  - **Crippling Injuries (+20):** Heal a permanent Crippling Injury (p. 95).
    Requires an hour of preparation, and can only attempt once per injury. If it
    fails, this caster cannot heal that particular injury. If successful, the
    subject is Exhausted for 24 hours.
  - **Neutralize Poison or Disease (+1):** Successful roll negates any poison or
    disease. If the poison or disease has a modifier, apply it to the arcane
    skill roll.
- **Light/fog (light/darkness, p. 164):** PP 2; Range Smarts; Duration 10
  minutes; Illuminate or block illumination in a Large Blast Template.

  Light creates bright illumination in a Large Blast Template. With a raise, the
  light can be focused into a 5" beam as well.

  Darkness blocks illumination in a Large Blast Template, making the area Dark
  (-4), or Pitch Darkness (-6) with a raise (see p. 102).

  If light and darkness overlap, they create a patch of Dim light (-2).

  **Modifiers:**

  - **Mobile (+1):** The caster can move the area up to her arcane skill die
    type each round after casting, or attach it to an inanimate object when
    first cast.
- **Wild shape (shape change, p. 166):** PP 3/5/8/11/15; Range Self; Duration 1
  hour; Turn into a beast, Size depends on Power Points. On a raise, increase
  Strength and Vigor.

  The caster turns into an animal.

  Novice, Cost 3, Size -4 to -1\
  Seasoned, Cost 5, Size 0\
  Veteran, Cost 8, Size 1 to 2\
  Heroic, Cost 11, Size 3 to 4\
  Legendary, Cost 15, Size 5 to 10

  With a raise, the character turns into a strong version of the animal—increase
  its Strength and Vigor by one die type each.

  Weapons and other personal effects are assumed into the animal's form and
  reappear when the power ends, but other objects are dropped.

  While transformed, the character retains her own Smarts, Spirit, Hindrances,
  Edges, and linked skills. She gain's the animal's Agility, Strength, Vigor,
  and linked skills. She cannot talk and cannot activate new powers.

  **Modifiers:**

  - **Speech (+1):** The recipient retains speech, but still cannot activate new
    powers.

**Gear:**

- Flask of everburning oil TODO

**Advances:**

1. New Powers
2. Power Points

# Hoop

**Attributes:** Agility d8, Smarts d6 (A), Spirit d6, Strength d4-3, Vigor d6\
**Skills:** Athletics d6, Notice d10, Stealth d8, Survival d4\
**Pace:** 3; **Parry:** 2; **Toughness:** 1\
**Special Abilities:**

- **Bite/Claws:** Str.
- **Flight:** Hoop flies at a Pace of 36".
- **Low Light Vision:** Hoop ignores penalties for Dim and Dark Illumination.
- **Size -4 (Tiny)**