---
title: "Sessie 26"
date: 2020-10-02T04:54:41+02:00
draft: false
---

# Onderweg

- Gezelschap komt een groep Aundairiërs tegen. Er zijn enkele soldaten onder de
  Aundairiërs die een karavaan verdedigen. Het zijn twee karren, waarvan één een
  volledig kappot wiel heeft. De karren vervoeren magisch bewerkt hout van
  Arcanix richting Passage voor de brug in Riviertraan.

- De Aundairiërs kijken een beetje raar naar het Eldeense gezelschap.

- Ze hebben hout nodig om het wiel te repareren. Ze kunnen het magisch bewerkte
  hout niet gebruiken. Ze willen het bos wel ingaan om een boom neer te hakken,
  maar ze kennen te veel verhalen van nare dingen in het Eldritchwoud: Boze
  druïdegeesten, verspoorde feeën, een monster met ijzeren tanden, een dryade
  die alleen maar problemen veroorzaakt, en wilde hongerige beesten.

- Ze hebben een persoon (Francisca, mens v) teruggestuurd naar Arcanix om een
  wielvervanging te vinden, maar ze is al twee dagen weg. Francisca was de
  "nieuweling" tijdens de reis.

- Margot (mens v) is de sergeant van de wacht, en de praatpaal voor het
  gezelschap.

- De Aundairiërs vragen het gezelschap om hulp: Óf het wielrepareren, óf
  Francisca vinden.

  + Het wiel heeft creativiteit nodig.
  + Francisca bestaat niet. Niemand in Arcanix heeft ooit van Francisca gehoord.

- Als het gezelschap niet helpt, dan verliest iedereen magisch 200 sp.

- Als het gezelschap wel helpt, dan verdient iedereen 500 sp.

- De encounter was een manifestatie van de Reiziger. Common Knowledge roll.

# Vervolg

- Eén kilometer rond de gaarde:

> De bomen zijn niet dood. Dood betekent dat leven ooit door hen heen liep. Deze
> wetteloze schillen verwelken alsof het leven ze nooit gezegend heeft. Ze zijn
> een ongeboren fout---een woud van lijkenloze geesten. Hun takken dansen de
> wals van de galg terwijl de wind door hen heen sijpelt.

| Resultaat | Tekst |
|:--:|:---|
| 10-14 | De lucht om je heen ruikt naar niets, alsof de bomen zelf bang zijn om te leven. Het leven is pijn voor hen. |
| 15-19 | De bron van hun pijn is nog steeds in de buurt. Visioenen ervan dansen om je heen, en een geur van pittige sinaasappels weigert je neus te verlaten.|
| 20-24 | Ze spreken tot je, deze beeltenissen van de wanhoop. "Hij had ijzer in zijn tanden, en hij liet de fee branden", zeggen ze. "Hij bloedt nog steeds. Hij schreeuwt nog steeds. Hij heeft de glimlach uit de wind gedreven en heeft hem met angst gestuit. O, vreemdeling, hoe sterft het levenloze als de pijn dat niet wil?" |
| 25+ | "Dood het, als het moet", spreken de bomen. "Dood het, als het moet!" hun kreten worden sterker. "Dood het, als het moet!" Jouw geest stort zich in een opgerolde doodsstrijd. "DOOD HET! HET MOET!" en alles wat overblijft is stilte, en visioenen van een zielloze mond. |

- Aangekomen:

> Als een omgekeerde van de vorige bomen, kon deze gaarde sinaasappelbomen niet
> gezonder zijn. In tegenstelling tot de traditionele vruchtdragende bomen
> groeien bloesems en vruchten van deze bomen tegelijkertijd, de witte bloei is
> zo helder als zijden doek en de vruchten zwellen op als bubbelbellen die op
> het punt staan te barsten.

- Dribbel eet van de vrucht.

- Mond:

> Een mond, vreemd genoeg menselijk, rust in de grond. Dit open monster---drie
> meter in diameter---beeft alsof zijn kaak klaar is om uit te klappen. Een
> stille, oneindige schreeuw tiert over de mond alsof hij al eeuwenlang
> schreeuwt. De tanden zijn roetig van het vuil, en hoewel er geen enkele
> ademtocht ontsnapt aan zijn muil, balgt de geur van citrusrot uit dit gapende
> gat in de grond.

- Slablaadje kan zichzelf opofferen om de nieuwe beschermer te worden van de
  oranjegaarde. Hij plant zich in het gat, en begint te bloeien. Langzaam
  verschijnt een bloedsinaasappel op één van de takjes van Slablaadje.

# Terugkomst

- Bij terugkomst in Arcanix wordt het gezelschap uitgenodigd naar de kamer van
  Adal.

- In plaats van Adal vindt het gezelschap Leselor achter zijn bureau zitten.

- Leselor was vrijgekomen door losgeld. Ze is benieuwd hoeveel er voor haar
  betaald is, maar dat weet ze niet.

- Leselor vertelde niets aan de Wachters van het Woud.

- Leselor geeft de Eberronscherf aan het gezelschap.

- Leselor zegt dat ze graag in Groenhart komt, en dat ze de agressie betreurt.
  Oorlog is zó uit de tijd.

- Gelukkig heeft ze het mysterie opgelost voor het gezelschap, en kunnen ze weer
  goede vrienden zijn. Onderzoek heeft laten doen blijken dat Fhazira schuldig
  was aan de poging op Wilgspruit.

  + Sinds de calamiteiten in Riviertraan volgen mensen van Aundair belangrijke
    personen in Riviertraan. Leselor was verantwoordelijk voor het volgen van de
    Wilgenwachters.

  + Een andere persoon volgde Fhazira, en vond lange tijd niets. Echter, op een
    dag kon hij Fhazira nergens vinden nadat ze haar tempel binnentrad. Een dag
    daarna ging hij op onderzoek uit, en vond hij een ondergronds complex
    beneden de tempel. In deze kerker vond hij religieuze voorwerpen, niet voor
    de Negen Soevereinen, maar voor Khyber. Daarnaast vond hij een zak vol met
    Khyberscherven waar de Wilgenwachters het over hadden.

- Nu, hier is het probleem: Aundair heeft in haar bezit zowel de oorzaak als de
  oplossing voor de boomziekte. Dit is heel erg vervelend, want het betekent dat
  Aundair niet anders kán dan het inzetten van de Khyberscherven. Eldeen is
  immers openlijk aan het rebelleren, en Eldeen is eigenlijk grondgebied van
  Koningin Aurala.

- Om die reden heeft ze een voorstel: In plaats van het conflict oplossen met
  geweld en magische ziektes, kan het conflict ook opgelost worden via een
  diplomatische manier. 

- Om die reden heeft ze een voorstel: In plaats van het conflict oplossen met
  geweld, kan het probleem opgelost worden met pen op papier. Als de
  Wilgenwachters hiervoor kunnen zorgen, dan wordt veel leed voorkomen.
  
# Ondertussen in Riviertraan

- Fhazira werd gewaarschuwd door Aundair om te vertrekken richting Schoonhaven
  voor haar eigen veiligheid.

- Sommige werkers van Aundair hebben een *nieuwe* ondergrondse kerker gemaakt
  onder de Tranentempel.
