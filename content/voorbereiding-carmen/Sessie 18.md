---
title: "Sessie 18"
date: 2020-07-24T01:24:46+02:00
draft: false
---

- FOUTJE: Palai én Lifra hadden niet geantwoord op uitnodiging voor zomerfeest.

- Gevecht is net over.

  + Resultaat: Gelijkspel, beide zijden aan de eigen kant van de rivier.

- Mumzok, Eenpoot, en het gezelschap zijn gewond.

- Dingen die moeten gebeuren:

  + Voorbereiding van verdediging tegen Aundair.

  + Bericht sturen naar de Wachters van het Woud over de aanval van Aundair.
    Probleem: Sivis-steen is aan de Aundair-kant van Riviertraan.

  + Helen.

  + De strijdgestegenen vinden.

  + Gevangenen uitwisselen met Aundair?

- Een journalist (Selbi, gnoom v) van de Korranberg Kroniek komt op het
  gezelschap af en vraagt hen *alles* over het gevecht. Belangrijkste vragen:

  + (NOTITIE AAN ZELF: SCHRIJF ANTWOORDEN OP)
  + Wat zijn jullie namen?
  + Wat gebeurde er volgens jullie?
  + Waar is de ent?
  + Wie blies de brug op?
  + Wie beschadigde de Tempel van de Negen Soevereinen?
  + Gedoogt Eldeen de acties van de Eldeen-wolven?
  + Hoe beschouwen de Wachters van het Woud dit conflict?
  + Wat zullen de Wachters van het Woud ondernemen?
  + Wat zullen de Wachters van het Woud doen als Aundair dit conflict escaleert?
  + Is dit het einde van het Interbellum?

- Vlotte boodschapper komt van strijdgestegenen naar Riviertraan. Boodschap:
  "Ontmoet in grot in Rivierwoud. Bevestig bericht."

# Strijdgestegenen

- Onderweg naar grot: Gevecht aan rivier tussen 1 krokodil en 3 chuuls.

> Een geelgroene kreeft met vier lange poten, twee grote klauwen, een sterk
> beschermend exoskelet, een waaierstaart en een massa tentakels om zijn mond.

- Cosmology roll om te leren dat de geelgroene kreeften uit Khyber komen.

- Ontmoeting met strijdgestegenen + Bel'Ataka + Eldeen-wolven.

  + Strijdgestegenen willen NU oorlog met Aundair.

    + Ze willen de oorlog echter winnen. Ze willen Thrane zo snel mogelijk
      betrekken. Ze hebben geen liefde voor Thrane, maar als twee vijanden
      vechten, dan is dat een gewin.

  + Strijdgestegenen werken samen met Eldeen-wolven. De samenwerking is lastig:
    De Eldeen-wolven appreciëren het anti-Aundair-sentiment van de
    strijdgestegenen, maar beschouwen hen nog steeds als onnatuurlijk.

  <!-- + Muiter en Lifra willen allebei *niet* samenwerken met Thrane. -->

  + Bel'Ataka denkt dat het mogelijk is om Tairnadal-elven te overtuigen om te
    strijden voor haar voor Eldeen.
  
  + Bel'Ataka strijdt voor Eldeen omdat Leylandra dat zo bepaald heeft.

- Eindig sessie hier misschien.
