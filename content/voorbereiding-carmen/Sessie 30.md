---
title: "Sessie 30"
date: 2020-10-15T18:29:26+02:00
draft: false
---

# Adals kantoor

- Bast en Grinvor vinden de volgende dingen.

  + Heel veel boeken over magie.

  + Een cabinet vol magische componenten, verdeeld in vakjes:

    + Vloeistoffen (water, inkt, anders).
    + Grondsoorten (zand, aarde, klei, steentjes).
    + Bont en leer.
    + Metalen (koper, zilver, goud, lood).
    + Stofjes (allerlei).
    + Notice, penalty -2, grijsblauwe agaat (1,000 gp, gebruikt voor awaken)

  + Luxe kleding.

  + (Spreek)steen.

  + Magisch gesloten lade met brieven e.d.. *Lock*, Hardness 10.

    + Professionele brieven van mensen binnen het Arcane Congres over
      administratieve zaken. Saai.

    + In werkelijkheid zijn twee van deze brieven magisch. Met *detect magic*
      verplaatsen de letters zich over het papier om een ander bericht te
      vormen.

> Prins Adal,
>
> Bedankt voor het nieuws over de Eldeeners. Uw aanpak is zeer nobel. Ik zal
> meteen naar Arcanix reizen om een vervolg te plannen.
>
> Excuses voor het ongemak. Deze Eldeeners zijn zeer vastberaden.
>
> Hoogachtend,
> Leselor ir'Qarin

<!-- -->
> Zus,
>
> Onderzoek naar de juiste middelen gaat moeizaam maar voortdurend vooruit. Het
> is precair om een voorspelling te doen naar de voltooiing van de arcane
> apocalyps, maar met de huidige vaart verwacht ik dat de voorbereidingen
> volbracht zullen zijn voor het voorbijgaan van het millennium.
>
> Nu, ik kan begrijpen dat ambitie ongeduldig is, maar de doldriest van Darro is
> waanzin. Ik weet dat ik eerder en vaker over dit onderwerp gesproken heb, maar
> Darro dreigt nu alle inspanning ongedaan te maken. Eldeen is een
> gehaktmolen---stuur strijdkrachten westwaarts, en ze worden enkel brunch voor
> de beesten van het brute, barbaarse bos; dat waardeloze, wetteloze woud.
>
> Ik weet dat Eldeen een doorn in de Aundairse trots is, maar wat is het
> dividend van het verdelgen van die doorn? Zou de Aundairse trots niet méér
> toenemen met de triomf over Thaliost? Thrane? Troonburcht? De troon van
> Galifar?
>
> Sluit vrede met Eldeen. Het gezichtsverlies in het westen is niets vergeleken
> met de potentiële prestige van de hoofdprijs in het oosten.
>
> ... Voeg iets toe over die scherven. Verbeter de alliteratie.

# Ontsnappen uit Nocturnas

- Stealth roll tijdens verlaten van kantoor.

- Notice roll om een leegstaande kamer te vinden.

- Dramatic task om te ontsnappen?

# Oranjegaarde

- Donna komt aan bij gaarde.

- Vertelt alle kennis aan druïdes.

- Stuurt druïde naar Oalian met kennis + Khyberscherven + Eberronscherf.

# Terug in Arcanix

- Vermomming van Jes.

- TODO

# Krant

- In zowel de Kroniek van Korranberg én de Aundairse Spreuk staat een artikel
  over een terroristische aanval---mogelijke poging tot moord---van Eldeen in
  Arcanix, op Prins Adal. Aundair heeft een beloning op het levend vangen van
  Grinvor Bairn, Bast (ook wel bekend als Drie), en Donna.

- Aundair heeft volledige militaire mobilisatie aangekondigd. Alle jonge
  Aundairiërs moeten zich melden bij de lokale overheid. Alle producenten van
  waren die nodig zijn voor oorlog (voedsel, wapens, kleding) krijgen belasting
  in goederen.

# Bericht aan Grinvor

- Grinvor ontvangt voor het slapen gaan een mentaal bericht van Leselor. Het is
  een offer om voor Aundair te komen werken.

- VRAAG IEDEREEN DE TAFEL TE VERLATEN.

> Grinvor, Leselor hier. Als het goed is heb je begrepen dat je gezocht wordt
> door de autoriteiten van Aundair. Nu, ik begrijp als geen ander dat jij je
> zeer hecht aan je vrijheid---je bent immers een reizende troubadour, en de
> landen van Aundair hebben veel te bieden. Ik begrijp ook dat je van de andere
> kant van de wereld komt---Eldeen is jou vreemd.
>
> Ik heb met de koningin gesproken. Ze heeft een zacht hart voor troubadours, en
> zou graag willen genieten van jouw muziek en andere kunsten. Sterker nog, ze
> is bereid om jou een leengoed in Aundair te geven---een groot perceel in
> Aundair waar je een landhuis kunt bouwen die onderhoud wordt door een paar
> knechten. Je zou jezelf "baron" mogen noemen.
>
> De enige tegenprestatie de de koningin van jou vraagt, is om trouw aan haar te
> zweren.
>
> Overweeg het. Wanneer je klaar bent, kun je jezelf melden bij Aundairse
> autoriteiten, en ik regel de rest voor je.
