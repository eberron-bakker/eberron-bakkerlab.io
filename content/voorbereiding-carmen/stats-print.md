### Ochre Jelly

**Attributes:** Agility d4, Smarts d4, Spirit d6, Strength d8, Vigor d8\
**Skills:** Athletics d6, Fighting d6, Notice d4, Stealth d4\
**Pace:** 2; **Parry:** 5; **Toughness:** 8 (2)/6 (0)/4 (-2)/2 (-4)\
**Edges:** ---\
**Special Abilities:**

- **Amorphous:** Can maneuver through any non-solid surface, pass through
  cracks in doors or windows, bubble through water, etc.
- **Ooze:** No additional damage from Called Shots; ignores 1 point of Wound
  penalties; cannot be blinded, deafened, or grappled; doesn't breathe; immune
  to disease, poison, lightning, and slashing.
- **Pseudopod:** Str+d10.
- **Size 2:** Ochre jellies are blobs the size of horses.
- **Split:** Instead of being damaged by lightning or slashing, the ochre jelly
  splits into two new jellies. New jellies are two Sizes smaller than the
  original.
- **Wall Walker:** Can move at its full pace on walls and ceilings.

### Knight Arcane / Knight Phantom

**Attributes:** Agility d8, Smarts d6, Spirit d8, Strength d8, Vigor d6\
**Skills:** Athletics d6, Common Knowledge d6, Fighting d10, Intimidation d6,
Notice d6, Persuasion d6, Riding d8, Shooting d8, Spellcasting d10, Stealth d4\
**Pace:** 6; **Parry:** 9; **Toughness:** 8 (3)\
**Edges:** Soldier\
**Gear:** Wand (Range 6/12/24, 2d6), arming sword (Str+d8), splint armor (+3),
medium shield (Parry +2, Cover -2)\
**Special Abilities:**

- **Spellcasting:** Knights Arcane have 15 Power Points and know the following
  powers: *blast*, *bolt*, *burst*, *detect/conceal arcana*, *fly*,
  *protection*, *smite*, and *summon ally*.

### Wandslinger

**Attributes:** Agility d6, Smarts d6, Spirit d6, Strength d6, Vigor d6\
**Skills:** Athletics d6, Common Knowledge d6, Fighting d4, Intimidation d6,
Notice d6, Persuasion d6, Shooting d6, Stealth d4\
**Pace:** 6; **Parry:** 4; **Toughness:** 6 (+1)\
**Edges:** ---\
**Gear:** Wand (Range 6/12/24, 2d6), shortsword (Str+d6), leather armor (+1)

### Warforged Soldier

**Attributes:** Agility d6, Smarts d6, Spirit d6, Strength d8, Vigor d8\
**Skills:** Athletics d8, Battle d4, Fighting d8, Intimidation d6, Notice d6,
Shooting d8, Stealth d4, Survival d6\
**Pace:** 6; **Parry:** 8; **Toughness:** 11 (4)\
**Hindrances:** Outsider (Major), Quirk\
**Edges:** Soldier\
**Gear:** Longsword (Str+d8), longbow (Range 15/30/60, 2d6, AP 1), medium shield
(Parry +2, Cover -2)\
**Special Abilities:**

- **Armor +4:** A warforged is covered in tough plating made of
  magically-treated woods and metals.
- **Living Construct:** +2 to recover from being Shaken; ignore 1 point of Wound
  modifiers; does not breathe or suffer from disease or poison.
- **Resilient:** Warforged soldiers can take one Wound before they're
  Incapacitated.
- **Size 1:** Warforged tower over their human makers.

## Living Spells

Creating your own living spell is immensely easy. Pick a power, copy a template
below, then adjust **Area of Effect** and **Spell Mimicry** to contain the name
of the power. Adjust or remove **Immunity** to match the spell's trapping. It
should be possible to adapt most powers, but living spells such as a *living
beast friend* might be a little difficult to envision.

### Living Burning Hands (Burst)

**Attributes:** Agility d8, Smarts d4, Spirit d4, Strength d12, Vigor d6\
**Skills:** Fighting d4, Notice d4, Spellcasting d12\
**Pace:** 5; **Parry:** 4; **Toughness:** 9\
**Edges:** Arcane Resistance\
**Special Abilities:**

- **Amorphous:** Can maneuver through any non-solid surface, pass through
  cracks in doors or windows, bubble through water, etc.
- **Area of Effect:** Any creatures within the living spell's space are subject
  to the effects of the *burst* power once per round on the living spell's turn.
  If a creature enters it outside of the living spell's turn, they are subject
  to it once on that turn as well, but no more than once per round. The living
  spell rolls its Spellcasting for each target to determine whether the target
  is affected. Each inch of movement while covering a creature uses 2" of the
  living spell's Pace.
- **Immunity:** Immune to fire.
- **Living Spell:** +2 to recover from being Shaken; no additional damage from
  Called Shots; ignores 1 point of Wound penalties; doesn't breathe; cannot be
  blinded, deafened, or grappled; immune to Fear, Intimidation, Fatigue,
  disease, poison, and non-magical attacks.
- **Size 4 (Large):** Living spells are 4 yards in diameter, covering an area
  equal to a Small Blast Template.
- **Spell Mimicry:** The living spell can cast *burst* once per turn using its
  Spellcasting skill for the roll. Creatures are not affected by this power if
  they were already affected by it through **Area of Effect** in this round.

### Living Cure Wounds (Healing)

**Attributes:** Agility d8, Smarts d4, Spirit d4, Strength d12, Vigor d6\
**Skills:** Fighting d4, Notice d4, Spellcasting d12\
**Pace:** 5; **Parry:** 4; **Toughness:** 9\
**Edges:** Arcane Resistance\
**Special Abilities:**

- **Amorphous:** Can maneuver through any non-solid surface, pass through
  cracks in doors or windows, bubble through water, etc.
- **Area of Effect:** Any creatures within the living spell's space are subject
  to the effects of the *healing* power once per round on the living spell's
  turn. If a creature enters it outside of the living spell's turn, they are
  subject to it once on that turn as well, but no more than once per round. The
  living spell rolls its Spellcasting for each target to determine whether the
  target is affected. Each inch of movement while covering a creature uses 2" of
  the living spell's Pace.
- **Living Spell:** +2 to recover from being Shaken; no additional damage from
  Called Shots; ignores 1 point of Wound penalties; doesn't breathe; cannot be
  blinded, deafened, or grappled; immune to Fear, Intimidation, Fatigue,
  disease, poison, and non-magical attacks.
- **Size 4 (Large):** Living spells are 4 yards in diameter, covering an area
  equal to a Small Blast Template.
- **Spell Mimicry:** The living spell can cast *healing* once per turn using its
  Spellcasting skill for the roll. Creatures are not affected by this power if
  they were already affected by it through **Area of Effect** in this round.

### Living Lightning Bolt (Bolt)

**Attributes:** Agility d8, Smarts d4, Spirit d4, Strength d12, Vigor d6\
**Skills:** Fighting d4, Notice d4, Spellcasting d12\
**Pace:** 5; **Parry:** 4; **Toughness:** 9\
**Edges:** Arcane Resistance\
**Special Abilities:**

- **Amorphous:** Can maneuver through any non-solid surface, pass through
  cracks in doors or windows, bubble through water, etc.
- **Area of Effect:** Any creatures within the living spell's space are subject
  to the effects of the *bolt* power once per round on the living spell's turn.
  If a creature enters it outside of the living spell's turn, they are subject
  to it once on that turn as well, but no more than once per round. The living
  spell rolls its Spellcasting for each target to determine whether the target
  is affected. Each inch of movement while covering a creature uses 2" of the
  living spell's Pace.
- **Immunity:** Immune to lightning.
- **Living Spell:** +2 to recover from being Shaken; no additional damage from
  Called Shots; ignores 1 point of Wound penalties; doesn't breathe; cannot be
  blinded, deafened, or grappled; immune to Fear, Intimidation, Fatigue,
  disease, poison, and non-magical attacks.
- **Size 4 (Large):** Living spells are 4 yards in diameter, covering an area
  equal to a Small Blast Template.
- **Spell Mimicry:** The living spell can cast *bolt* once per turn using its
  Spellcasting skill for the roll. Creatures are not affected by this power if
  they were already affected by it through **Area of Effect** in this round.

### Living Shatter (Blast)

**Attributes:** Agility d8, Smarts d4, Spirit d4, Strength d12, Vigor d6\
**Skills:** Fighting d4, Notice d4, Spellcasting d12\
**Pace:** 5; **Parry:** 4; **Toughness:** 9\
**Edges:** Arcane Resistance\
**Special Abilities:**

- **Amorphous:** Can maneuver through any non-solid surface, pass through
  cracks in doors or windows, bubble through water, etc.
- **Area of Effect:** Any creatures within the living spell's space are subject
  to the effects of the *blast* power once per round on the living spell's turn.
  If a creature enters it outside of the living spell's turn, they are subject
  to it once on that turn as well, but no more than once per round. The living
  spell rolls its Spellcasting for each target to determine whether the target
  is affected. Each inch of movement while covering a creature uses 2" of the
  living spell's Pace.
- **Immunity:** Immune to sound-based attacks.
- **Living Spell:** +2 to recover from being Shaken; no additional damage from
  Called Shots; ignores 1 point of Wound penalties; doesn't breathe; cannot be
  blinded, deafened, or grappled; immune to Fear, Intimidation, Fatigue,
  disease, poison, and non-magical attacks.
- **Size 4 (Large):** Living spells are 4 yards in diameter, covering an area
  equal to a Small Blast Template.
- **Spell Mimicry:** The living spell can cast *blast* once per turn using its
  Spellcasting skill for the roll. Creatures are not affected by this power if
  they were already affected by it through **Area of Effect** in this round.
