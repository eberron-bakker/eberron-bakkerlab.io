---
title: "Sessie 31"
date: 2020-11-28T18:29:26+02:00
draft: false
---

# Oalian en Wilgspruit

- Bast en Donna zijn bij Oalian, Wilgspruit aan het genezen.

- Na afloop is er een blij sfeer. Wilgspruit heeft een grote glimlach op zijn
  "gezicht" en ziet er energieker uit.

- Oalian reageert geshokeerd op het nieuws dat Aundair mobiliseert. "Oh nee".
  Hij reageert ook geshokeerd op de zak vol Khyberscherven van Donna en doet
  peinsend zijn ogen dicht.

- Oalian stuurt een bericht naar Faena in Riviertraan met het nieuws.

- Plannen?

# Grinvor en de Hofnar

- Indien Grinvor nee zegt tegen Leselor/de koningin...

- Grinvor wordt wakker en doet zijn schoenen aan.

- Hij kijkt omhoog, en ziet een rustige, blije, melancholieke Herfsthofnar.

- De winter is voorbij.

- Grinvor heeft zo veel mensen geholpen en blijgemaakt, en nu heeft hij
  Wilgspruit---eregast van het jaar van de kleine linkerteen---genezen van zijn
  ziekte.

- Grinvor krijgt een wens. De wens is nep en de Hofnar moet lachen.

- Grinvor krijgt een alles-of-niets-munt.

Eens per dag: In plaats van een trait roll te maken gooi je de
alles-of-niets-munt op. Bij een slecht resultaat krijg je een Critical Failure.
Bij een goed resultaat rol je een 20. Je kunt dit ook voor damage doen, maar
wordt je Shaken, Stunned en Wounded op een slecht resultaat.

# Diplomatieke missie

- Als laatste poging om oorlog te voorkomen is een diplomatieke top gepland op
  Troonburcht. De top begint vanaf 998-06-12, op de Dag van het Heldere Zwaard,
  de dag van Dol Dorn, de soeverein van kracht en staal.

  + Aundair koos de datum.

- Thrane en Breland drongen aan op de diplomatieke top. Karrnath stemde later
  pas in.

- Droaam en Valenar sturen ongevraagd een delegatie.

# Riviertraan

- Er staan duidelijk troepen aan de Aundairzijde van de rivier.

- De Eldeenzijde ziet er verlaten uit. Een redelijk gedeelte van de bevolking is
  tijdelijk verhuisd.

- De Tranentempel is volledig verlaten. De tempel is nog steeds niet
  gerepareerd.

- Ondergronds is een soort kelder met een altaar aan een draak---Khyber. Er zijn
  kaarsen en wierrook en teksten vol met mantra's. Het gezelschap kan een
  document vinden met notities over het bewerken van Khyberscherven. Bijna alle
  pagina's zijn eruitgescheurd.

- Het gezelschap kan onderzoek doen naar de stenen muren van de kelder. Met
  genoeg inzicht kunnen ze achterhalen dat de muren *nieuw* zijn---niet ouder
  dan een paar weken.

# Varna

- Als het gezelschap vraagt om Raghars Rijders dan is er een grote kans dat ze
  verwezen worden naar Raghar, de dorpsgek die op een big rijdt.

- Het gezelschap kan Raghars Rijders vinden bij de Zwarte Bar & Gril. De groep
  is net terug van een avontuur om iemand (Fhazira) veilig van Riviertraan naar
  Passage te brengen.

- Een van Raghars Rijders pakt een "gezocht"-poster en laat hem aan het
  gezelschap zien. Het is niet veilig om in Varna te blijven, en hoewel Raghars
  Rijders graag het geld op zouden willen eisen, hebben ze dank en respect aan
  de Wilgenwachters voor het bevrijden van hun magiër.

- Amilya wil de papieren wel doorpluizen, maar heeft een paar uur nodig. Indien
  mogelijk maakt ze kopieën voor eigen gebruik. Ze komt erachter dat Aundair
  hoofdzakelijk twee dingen wil doen:

  + Magische soldaten maken à la strijdgesmedenen, maar dan levende spreuken.
  + Een waanzinnige bom die alles binnen een radius van 100 meter desintegreert.

- Kleinere dingen:

  + Een magisch schild tegen pijlen.
  + Een anti-magie-spreuk die specifiek geloofsmagie onmogelijk maakt.

# De reis

- Met luchtschip: Wachten op luchtschip van Huis Lyrandar, en onderweg
  overvallen door piraten. De piraten werden ingehuurd door Aundair.

- Met elementair schip: Reizen naar Delethorn. Via de Wynarn richting de zee, en
  onderweg overvallen door piraten. De piraten werden ingehuurd door Aundair.

- Met de trein: Reizen naar Delethorn. Met het schip naar Passage. Met de trein
  naar Thaliost. Onderweg aangevallen door Huis Phiarlan (Drakenmerk van
  Schaduw).

## Piraten

Het Vliegende Fortuin valt aan. Met verschillende harpoons wordt de body van het
schip geraakt, en zitten de twee schepen "vast" aan elkaar. De piraten willen
het schip vernietigen en de meevarenden gevangen zetten.

### :spades: k'Lung

**Attributes:** Agility d10, Smarts d8, Spirit d8, Strength d6, Vigor d4\
**Skills:** Athletics d8, Common Knowledge d4, Fighting d12, Notice d6,
Performance d6, Persuasion d8, Stealth d6, Taunt d10\
**Pace:** 5; **Parry:** 11; **Toughness:** 4 (1)\
**Hindrances:** Arrogant (Major)\
**Edges:** Dodge (Imp), Extraction, Provoke, Trademark Weapon (Imp)\
**Gear:** Rapier (+2, Str+d6, Parry +1), supple leather jacket (+1)\
**Special Abilities:**

- **Amphibious:** Grungs can breathe underwater.
- **Natural Climber:** Grungs can climb vertical surfaces as though walking
  normally, or inverted surfaces at half Pace.
- **Poison Immunity:** Grungs are immune to poison.
- **Poisonous Skin:** Any non-grung that comes into direct contact with a
  grung’s skin must roll Vigor or suffer the effects of Mild Poison. On a
  Critical Failure, or if the victim is poisoned again within a minute, they
  suffer an additional effect for 2d6 rounds dependent on the grung’s skin
  color. 
  - **Green:** The victim can’t move except to climb or jump.
- **Leaper:** Grungs can jump twice as far as listed under Movement (see Savage
  Worlds core rules). In addition, grungs add +4 to damage when leaping as part
  of a Wild Attack instead of the usual +2 (unless in a closed or confined space
  where they cannot leap horizontally or vertically—GM’s call).
- **Size -1**

### :spades: Inferno

**Attributes:** Agility d10, Smarts d6, Spirit d6, Strength d8, Vigor d6\
**Skills:** Athletics d8, Boating d8, Common Knowledge d4, Fighting d10,
Gambling d6, Notice d8, Persuasion d4, Spellcasting d8, Stealth d4\
**Pace:** 6; **Parry:** 7; **Toughness:** 7 (2)\
**Hindrances:** Impulsive\
**Edges:** Frenzy (Imp)\
**Gear:** Sword (Str+d8), tough leather (+2)\
**Special Abilities:**

- **Bite/Claws:** Str+d4.
- **Low Light Vision:** Shifters ignore penalties for Dim and Dark Illumination.
- **Shifting:** Shifters can tap into their lycanthropic heritage. Shifting is a
  free action and lasts 10 rounds or until dismissed as a free action. While in
  this animal-like form, the shifter can ignore 1 Wound penalty. When the
  shifting ends, the shifter must wait 4 hours before shifting again or suffer
  Fatigue. The type of shifting is determined by their subrace. 
  - **Longtooth:** The Natural Weapons of the longtooth shifter grow larger,
    increasing their damage die by a die type. Additionally, they add +4 to
    damage when making a Wild Attack with their Natural Weapons instead of the
    usual +2.
- **Spells:** Inferno has 20 Power Points and knows the following powers:
  *Bolt*, *blast*, *burst*, and *smite*.

### :spades: Laurea Valu-Nelo

**Attributes:** Agility d10, Smarts d4, Spirit d6, Strength d10, Vigor d10\
**Skills:** Athletics d12, Boating d4, Common Knowledge d4, Fighting d10,
Intimidation d8, Notice d4, Persuasion d4, Shooting d4, Stealth d4\
**Pace:** 6; **Parry:** 9; **Toughness:** 11 (2)\
**Edges:** Sweep, Trademark Weapon (Imp)\
**Gear:** Hoe (+2, Str+d8, Reach 1), tough hide (+2)\
**Special Abilities:**

- **Mountain Born:** Goliath physiology is acclimated to high altitude.
  They receive a +4 bonus to resist the cold. Cold damage is reduced by 4.
- **Size +1**

### Imhotep & Neithotep

**Attributes:** Agility d6, Smarts d4, Spirit d6, Strength d6, Vigor d6\
**Skills:** Athletics d6, Boating d8, Common Knowledge d4, Fighting d6,
Intimidation d4, Notice d6, Persuasion d4, Shooting d6, Stealth d4, Taunt d6,
Thievery d6+1\
**Pace:** 6; **Parry:** 5; **Toughness:** 6 (1)\
**Hindrances:** Wanted (Major)\
**Edges:** Thief (+1 to Athletics to climb and Stealth in urban environments)\
**Gear:** Scimitar (Str+d8), leather armor (+1)

### :spades: Regen

**Attributes:** Agility d8, Smarts d6, Spirit d10, Strength d6, Vigor d6\
**Skills:** Athletics d8, Boating d10, Common Knowledge d6, Fighting d8, Focus
d8, Gambling d6, Intimidation d8, Notice d6, Persuasion d10, Shooting d8,
Stealth d6, Taunt d8, Thievery d6\
**Pace:** 6; **Parry:** 6; **Toughness:** 7 (2)\
**Hindrances:** Wanted (Major)\
**Edges:** Natural Leader, Command (+1 to Shaken rolls)\
**Gear:** Studded leather armor (+2), ijszwaard (Str+d6)\
**Special Abilities:**

- **Magic:** 20 Power Points, and knows *burst* (2), *damage field* (4),
  *entangle* (2), and *smite* (2). Trapping is ice.

## Huurmoordenaars

### Phiarlan Assassin

**Attributes:** Agility d10, Smarts d8, Spirit d8, Strength d6, Vigor d6\
**Skills:** Athletics d10, Common Knowledge d6, Fighting d12, Focus d8, Notice
d8, Performance d10+1, Persuasion d6, Shooting d12, Stealth d12+1\
**Pace:** 6; **Parry:** 8; **Toughness:** 8 (3)\
**Edges:** Combat Reflexes, Counterattack (Imp), Frenzy (Imp)\
**Gear:** Sword (Str+d8), mithril chain (+3)\
**Special Abilities:**

- **Fey Ancestry:** Elves receive a +2 bonus to resist magical effects that
  alter their mood or feelings, and magic cannot put them to sleep.
- **Low Light Vision:** Elves ignore penalties for Dim and Dark
  Illumination.
- **Spells:** Phiarlan assassins have 15 Power Points and knows the following
  powers: *Darkness*, *disguise*, *illusion*, *invisibility*, *sound/silence*.
- **Very Resilient:** Two extra Wounds.

# De aanwezigen

## Aundair

- Koningin Aurala, Koning-Consort Sasik d'Vadalis, Heer Darro.

- Wil:

  + Annexeren van Eldeen.
  + Annexeren van Oost-Eldeen.
  + Uitlevering van de Wilgenwachters aan een Aundairs gerechtshof.
  + Schadevergoeding.
  + Demilitarisatie van Eldeen.
  + Sasik als hertog van Eldeen.
  + Ontbinding van Eldeen naar onafhankelijke stadstaten.

## Breland

- Heer Kor ir'Wynarn, broer van Koningin Boranel.

- Als Aundair Eldeen kan binnenvallen, dan wil Breland Droaam kunnen
  binnenvallen.

- Breland wil NIET in een oorlog belanden met landen anders dan Droaam.

## Droaam

- Zarud (half-ork m)

  + Neef van Mumzok. Jong en onervaren, maar gericht op de missie.

  + Diplomaat van Katras Stem.

- Wil erkenning van Eldeen in ruil voor een verdedigingspact.

- Wordt niet toegestaan op het eiland. Als protest blijft de boot van Droaam
  aangemeerd. Aan boord zijn voornamelijk half-orks, dubbelgangers, en
  goblins---"gangbare" monsters in de rest van Khorvaire.

## Eldeen

- Faena Grauwmorgen.

- Wil onafhankelijkheid en vrede.

## Karrnath

- Minister van Buitenlandse Zaken Moranna ir'Wynarn.

  + Goede relaties met Aundair.

- Wil een uitkomst die slecht is voor andere landen, en op z'n minst neutraal
  voor Karrnath.

## Thrane

- Kardinaal Taya.

- Wil verdedigingspact met Eldeen.

- Wil Aundairs ambities tegengaan.

## Valenar

- Hoge Koningin Shaeras Vadallia.

- Bel'Ataka kon een audiëntie krijgen bij de Hoge Koningin. Omdat Valenar snakt
  naar een waardige vijand kwam de uitnodiging van Bel'Ataka goed uit.

- Wil glorie in oorlog tegen Aundair.
