---
title: "Sessie 20"
date: 2020-08-03T13:46:16+02:00
draft: false
---

# Drakenhavik

- Twee dagen terugreizen naar Riviertraan.

## 04-22

- Aankomst in Riviertraan.

- Laat krant zien.

- Faena komt aanvliegen als vogel.

- Faena vertrouwt Aundair niet. Ze wil:

  + Sitrep.
  + Handelsembargo tegen Aundair.
  + Troepen richting de grens halen.
  + Zo snel mogelijk Varna overhalen om mee te doen met de rest van Eldeen.
  + Eisen opstellen richting Aundair:
    + Troepen weghalen bij grensgebied.
    + Staken met het bouwen van de brug.
    + Officiële verklaring dat Aundair de soevereiniteit van Eldeen respecteert.

## 04-23

- Dag één van "training".

  + Drakenhavik retcon: Drakenhavik is jong en kan maar één of twee rijders
    dragen.

- Aan het eind van elke dag: Rol Riding. Succes = 1 token, raise = 2 tokens.
  Blijf doorgaan totdat je 7 tokens hebt.

<!-- 
  + Drakenhavik is best wel gewond.

  + De wond van de drakenhavik begint te infecteren, en wordt elke dag erger.
-->

## 04-24+

<!-- 
- De drakenhavik sterft langzaam aan de geïnfecteerde wond. De enige manier om
  de drakenhavik te redden is door een ritueel dat Faena kan uitvoeren. Aan het
  eind van het ritueel zijn de "essenties" van de twee wezens (Hoop en de
  drakenhavik) één.
-->

<!-- 
- Hoop!drakenhavik heeft:

  + Groene veren op de kop.
  + Een rode snavel.
  + Bruine veren aan de bovenkant.
  + Zilveren veren aan de onderkant.
-->

# Arcanix (p. 37 Five Nations)
  
- Gezelschap wil naar Arcanix. Er gaat momenteel geen boot van Riviertraan naar
  Varna. Reizen via land.

- Varna naar Arcanix: 150 sp per persoon.

- Eenmaal in Arcanix komt het gezelschap bij het kleine dorp.

> This village of 800 people lies directly beneath the floating towers. The
> residents of Arcanix owe their livelihoods to the Arcane Congress,

> The Trannick family offers trained hippogriffs that can ascend to the floating
> towers for visitors unable to get there under their own power. The Trannicks
> collect a 25 gp per person transit fee, which they use to fund scholarships
> for deserving young mages.

- Glarehold & Amberwall

  + Scholen.
  + Amberwall heeft een vleugel gespecialiseerd in alchemie.

- Nocturnas & Skyreach

  + Woningen, bibliotheken, laboratoria.

- Donna kent Paulus in het dorp, en vindt hem in de Vrolijke Hippogrief.

- ... speel "The Lover's Potion". Diana is de dochter van Adal. Als ze
  Paulus/Diana helpen, dan kan Adal misschien helpen met de taak.

<!-- - Margot Trannick (v) suggereert dat de spelers naar Nocturnas gaan voor hun
  missie.

- Quantum ogre: Spelers zoeken naar tovenaar, en zullen uiteindelijk Eerste
  Generaal en Koninklijke Minister van Magie Adal ir'Wynarn vinden.

- Spelers kunnen gaan naar deze plekken om te zoeken naar een magiër die kan
  helpen:

  + Bibliotheek
  + Magische winkel
  + Laboratorium
  + Open klaslokaal
  + Altaar naar Aureon

- Terwijl ze hun probleem uitleggen, overhoort Adal het. -->

- Adal wil de problemen met Eldeen zo snel mogelijk over hebben, en is bereid om
  het gezelschap te helpen.

- De magische bewerking is echter niet triviaal, en niet goedkoop. Omdat hij
  gokt dat het gezelschap niet kan betalen voor de magische procedure, vraagt
  hij om een tegenprestatie.

  + Een laboratorium is recentelijk onbruikbaar gemaakt door spreuken die op hol
    geslagen zijn. Het laboratorium is afgesloten. Hij zou in eerste instantie
    wat vierdejaarsstudenten vragen om het probleem op te lossen, maar vraagt nu
    aan het gezelschap om het probleem op te lossen.

# Laboratorium

- Monsters:

  + Living Burning Hands (vuur)
  + Living Cure Wounds (water)
  + Living Lightning Bolt (bliksem)
  + Living Shatter (donder)

- Als het gezelschap na het gevecht onderzoek doet in het laboratorium,
  ontdekken ze dat de wetenschappers bezig waren om magische "soldaten" te
  maken: Levende spreuken die autonoom vechten voor Aundair.

  + Ze ontdekken ook symbolen van de Zilveren Vlam, en indicaties dat de
    Aundairiërs onderzoek deden naar de militaire effectiviteit van deze levende
    spreuken tegen troepen en magie van Thrane.
