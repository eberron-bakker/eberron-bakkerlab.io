---
title: "Plot: Aundair"
date: 2019-10-27T12:45:20+01:00
draft: false
---

Aundair wil een nieuwe oorlog met Eldeen, om haar land terug in te nemen. Het is
echter politiek onmogelijk om een oorlog te beginnen zonder een nieuwe
wereldoorlog te starten. De oorlog moet daarom een juiste casus belli hebben.
Aundair denkt een casus belli te kunnen fabriceren:

Als blijkt dat de Wachters van het Woud een corrupte organisatie zijn die schade
aanrichten aan Eldeen, dan kan Aundair de *redder* van Eldeen zijn. Aundair kan
zo de Wachters van het Woud het bos uitjagen, en Eldeen zelf weer innemen.
Aundair denkt dat het achteraf wel mogelijk is om de gedane schade ongedaan te
maken.

Het zwaktepunt van de Wachters van het Woud is Oalian, het centrum van de
organisatie. Als Oalian corrupt raakt, dan zal de organisatie zeker uit elkaar
vallen. Aundair wil in principe voorkomen om andere bossen of individuen te
corrumperen.

Maar omdat ze waarschijnlijk maar één kans hebben om Oalian te corrumperen,
moeten ze eerst een testronde doen. Aundair wil de testronde uitvoeren in het
Rivierwoud.

# Corruptie van Wilgspruit

In het Rivierwoud is een nomadische ent genaamd Wilgspruit. Aundair wil beginnen
met het corrumperen van Wilgspruit. Als volgt:

Het laatste herfstfeest was 1 Sypheros, tijdens de herfstequinox. Het feest is de
hele dag lang; van zonsopkomst tot zonsondergang. Liederen, dans, gebeden, en
een groot feestmaal zijn normaal voor het herfstfeest. Er word vooral noten,
appels, hertenvlees en pompoen gegeten.

Het grootste feest is in Groenhart, waar de Wachters een ceremonie doen als dank
voor Oalian. De Wachters bereiden een drank van honing en wijn voor, en tillen
het grote vat naar de boom. Ze gieten het voorzichtig uit op de grond, op enige
afstand van de stam van de boom, zodat de wortels de vloeistof kunnen
absorberen. Dit gaat gepaard met gezang, en de druïdes vragen Oalian om wijsheid
in het komende jaar.

Omdat niet iedereen naar Groenhart kan reizen, werd er rond het Rivierwoud een
alternatief gesuggereerd: Waarom houden we niet een soortgelijk feest met
Wilgspruit? Dit was de suggestie van Eenpoot.

De volgende organisaties waren aanwezig, met de volgende hoofdpersonen:

- Wachters van het Woud - Eenpoot.

- Eldeen-wolven (asgebondenen) - Lifra.

- Stam van de schemermaan - Palai.

- Tempel van de Negen Soevereinen - Fhazira.

- Kinderen des Winters - Frost.

- Poortwachters - Mumzok.

Palai is verantwoordelijk voor de corruptie van Wilgspruit.

## Wilgspruit

Wilgspruit:

- heeft een stofgemalen Khyberscherf binnenin zich.

- heeft de harde, grove bast van een oude, stervende boom.

- weigert voedsel, dus begint te verhongeren.

- heeft tien keer de hoeveelheid water nodig die hij normaal gesproken nodig
  hebt, of zal sterven van de dorst.

- 's ledematen worden te zwaar om te bewegen en zijn bast krijgt de schilferige,
  gelaagde uitstraling van houtsnippers.

- heeft kleine paarsblauwe dotjes in de bladen en ledematen.

- wil niet veel praten.

- kan geheeld worden met *greater restoration*.

## Wachters van het Woud - Eenpoot

Eenpoot:

- blijft rustig---maar somber---onder de situatie.

- wijst niemand aan, omdat dit de situatie zou verslechteren. Privé denkt hij
  dat Frost of Fhazira verantwoordelijk is voor de situatie.

## Eldeen-wolven - Lifra

Lifra:

- is woedend dat Wilgspruit ziek is.

- wijst direct Fhazira aan, en wil haar tempel afbranden.

- wijst ook Aundair aan, voor wie Fhazira gewerkt zou hebben.

- wijst ook Frost aan, die er vast iets mee te maken gehad heeft.

- wijst ook Eenpoot aan, die te weinig gedaan heeft om Wilgspruit te verdedigen.

## Stam van de schemermaan - Palai

Palai:

- wil niets te maken hebben met het probleem.

- denkt dat de Zilveren Vlam er iets mee te maken heeft. Zoniet, dan heeft
  Fhazira het waarschijnlijk gedaan.

- is verantwoordelijk. De Wisselpoort is al een tijdje zwak, en hij is
  overgenomen door een *intellect devourer*. In een ontmoeting met Fhazira
  stemde hij in om mee te werken aan de vergiftiging van Wilgspruit. Hij stemde
  in om oorlog te veroorzaken tussen Aundair en Eldeen, waardoor hij engestoord
  verder kan gaan met het openen van de Wisselpoort.

- kreeg later van Aundair een *Khyberscherf*. Palai heeft de Khyberscherf
  verfijnt tot stof, en heeft de stof gemengd met de alcohol die Wilgspruit
  toegediend kreeg.

## Tempel van de Negen Soevereinen - Fhazira

Fhazira:

- is bang, omdat ze denkt dat men haar zal aanwijzen als schuldige. Ze durft
  niet dicht bij Wilgspruit te komen.

- zegt alleen bekend te zijn met Eenpoot, en weet niemand aan te wijzen.

- is een informant voor de Koninklijke Ogen van Aundair. Ze is deels
  verantwoordelijk voor het opzetten van het plan. Ze kreeg te weten dat Aundair
  iemand zoekt om een ent te vergiftigen. Ze dacht dat de Stam van de
  Schemermaan in wilde stemmen, als voorwaarde voor reparaties van Aundair.

- is niet op de hoogte van Palais corruptie.

## Kinderen des Winters - Frost

Frost (*veteran*) is een gematigd Kind des Winters. Ze gelooft dat dood en
verderf een natuurlijk onderdeel van de natuur is. Het is iets waar men niet
bang voor moet zijn, maar moet omarmen. Ze gelooft dat er binnenkort een grote
zuivering zal komen, die de wereld vernietigt zodat de natuur opnieuw kan
beginnen op een leeg canvas.

Wat haar anders maakt dan andere Kinderen des Winters, is dat zij dit proces
niet wil versnellen: Alles op zijn tijd.

Frost komt als soldaat uit Cyre, waar zij de Rouwdag overleefd heeft. Ze draagt
nog steeds haar militaire uniform. Ze woont in Roodblad, waar ze een paar
volgelingen heeft.

Frost:

- is niet ongelukkig dat Wilgspruit ziek is.

- weet niet wie het gedaan heeft. Voor haar part heeft Eenpoot het gedaan. Zijn
  de Wachters echt zo goed?

- wil Wilgspruit uit zijn lijden verlossen.

## Poortwachters - Mumzok

Mumzok (*druid*) is normaal gesproken niet in het Rivierwoud. Hij was echter
toevallig in de buurt, en kwam langs voor het feest.

Mumzok:

- kent niemand goed genoeg om een oordeel te vellen.

- denkt dat Khyber verantwoordelijk is voor de corruptie van Wilgspruit. Hij
  weet alleen niet hoe.

<!-- - Erkent Oalian niet als leider van Eldeen. De andere druïdische sektes doen dit
  wel. -->

# Volgorde

## Vooraf

- De Stam van de Schemermaan verdedigt de Wisselpoort, een van de poorten die
  Eberron afschermt van Khyber. De Wisselpoort was al een tijd zwak. Een
  *intellect devourer* is door de poort geglipt, en heeft Palai---de
  leider---overgenomen.

- Fhazira is informant voor Aundair. Ze kreeg te weten dat ze iemand moest
  zoeken die eventueel zou willen instemmen met de vergiftiging van een ent. Ze
  besloot dat de Stam van de Schemermaan mogelijk zou willen instemmen, op
  voorwaarde van reparaties naar de shiftergemeenschap.

- Palai stemde in.

- Voor het herfstfeest van 997 is besloten om ook enkele buitenstaanders uit te
  nodigen. Dit werd gepusht door Eenpoot, die vond dat dit goed zou zijn voor
  het vredesproces na de Laatste oorlog.

- Er was onenigheid binnen de Wachters van het Woud over hoe open het feest zou
  moeten zijn. Uiteindelijk is er besloten om het niet een open feest te maken,
  maar om een lijst van uitgenodigden op te stellen. Dit moest vijanden en
  kwaadwilligen buiten de gaard houden.

- De drank voor Wilgspruit werd meegenomen door Eenpoot. Eenpoot had hier lange
  tijd aan gewerkt in Riviertraan, met samenwerking van Het Gebroken Glas.

- Eenpoot heeft voor vertrek de drank gecontroleerd door te proeven. Er was toen
  niets mis met de drank.

## Het feest

- De dagen voor het feest kwamen gasten aan bij de gaard. Er werden tenten
  opgezet buiten de gaard. Er was geen ruimte in de boomtoppen voor gasten van
  buiten de gaard.

- De Wachters van het Woud waren het eerst aanwezig. Daarna de Eldeen-wolven,
  dan de rest, en als laatste Fhazira.

- Alle gasten hadden eten en drank meegenomen om te nuttigen en te delen tijdens
  het feest. Uitzondering hier waren de Eldeen-wolven, die normaliter
  afhankelijk zijn van plunderen.

- De drank zat in een ton die in de gaard bewaard werd, naast een van de bomen.
  Er was geen permanente bewaking voor de drank, maar er was wel constant een
  Wachter van het Woud aanwezig in de gaard.

- Er zat een kurk op een van de vlakke kanten van de ton. Het wegnemen van de
  kurk is geen makkelijk werk en kan niet met de hand. Het wegnemen van de kurk
  creëert een luid geluid.

- Lifra van de Eldeen-wolven stond erop om de drank te testen voordat het
  gegeven werd aan Wilgspruit. Ze heeft in totaal twee keer getest:

  + De nacht van tevoren, toen ze dit als dronken idee verzon. Ze was in een
    goede stemming en wilde meer drinken. Ze zag de ton staan, en verklaarde
    zichzelf publiekelijk als officiële voorproever van Wilgspruit. Ze heeft een
    minuut lang geprobeerd om de ton te ontkurken totdat Mumzok haar hielp, en
    met orkkracht de ton wist te ontkurken. Ze kreeg één glas om te drinken.

  + Kort van tevoren, toen ze haar taak als voorproever uitvoerde. Ze kreeg de
    ontkurker van Eenpoot.

- In werkelijkheid was de drank niet vergiftigd, maar de grond. Palai heeft de
  stof gestrooid over de grond voor Wilgspruit, en heeft er daarna een dun
  laagje grond overheen gelegd. Dit deed hij stiekem en onopgemerkt.

- Op het midden van de dag werd de ton door meerdere druïdes gedragen naar
  Wilgspruit:

  + Eenpoot
  + Mumzok
  + Frost

- Wilgspruit werd moe na de alcohol, maar had lange tijd geen negatieve
  effecten.

- De Stam van de Schemermaan vetrok als eerste, samen met Fhazira. Daarna
  vertrokken de Eldeen-wolven, daarna de rest. Als allerlaatste de Wachters van
  het Woud.
