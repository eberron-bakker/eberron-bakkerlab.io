---
title: "Sessie 7"
date: 2020-02-29T13:11:00Z
draft: false
---

- Grinvor droomt over Thelanis (NIET ZEGGEN DAT HIJ SLAAPT), het land van
  altijd-lente. De kou van de de winter is weg, en hij rijdt op zijn raptor
  langs een volgroen bospad. In de verte ziet hij heuvels en een blauwe lucht.
  Hij ziet een slapende dromedaris langslopen, en een vogel fluit een gedichtje:

  Daar het voor een dromedaris,
  Met een mager maandsalaris,
  Slechts een wensdroom is gebleken,
  Weg te gaan naar verre streken,
  Heeft ie zich maar voorgenomen,
  Dat hij daar-is in zijn dromen.

- Grinvor mag verkennen. HIJ ZIET EEN PEGASUS IN DE LUCHT.

- Grinvor komt op een gegeven moment bij een beekje. Aan het beekje ziet hij de
  Hofnar zitten met een portret in zijn handen. Hij kan niet zien wie er op het
  portret afgebeeld is. De Hofnar spreekt het portret liefelijk toe:

  ik hou van jou, ik hou van jou
  een dag, een maand, een eeuw
  ik weet dat liefde lente is
  maar ook een beetje sneeuw

- Een klein sneeuwvlokje valt in Grinvors hand, en smelt. Hij merkt dat het een
  heel klein beetje sneeuwt in Thelanis. Dan: Einde droom.

- Wakker worden.

- Het gezelschap komt aan bij het gevang. Het gevang zit in het zelfde gebouw
  als de bank van Huis Kundarak. Het gevang is tamelijk klein: Hooguit twee
  gangpaden en tien cellen over twee verdiepingen.

- De bewaker is een dwerg genaamd Breandan. Hij rookt hevig, en slaapt achter
  zijn bureau als het gezelschap binnenkomt, voeten op tafel.

- De borg voor Amilya is 10 goud, maar Breandan wil zeker weten dat Amilya niet
  weer in het gevang terecht komt. Iemand moet garant staan, of een goed
  argument geven dat Amilya geen problemen meer zal veroorzaken.

- ... Speel de rest van de dinges zoals in Wizard In A Bottle. Vervang *Eldath*
  met *Balinor*. Op de tweede dag heeft ze een koi karper aan de leeuwen gevoed.
  De karper was 105 jaar oud; geboren vóór de Laatste Oorlog. 
  
- Het gezelschap heet Raghars Rijders. Wie is Raghar? De dorpsgek die vaak op
  een varken rijdt. Hij is niet een lid van de Rijders. Wel leden:

  + Shian (mens v, krijger)
  + Azif (mens m, heilige)
  + Zifre (khoravar v, bard)
  + Nul (dubbelganger m/v, jager)

- Tijdens bezoek aan de tempel (of op een andere manier als het gezelschap niet
  naar de tempel komt), wordt het duidelijk dat het pronkstuk van de tempel weg
  is---een pegasus. De pegasus bevond zich altijd rond het standbeeld van
  Balinor, maar was plots weg op een dag, en diezelfde dag vonden ze een
  hippocampus---half-paard, half-vis. Niet dat de tempel onblij is met de
  hippocampus, maar vooral verbijsterd.

- De priester die dit verhaal vertelt is een half-orc genaamd "Gurk". Hij is de
  priester verantwoordelijk voor het dierenwelzijn in de tempel, en is bezorgd
  voor de pegasus. Hij heeft de hippocampus onderzocht, en weet vrijwel zeker
  dat het een ander beest is---de pegasus is geen hippocampus geworden.

- Desondanks heeft de hippocampus veel mensen naar de tempel gebracht.

- Op dit moment wil Grinvor waarschijnlijk naar Thelanis. Hij weet alleen niet
  hoe. Hij kan hardop vragen aan de Hofnar, en de Hofnar zal hem een raadsel
  geven. Drie geopende flessen verschijnen. In de vlessen zit rottend voedsel
  dat verschrikkelijk ruikt. De flessen volgen Grinvor. Hij krijgt ook twee
  kurken ter beschikking. Wanneer hij iets anders dan de kurken gebruikt om de
  flessen dicht te stoppen, schiet dat voorwerp weg. De oplossing is om de twee
  kurken in zijn neusgaten te stoppen.

- Wanneer het raadsel opgelost is, verdwijnt de geur. Het verrotte voedsel wat
  in de flessen zat is nu heerlijk fruit. Het fruit proeft anders dan het eruit
  ziet. Door het eten van het fruit, wordt men naar Thelanis gebracht.
