---
title: "Murder Mystery"
date: 2020-03-15T19:02:36Z
draft: false
---

Egan Bakker is a foundling from House Cannith, trying to make his way into their
good graces. Bakker is an artificer on his way from Sharn to Fairhaven with his
family and a suspicious collection of others. On the final day of travel, Bakker
is shot and killed with a newly invented firearm.

# Characters

## Egan Bakker

{{< figure src="/img/Cédric.png" width="200px" >}}

Egan Bakker has made his fortune in his home city of Sharn, the City of Towers.
He has the Dragonmark of Making, but his mother was an excoriate (a member of
the house who was banished) of House Cannith. The leader of House Cannith in
Sharn, Baron Merrix d'Cannith, hated Egan's mother so much that he would not
even accept Egan into the house when he returned as an adult.

Egan has spent many years attempting to win the good favour of the Baron Merrix
d'Cannith, but was continuously rebuffed, so now he and his household are
leaving Sharn and moving to Fairhaven to meet with another Baron of House
Cannith, Jorlanna d'Cannith. She is a rival to Merrix, and she has a competing
claim on the leadership of House Cannith. Travelling with Bakker is his spouse,
his loyal retainer, his former business partner, his bodyguard, his lover, and a
strange youth who claims to be his long-lost child.

Egan Bakker is an artificer that specialises in weapons. Recently he developed a
new kind of firearm that does not require arcane training to use through its use
of explosive powder and makes hardly any noise when fired due to a *Silence*
enchantment. This invention seems to have caught the interest of quite a few
people.

## Blue Shield

{{< figure src="/img/Blue Shield.jpg" width="200px" >}}

Blue Shield is a warforged bodyguard who is fiercely devoted to Egan in the
absence of knowing what else to do now that the war is over. Blue Shield will
enter a grief-fueled frenzy and attack everyone in sight after learning that
Egan is dead.

**Attributes:** Agility d8, Smarts d4, Spirit d6, Strength d10, Vigor d8\
**Skills:** Athletics d8, Battle d6, Fighting d8, Intimidation d4, Notice d6,
Shooting d6, Stealth d4\
**Pace:** 6; **Parry:** 6; **Toughness:** 8 (2)\
**Hindrances:** Loyal (Minor), Outsider (Minor)\
**Edges:** Sweep\
**Gear:** Greatsword (Str+d10, two hands)\
**Special Abilities:**

- **Armor +2:** Integrated plating.
- **Steel Fists:** May attack with their fists and deal Str+d4 damage.
- **Sweep:** Fighting roll at -2 to hit all targets in weapon's Reach, no more
  than once per turn.
- **Living Construct:** Warforged are living constructs made from organic and
  inorganic matter and are capable of emotions and conscious thought. They do
  not need to breathe, eat, or drink, and are immune to poison and disease.
  Warforged also don’t need to sleep, and arcane powers can’t put them to sleep;
  however, they must spend at least six hours in a relaxed state. In this state,
  warforged remain conscious, can see and hear as normal, and can perform simple
  tasks.

## Sappho Stananza (The Scholar)

*Female human scholar*

{{< figure src="/img/Sappho.png" width="200px" >}}

**Attributes:** Agility d4, Smarts d10, Spirit d6, Strength d4, Vigor d6\
**Skills:** Academics d10+2, Athletics d4, Common Knowledge d8, Fighting d4,
Notice d8, Persuasion d6, Research d8, Spellcasting d10, Stealth d4\
**Pace:** 6; **Parry:** 4; **Toughness:** 5\
**Hindrances:** Arrogant (Major), Shamed (Major)\
**Edges:** Arcane Background (Magic), Aristocrat, Scholar\
**Gear:** Wand (Range 5/10/20, Damage 2d6)\
**Special Abilities:**

- **Magic:** Has 10 Power Points and knows the following powers: *bolt*
  (chromatic orb), *confusion*, *empathy* (friends), *mind link* (message), and
  *mind reading*.

Sappho is privileged, well-educated, soft-spoken, and a little snobbish.

The primary reason that Sappho is on the train is because Odi
told her that it was important to Egan.

Although Sappho behaves herself as though she is rich and important, she is not
in truth. Coming from a lower middle class Thrane family, she was admitted into
the Passage Institute, an arcane college of ill repute. It has poor scholarship
and low standards. Sappho could afford no better, but was easily one of the best
students.

When she got her degree in enchantment, she spent a few years in employ of a
wizard in Varna. She tried to be admitted into the Arcane Congress after that,
but they would never allow alumni from the Passage Institute. She was incredibly
upset by this, and decided to delay her scholarly career no longer.

A single lie started it all: She wrote a letter to the Morgrave University in
Sharn stating that she had a degree from the Arcane Congress. This easily gave
her admission, and she has been a scholar at the Morgrave University ever since.
This one lie led to more lies, until she pretended to be living a life of luxury
and privilege.

In truth, she made it to upper middle class.

At one social event in Sharn, Egan revealed these lies to all, which upturned
her academic career. Sappho struggled to retain her status with the Morgrave
University, and was scoffed at by colleagues she had regarded as her friends up
until then.

Sappho resolved to have revenge on Egan.

A long-time friend of Sappho---Odi---became Egan's lover. This was annoying, but
after some time, Sappho discovered that Egan was leaving Odi dry. When Odi then
rebound to Sappho, she was enthusiastic to jump on the opportunity. Sappho
plotted to win her over and have her publicly choose her over Egan to humiliate
him.

Does she really love Odi? Hard to say.

### Knowledge

- Thinks Egan is a cash-grabbing social climber who made his money during the
  war as an arms dealer.
- Met Egan at social events and parties in the last few years.
- At one such event, Egan publicly stated that he had been interested in hiring
  Sappho, but had his retainer do a background check. He then discovered that
  Sappho had not graduated at the Arcane Congress like she said, but at the
  Passage Institute, a substandard school for poor folks. He ridiculed her for
  this, and she has hated him ever since.
- Has been sleeping with Odi, whom Egan has had on his arm, for a short while.

### Contacts

- Hates Egan.
- Met Caeldrim, Egan's husband, once or twice.
- Has been sleeping with Odi for a short time. On the train, they pretend to be
  gal pals on a road trip.

### At the time of the murder

- Woke up in her cabin with Odi.

### Items

- A lot of books, writing materials, and ink.
- One of Odi's earrings under her pillow.

## Aston Oakchin (The Former Business Partner)

*Male dwarf marketeer*

{{< figure src="/img/Aston.jpg" width="200px" >}}

**Attributes:** Agility d4, Smarts d6, Spirit d10, Strength d4, Vigor d6\
**Skills:** Athletics d4, Common Knowledge d6, Fighting d4, Gambling d4,
Intimidation d6, Notice d6-1, Performance d4, Persuasion d10, Stealth d4, Taunt
d6\
**Pace:** 5; **Parry:** 4; **Toughness:** 5\
**Hindrances:** Bad Eyes (Minor), Greedy (Minor)\
**Edges:** Charismatic\
**Gear:** Shortsword (Str+d6, two hands)\
**Special Abilities:**

- **Low Light Vision:** Ignores penalties for Dim and Dark Illumination.

Aston has known Egan for fifteen years or more, and helped Egan set up his first
workshop. At first, everything was split fifty-fifty, but then Egan squeezed
Aston out of the business right before he made big.

Aston is a marketer more than anything. He smooth-talks and knows how to talk
the language of money. His contribution to the partnership was Egan was all the
business side of things, allowing Egan to focus on artificing. Once Aston was
close to making a deal with the Brelish military, Egan went behind his back and
sealed the deal himself. This cut Aston out of all the earnings.

Aston refused to talk to Egan ever since, until recently.

### Knowledge

- Egan Bakker is a bully and he loves money more than anything.
- Egan does love his spouse in his own, possessive way, but Caeldrim Ahinar is
  too good for him.
- Egan still owes Aston five hundred gold in consultance fees.
- Egan's inventions would have never been bought during the War if it were not
  for Aston's hard work.
- Recently Egan seemed interested in Aston's help again. Aston hates it, but
  wants the money he is owed.

### Contacts

- Has known Egan's spouse (Caeldrim Ahinar) since he met Egan.
- Shimyra has known Egan for longer than Aston.
- Shimyra looks familiar.

### At the time of the murder

- Aston was drinking together with Egan in the dining car until Egan passed out.
- Before Egan passed out, he admitted that he had double-crossed Aston in the
  past, and was going to do it again.
- Aston left Egan sleeping it off on the table and went to bed.

### Items

- An old picture of you and Egan at a party in Sharn when you first met. Shimyra
  is just visible in the background. A coincidence? 
- A schema for a device very similar to Egan's new gun. Aston copied
  Egan's design while on the train, as blackmailing material in case Egan
  wants to cut him out again.

## Odi (The Secretive Lover)

*Female human painter*

{{< figure src="/img/Odi.png" width="200px" >}}

**Attributes:** Agility d6, Smarts d6, Spirit d8, Strength d4, Vigor d6\
**Skills:** Athletics d4, Common Knowledge d6, Fighting d4, Notice d6,
Performance d8, Persuasion d10, Stealth d6, Thievery d4\
**Pace:** 6; **Parry:** 4; **Toughness:** 5\
**Gear:** Painting knife (Str+d4)\
**Edges:**

- **Attractive:** +1 to Performance and Persuasion rolls if target is attracted
  to her general type (gender, race, etc.).
- **Charismatic:** One free reroll on Persuasion rolls.

**Hindrances:**

- **Jealous:** Envious of others when it comes to the arena of love.
- **Mild Mannered:** Odi's milquetoast just isn't threatening. Subtract 2 when
  making Intimidation rolls.

Odi is an affluent painter, painting paintings for even more affluent people of
Sharn.

Odi had always been good friends with Sappho, a scholar at the Morgrave
University, and enchantment wizard. Odi knows that Sappho pretends to be more
posh than she really is, but there is a certain charm to that.

At one social party in Sharn, Odi met Egan, and somehow ended up entangled in an
affair with the married man. The affair was thrilling due to the lavish gifts
she received, but the thrill soured after a while when it became evident that
Egan was not going to commit to the affair, and would never really leave his
husband.

Odi resorted to Sappho as a rebound girlfriend. This relationship has been
fairly fresh, and she herself has not dared commit to Sappho yet, because Egan
is still showering her with lavish gifts.

Egan invited Odi to come along to Fairhaven, where he was going to move. When
she told the news to Sappho, Sappho suggested to come along, and make a city
trip out of it.

### Knowledge

- Egan was a greedy, selfish man, but giving lavish gifts made him feel
  important.
- Egan had no intention of ever really leaving his spouse.
- Although Odi had Egan's arm and received plenty gifts and attention, he never
  took their quasi-relationship any further. Sappho was Odi's rebound, a
  long-time friend of hers. Sappho had been apprehensive about pursuing anything
  with Odi at first, but quickly came around to the idea after a moment alone.
- Even though it was assumed that House Cannith in Fairhaven would accept him,
  he was still paranoid that they would not.
- Sappho hated Egan for humiliating her at a party years ago, but Odi does not
  know much about it.
- Sappho pretends to be more posh than she really is.
- Odi has attended Sharn high-society social parties together with Egan.
  Sometimes Sappho would be present at those parties as well, but she avoided
  Egan like the plague.

### Contacts

- Caeldrim Ahinar, Egan's spouse, from a distance.
- Quen, the retainer, from her waiting on Egan.
- Has been sleeping with Sappho for a while.

### At the time of the murder

- Odi was waking up in the shared cabin with Sappho.

### Items

- Gifts from Egan (jewelry, perfume, etc.)
- A notebook with sketches and drawings. One doodle very obviously shows a
  hideous depiction of Caeldrim. Another series of doodles show a woman in
  various naked poses. There is also a series of depictions of Egan, some with
  little drawings of hearts.
- Painting supplies, among which a dull painting knife that can be sharpened
  (Str+d4).

## Caeldrim Ahinar (The Spouse)

*Male elf soldier*

{{< figure src="/img/Caeldrim.png" width="200px" >}}

**Attributes:** Agility d8, Smarts d4, Spirit d8, Strength d8, Vigor d6\
**Skills:** Athletics d6, Common Knowledge d4, Fighting d8, Notice d6,
Persuasion d6, Shooting d8, Stealth d4, Taunt d8\
**Pace:** 6; **Parry:** 6; **Toughness:** 7 (3)\
**Gear:** Scimitar (Str+d8), bronze armor (+3)\
**Edges:**

- **Ambidextrous:** Suffers no penalty from off-hand attacks.
- **Low Light Vision:** Ignores penalties for Dim and Dark Illumination.
- **Trance:** Elves don't need to sleep. Instead, they meditate deeply,
  remaining semiconscious. This trance lasts approximately three hours.

**Hindrances:**

- **Frail:** Elves aren't as sturdy as most other races. Their Toughness is
  reduced by 1.
- **Jealous (Minor):** Immensely jealous in the field of romance.
- **Outsider:** Elves have a strong tradition of racial pride, sometimes at the
  expense of other races. Subtract 2 from Persuasion rolls with people who
  dislike elven pride.
- **Ruthless (Minor):** Will not be held back from his goal.
- **Stubborn (Minor):** Nothing can convince Caeldrim of his wrongs.
- **Vengeful (Major):** Caeldrim's husband was murdered, and he will have
  revenge.
- **Vow (Major):** Has a religious Tairnadal vow to his ancestor.

Caeldrim is the descendant of Ahinar, a hero from long, long past. He has been
chosen by Ahinar to be his beacon in life, and emulate her heroics to the best
of his abilities. No small feat, because Ahinar killed Lady Vol and ended the
bloody war with her dragonmarked house.

Ahinar did this with a legendary weapon, crafted by her partner, a dwarven
weaponsmith. The weapon was revolutionary for its time, and the first of its
kind.

For this reason, Caeldrim came to Khorvaire from Aerenal. War was raging, and
the Blood of Vol was prominent in the northern province of Karrnath. But first,
he needed to find himself a weaponsmith partner. He found that partner in the
City of Towers---Egan Bakker. Egan was an artificer with the Mark of Making, and
constantly coming up with new weapon designs. Nothing revolutionary, but
Caeldrim was confident that the time would come. In the meantime, he fought in
the war for Breland.

The abrupt ending of the war was a sad occurrence for Caeldrim. He came home to
Egan, and slowly fell out of love. Egan had different ideas about the war, and
willingly sold weapons to Karrns. And, in truth, Egan wasn't the most
compassionate person. But he was destiny, and therefore Caeldrim put up with the
man, even when Egan started keeping a younger woman in his presence.

Fortunately, things started looking up when Egan developed his revolutionary
weapon: A weapon that uses explosive powder to fire at range. The prototype is a
little rough around the edges, but the promise is great. This took Caeldrim one
step closer to his personal vendetta with Karrnath, and the Blood of Vol.

When Egan proposed to relocate to Fairhaven to join up with House Cannith West,
that was a blessing in and of itself. Aundair has long been the arch enemy of
Karrnath, and the two nations remain on the brink of war.

### Knowledge

- Married Egan nineteen years ago.
- Fought in the Last War for Breland.
- Egan has been neglecting Caeldrim since the Last War ended.
- Egan has always been rejected by House Cannith South, due to a feud between
  Baron Merrix and Egan's mother. Egan takes even small rejections very badly.
- Egan has been blackmailing the train conductor over something that happened
  long ago.
- Egan has functionally been accepted into House Cannith West.
- Egan recently revealed that he had a lost heir, Bram.

### Contacts

- Aston Oakchin, a former business partner of Egan, was still good friends with
  Egan when Caeldrim married him. Things are no longer well between them.
- Quen is Egan's loyal retainer. She has been a live-in aide for as long as
  Caeldrim can remember.
- Has seen Sappho at parties in Sharn. Some kind of scholar whom Egan speaks
  poorly of. Sappho avoids Egan like the plague.
- Bram recently showed up as lost heir. Bram is nineteen years old.
- Has met Klabs, the train conductor, once or twice. He served in the war as a
  mechanic for Breland.
- Aware of Odi, a painter woman whom Egan keeps as a quasi-lover.
- Blue Shield is Egan's loyal warforged bodyguard, and he will protect Caeldrim
  too.

### At the time of the murder

- Asleep in his quarters. Had taken a "sleeping" potion to assist in meditation.
- Had also stealthily mixed some sleeping potion into Egan's brandy.

### Items

- A chest filled with single doses of *potion of sleeping*.
- A letter from Egan to the conductor asking for the blackmail to stop, begging
  that they had done enough.
- Bullets.

## Bram d'Cannith (The Lost Heir)

*Male human adolescent*

{{< figure src="/img/Bram.jpg" width="200px" >}}

**Attributes:** Agility d6, Smarts d6, Spirit d6, Strength d6, Vigor d6\
**Skills:** Athletics d4, Common Knowledge d6, Fighting d4, Notice d6,
Persuasion d6, Stealth d8, Thievery d8\
**Pace:** 6; **Parry:** 4; **Toughness:** 5\
**Hindrances:** N/A\
**Edges:** Arcane Background (Dragonmark)\
**Gear:** Cane (Str+d4)\

Bram never knew his father. That was until letters that his mother had kept from
him led him to Egan Bakker with proof. Bram confronted him, but he did not
believe the boy. Then a few days later, out of the blue, Egan asked Bram to come
with him to Fairhaven so he can get to know him better.

Now that Egan is dead, Bram is heartbroken not to have got the chance to get to
know his father. But he certainly stands to profit from it now.

Bram's mother was Lora, daughter of Merrix d'Cannith. Due to poor relations with
his mother, Bram was quick to agree to come with Egan. Bram is not intimately
aware of this, but Merrix absolutely hates Egan's mother, and Egan by proxy.

Also due to poor relations with his mother, Bram was out a lot, and ran into the
wrong sort of people. He had become a thief, not for a need of possessions, but
for the thrill. The rich of Sharn could do with a little less, anyway. Bram's
quick fingers have landed him in trouble a handful of times, but he was bailed
out every time due to his relation to House Cannith.

### Knowledge

- One day Egan didn't seem interested in knowing Bram. The next he completely
  changed his mind.
- Egan claims he had no idea that Bram existed.

### Contacts

- Met Caeldrim at the Bakker home when he visited.
- Met Quen and Blue Shield in the journey to the lightning rail station.

### At the time of the murder

- The night before the murder, Bram saw Caeldrim Ahinar mixing something into
  the brandy bottle that Egan took to drink with Aston.
- Bram then went back to his room.

### Items

- A picture of Egan and Bram's mother.
- A page in Egan's handwriting:

> I have deliberated with Quen. I should bring the child with me. I need an heir
> to further my genius when I am gone, and Arawai knows that my husband will not
> spawn me any. And even if he could, Arawai knows that love is lost between us.
> Damned elf.
>
> I dearly hope that the child will grow into his role as my heir, however. He
> seemed pitiful when I met him. Bears a mark, and scarcely knew how to cast
> even the simplest spell. Utterly useless. I would be surprised if he even
> knows how to read.

## Klabs (The Veteran Conductor)

*Male goblin conductor*

{{< figure src="/img/Klabs.jpg" width="200px" >}}

**Attributes:** Agility d6, Smarts d8, Spirit d8, Strength d4, Vigor d6\
**Skills:** Athletics d4, Common Knowledge d4, Driving d4, Fighting d4, Notice d6,
Persuasion d6, Repair d6, Shooting d6, Stealth d4, Taunt d8, Thievery d6\
**Pace:** 6; **Parry:** 4; **Toughness:** 4\
**Gear:** Wand (Range 6/12/24, Damage 2d6)\
**Edges:**

- **Humiliate:** Free reroll when making Taunt rolls.
- **Low Light Vision:** Ignores penalties for Dim and Dark Illumination.

**Hindrances:**

- **Size -1:** Goblins stand 3-4' tall.
- **Obligation:** Klabs has a full-time job working for House Orien.
- **Outsider:** Goblins fit in well with their fellow goblinoids, but many
  others may not view goblins fondly. They subtract 2 from Persuasion rolls with
  people who do not trust goblinoids.
- **Phobia:** Klabs is scared of warforged. -1 to all Trait rolls if a warforged
  is present.
- **Secret:** Klabs secretly works for the Tyrants.

Klabs was a mechanic in Sharn during the last war, where he worked on war
equipment. Through this occupation, Klabs had met Egan, who was a weapon
inventor and arms trader.

As part of the generally mistreated underclass of Sharn, Klabs struggled to make
ends meet. Whether for good or for bad, making ends meet was a little easier
once he came in employ of the Tyrants, a criminal organisation in the City of
Towers. Among many things, the Tyrants specialise in blackmail.

On one assignment, Klabs tried to smuggle out some of Egan's weapon designs to
the tyrants. However, due to an error in communications and an unhealthy dose of
misdirection, he had delivered the schema to a confidant of Egan instead of to
his partner in crime. Thus, Egan had found out, and blackmailed Klabs about it
ever since.

At some point during the war, Klabs was stationed near the frontlines on the
border of Cyre and Thrane. He was tasked to do repairs on war equipment. This
was a dangerous gig, however. One day, Klabs' base was attacked by a squadron of
warforged soldiers led by a warforged titan, resulting in a slaughter. Klabs
barely survived the ordeal, hiding inside some of the machinery.

After the war, Klabs became a train conductor for House Orien, running message
for the Tyrants and spying on passengers.

### Knowledge

- Has known Egan since before the War ended in the arms business.
- Egan knows that Klabs works for the Tyrants, and holds this above his head.
- Let Egan carry his gun with him on the train if he kept the warforged in
  the cargo.

### Contacts

- Has seen Quen around Egan as his retainer, doing his bidding.
- Has met Caeldrim Ahinar, Egan's husband. He was a soldier, and Klabs likes him
  better than Egan.
- Aware of Egan's warforged bodyguard, Blue Shield, in the cargo.

### At the time of the murder

- Was doing the last patrol of the train and had locked up the galley.
- Saw Egan slumped over a table.
- Went back to the staff quarters and slept.
- Found Egan dead just before dawn the next day, still slumped over the table.

### Items

- Wand of Magic Detection (free cast of *detect arcana*)
- Keyring for all the doors on the train.
- Schema of the train.
- Under the floorboard under his bed: A bunch of encrypted messages to the
  Tyrants written in Goblin.

## Shimyra (The Ice Queen)

*Female drow criminal*

{{< figure src="/img/Shimyra.jpg" width="200px" >}}

**Attributes:** Agility d6, Smarts d6, Spirit d8, Strength d6, Vigor d6\
**Skills:** Athletics d4, Common Knowledge d4, Fighting d6, Gambling d4,
Intimidation d6, Notice d6, Persuasion d8, Shooting d4, Spellcasting d6, Stealth
d6, Thievery d6\
**Pace:** 6; **Parry:** 5; **Toughness:** 5 (1)\
**Gear:** Component pouch, sawed-off shotgun (Range 5/10/20, Damage 1-3d6),
supple leather jacket, leggings (+1)\
**Edges:**

- **Arcane Background (Magic):** Has 10 power points, and the spells *empathy*,
  *mind reading*, and *sound/silence*.
- **Superior Darkvision:** Drow ignore all illumination penalties from a lack of
  light. They can see in Pitch Darkness.
- **Trance:** Drow don't need to sleep. Instead, they meditate deeply,
  remaining semiconscious. This trance lasts approximately three hours.

**Hindrances:**

- **Frail:** Drow aren't as sturdy as most other races. Their Toughness is
  reduced by 1.
- **Greedy (Major):** Greed is the primary motivator for Shimyra. She may even
  kill for greed.
- **Secret (Major):** Shimyra assisted in murdering Egan.
- **Sunlight Sensitivity:** Drow suffer a -2 illumination penalty in daylight.
- **Outsider:** Drow are often perceived as dangerous savages from Xen'drik.
  They subtract 2 from Persuasion rolls with people who do not trust drow.

A cold personality, and no apparent connection to anybody on the train, but
Shimyra knew Egan Bakker a long time ago, twenty years or so, before he made his
money. And Shimyra has a debt to collect.

Shimyra is a criminal working for the Daask, an organisation founded by
monstrous immigrants from Droaam. Daask engages mainly in violent crime,
including armed robbery, assault, arson, and murder. As its reputation has
grown, it has added extortion to this list, Shimyra's specialty.

Shimyra had long been a solo operator, blackmailing and extorting the people of
Sharn on her own. But now that she has joined forces with Daask, she is making
loads more money.

Shimyra's modus operandi is simple: gain a little trust, pry a secret, and
return later to collect a debt on the secret. Slowly but certainly, she has
gathered a lot of dirt on a lot of inhabitants of Sharn.

Egan Bakker was a target from her earlier days. He was a dragonmarked heir, but
shunned by his house. At the time, he had nothing of value to blackmail, but
Shimyra wagered that one day she would be able to collect. She "befriended" the
artificer, and after more than a few glasses, he confessed to a sin that was
certain to completely destroy any potential future he had with House Cannith.

The situation had been as such: Merrix d'Cannith is the baron of House Cannith
in Sharn. For some reason or another, she hates the guts of Egan's mother, and
Egan by proxy, thus disallowing him from joining House Cannith. So he deviced a
plan to seduce her daughter, Lora. The intent was to steal her away and force
Merrix to accept him through her daughter, but he failed in this when Lora broke
up with him within a few weeks, before they had gone public. A predictable
amount of months later, she showed up with a child, and demanded a solution. By
that time, Egan had already hooked up with Caeldrim, so the whole thing was
rather unfortunate.

Shimyra assisted Egan in paying off Lora such that she kept her mouth shut about
the affair. Most of the payment came from Shimyra's pocket rather than Egan's.

When Shimyra learnt that Egan was leaving for Fairhaven, she decided to collect
her dues. She got in contact with his retainer, Quen, expecting to arrange a
meeting with Egan. During the meeting, Shimyra and Quen reflected on times of
old, and as Quen reflected on the past, it became clear to her how much Egan had
used her as a disposable aide. Quen laughed and cried, and wondered why she even
bothered with the greedy egotistical artificer. Shimyra correctly guessed that
the khoravar was in complicated love with the man.

Shimyra pressed Quen's buttons, however. She could do with a retainer of her
own, and she could settle with having Quen instead of gold. From there, it did
not take much manipulating to talk Quen into ending Egan. When? When the perfect
opportunity presents itself. And the perfect opportunity did present itself on
the train. They would only have to sow enough confusion to make it as far as
Fairhaven, and then escape.

### Knowledge

- Quen is the only person who has known Egan long enough to know Shimyra, too.
- Aston saw Shimyra once, but Shimyra doesn't think he recognises her.
- Saw Bram as a baby.
- Egan did not expect to see Simyra aboard the train.

### At the time of the murder

- In her cabin, discussing with Quen.
- Quen informed Shimyra that Egan was alone and unconscious.
- Shimyra told Quen that the time is right.
- Quen returned with the gun, and gave it to Shimyra, then hurried back to
  her cabin.

### Items

- An old letter from Egan thanking Shimyra for her help, and promising to pay
  her back someday.

## Quen (The Loyal Retainer)

*Female khoravar retainer*

{{< figure src="/img/Quen.jpg" width="200px" >}}

**Attributes:** Agility d6, Smarts d8, Spirit d8, Strength d4, Vigor d6\
**Skills:** Athletics d4, Common Knowledge d8, Fighting d6, Intimidation d4,
Notice d6+2, Persuasion d8, Research d6, Stealth d4, Thievery d4\
**Pace:** 6; **Parry:** 5; **Toughness:** 5\
**Hindrances:** Loyal (Minor), Secret (Major)\
**Edges:** Alertness\
**Gear:** Dagger (Str+d4)\
**Special Abilities:**

- **Low Light Vision:** Ignores penalties for Dim and Dark Illumination.

Quen has long been at Egan's side, working as a personal assistant, housekeeper,
and confidante. Quen knows all of Egan's secrets and cleaned up after all his
messes all these years. Was he ever even grateful?

Quen came from the town of Riverweep on the border between the Eldeen Reaches
and Aundair. She was a villager, and furious about Aundair's continued war with
the Reaches. As a young rebel, she staged her own plan to destroy the village's
stone bridge going over the river, connecting Aundair to the Reaches. She
acquired explosives and caused the bridge to collapse when an elite squadron of
Knights Arcane happened to be passing over.

Quen fled Riverweep immediately, fearing retaliation by the Royal Eyes of
Aundair. She sought refuge with The Tyrants in Sharn, who were able to fashion
Quen a new identity. More than that, however, they blackmailed her into
continued payments, lest they tell the Royal Eyes.

That was twenty-five years ago. Shortly after that, she entered the service of a
young Egan, who was advertising for a grossly underpaid assistant at the time.
Within a few months, she was unable to make ends meet due to being blackmailed.
Egan "helped" her by paying her monthly dues to The Tyrants. To counter this
good deed, Quen became a live-in retainer with a measly allowance.

Quen helped Egan through thick and thin. At first to prove herself as a
competent retainer, and later because she developed a liking for the artificer.
In truth, she fell in complicated love for him. She hated his lack of empathy
and his huge mistakes that she had to clean up, but there was that *something*
that made her heart beat faster.

The last few days on the train have been ridiculous, however. Having Egan's
husband and lover on the same train, as well as so many people who utterly hate
his guts---it caused Quen to realise how toxic her relation with Egan has been.
Especially when Quen met with Shimyra and discussed the past, things clicked,
and Quen knew what had to be done, and was angry enough to do it.

### Knowledge

- Bakker used people then discarded them.
- Bram's mother (Lora, daughter of Merrix d'Cannith) had come to Egan and he
  paid her to go away.
- Egan did not care that his husband is unhappy, so long as they stayed
  together.
- Egan was blackmailing Klabs for something that happened years ago.
- House Cannith West has already accepted Egan.

### Contacts

- Knows Shimyra.
- Knows Caeldrim Ahinar.
- Knew Aston Oakchin, but kept her distance.
- Saw Bram as a baby, but he will not recognise her.
- Has seen Odi, but has not spoken to him.

### At the time of the murder

- See separate section.

### Items

- Bloodied clothes are soaking in the toilet in the bathroom. The toilet lid is
  closed.
- Burnt hand from firing gun.

# Murder time line

- Caeldrim prepared to sleep, gave himself some sleeping potion, and slipped
  some into Egan's brandy as well.
- Bram saw this happen.
- Quen saw Bram spying on Caeldrim, and also saw the sleeping potion thing.
- Egan took the brandy to have a conversation with Aston. They had a heated
  conversation in which Egan admitted that he would double-cross Aston again.
- Egan passed out drunk, and Aston left him alone.
- Train conductor did a last pass through the cabin before going to sleep.
- Quen murdered Egan with the gun, and burnt her hand.
- Quen took the gun to Shimyra and passed it on to her.
- Quen returned to her cabin and put her bloodied clothes in her toilet.
- Start.

# The game

- Ask players about smuggling.
- Ask players what they have done during the last two days aboard the train.
- Conductor finds Egan's body. Ask to see how he responds.
- Try to include all players in beginning scene.
- After a while, someone will suggest to bring Egan's bodyguard as an extra
  layer of protection.

## Details

- Egan has been shot in the back.
- There is a mostly empty bottle of brandy on the table beside the body.
- Successful Academics/Healing/Survival roll to see that Egan has been dead for
  at least an hour, but no more than five.
- There is a bullet hole in the table under Egan's body, with the bullet lodged inside.
