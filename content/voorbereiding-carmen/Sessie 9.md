---
title: "Sessie 9"
date: 2020-03-12T21:16:58Z
draft: false
---

- Het gezelschap belandt in de troonzaal van de Hofnar. Hoewel de Hofnar een
  troon heeft, zit hij er bijna nooit op, en blijft hij rondspringen. De Hofnar
  is zeer blij om de vrienden van Grinvor te zien, en inspecteert ze uitbundig.

  + "Wat ben jij?" vraagt hij aan Donna. Wanneer ze "half-elf" antwoordt, zegt
    hij "niet waar!" en wijst naar haar feeënvleugels. Voor een moment kan ze
    vliegen, maar de vleugels verdwijnen ook weer snel.

  + "Ik heb nog nooit zo'n vreemde dryade gezien", zegt de Hofnar tegen Bast.
    "Vertel een mop!" Het maakt niet uit wat Bast zegt, en de Hofnar schatert
    van het lachen.

- De Hofnar verklaart dat hij de pegasus omgeruild heeft voor een hippocampus.
  Waarom? De lol, natuurlijk. De pegasus was daar al zó lang, het was tijd voor
  iets nieuws. De Hofnar moet lang nadenken als hem gevraagd wordt of de pegasus
  terug mag naar Varna. Hij zegt dat hij er een nachtje over moet slapen, en
  nodigt het gezelschap uit om morgen terug te komen.

- In de tussentijd mag het gezelschap rondkijken in zijn Thelanis.

- In Thelanis wonen feetjes, satyrs, kabouters, feeëndraken, centaurs, gnomen,
  elven, et cetera.

- In Thelanis is het warm, maar sneeuwt het een klein beetje.

- Wanneer de spelers terugkomen heeft de Hofnar een paar eisen. Hij heeft
  binnenkort een feestje, en wil met de pegasus pronken op het feestje. Hij
  heeft de hulp van het gezelschap nodig om het feest goed te laten lopen.

  + Hij heeft zijn slippers nodig. Frendamus (gnome, m) zou zijn slippers voor
    hem maken, maar hij heeft nog niets gehoord.

  + Hij wil graag een gebraden zwijn (*giant boar*, medium) geven aan zijn
    gasten. Het bijzondere aan dit beest is dat het gebraden geboren is. Zodra
    het gespit wordt, stopt het beest met bewegen. Het zwijn is zeer zeldzaam,
    en het gezelschap moet meedoen aan de Wilde Jacht.

  + Hij wil graag dat het gezelschaap aanwezig is op zijn soirée.

- De Hofnar is verliefd op Bel'Ataka, een agressieve krijger. Bel'Ataka is niet
  verliefd op de Hofnar---de Hofnar is te zwak en heeft niet genoeg strijdlust.
