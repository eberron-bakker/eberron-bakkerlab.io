---
title: "Reis"
date: 2019-10-30T20:36:46+01:00
draft: false
---

# Belangrijke regels

## Extreme Cold (DMG 110)

Whenever the temperature is at or below -17 degrees Celsius, a creature exposed
to the cold must succeed on a DC 10 Constitution saving throw at the end of each
hour or gain one level of exhaustion. Creatures with resistance or immunity to
cold damage automatically succeed on the saving throw, as do creatures wearing
cold weather gear (thick coats, gloves, and the like) and creatures naturally
adapted to cold climates.

## Strong Wind (DMG 110)

A strong wind imposes disadvantage on ranged weapon attack rolls and Wisdom
(Perception) checks that rely on hearing. A strong wind also extinguishes open
flames, disperses fog, and makes flying by nonmagical means nearly impossible. A
flying creature in a strong wind must land at the end of its turn or fall.

A strong wind in a desert can create a sandstorm that imposes disadvantage on
Wisdom (Perception) checks that rely on sight.

## Heavy Precipitation (DMG 110)

Everything within an area of heavy rain or heavy snowfall is lightly obscured,
and creatures in the area have disadvantage on Wisdom (Perception) checks that
rely on sight. Heavy rain also extinguishes open flames and imposes disadvantage
on Wisdom (Perception) checks that rely on hearing.

## Frigid Water (DMG 110)

A creature can be immersed in frigid water for a number of minutes equal to its
Constitution score before suffering any ill effects. Each additional minute
spent in frigid water requires the creature to succeed on a DC 10 Constitution
saving throw or gain one level of exhaustion. Creatures with resistance or
immunity to cold damage automatically succeed on the saving throw, as do
creatures that are naturally adapted to living in ice-cold water.

## Foraging (DMG 111)

Characters can gather food and water as the party travels at a normal or slow
pace. A foraging character makes a Wisdom (Survival) check whenever you call for
it, with the DC determined by the abundance of food and water in the region.

|         Food and Water Availability         |  DC   |
| :-----------------------------------------: | :---: |
|       Abundant food and water sources       |  10   |
|       Limited food and water sources        |  15   |
| Very little, if any, food and water sources |  20   |

If multiple characters forage, each character makes a separate check. A foraging
character finds nothing on a failed check. On a successful check, roll 1d6 + the
character's Wisdom modifier to determine how much food (in pounds) the character
finds, then repeat the roll for water (in gallons).

### Food and Water (DMG 111)

The food and water requirements noted in the Player's Handbook are for
characters. Horses and other creatures require different quantities of food and
water per day based on their size. Water needs are doubled if the weather is
hot.

| Creature Size | Food per Day | Water per Day |
| :-----------: | :----------: | :-----------: |
|     Tiny      |  1/4 pound   |  1/4 gallon   |
|     Small     |   1 pound    |   1 gallon    |
|    Medium     |   1 pound    |   1 gallon    |
|     Large     |   4 pounds   |   4 gallons   |
|     Huge      |  16 pounds   |  16 gallons   |
|  Gargantuan   |  64 pounds   |  64 gallons   |

## Food and Water (PHB 185)

Characters who don't eat or drink suffer the effects of exhaustion (see the 
appendix). Exhaustion caused by lack of food or water can't be removed until the
character eats and drinks the full required amount.

### Food (PHB 185)

A character needs one pound of food per day and can make food last longer by
subsisting on half rations. Eating half a pound of food in a day counts as half
a day without food.

A character can go without food for a number of days equal to 3 + his or her
Constitution modifier (minimum 1). At the end of each day beyond that limit, a
character automatically suffers one level of exhaustion.

A normal day of eating resets the count of days without food to zero.

### Water (PHB 185)

A character needs one gallon of water per day, or two gallons per day if the 
weather is hot. A character who drinks only half that much water must succeed on
a DC 15 Constitution saving throw or suffer one level of exhaustion at the end 
of the day. A character with access to even less water automatically suffers one
level of exhaustion at the end of the day.

If the character already has one or more levels of exhaustion, the character
takes two levels in either case.

## Speed (PHB 181)

Every character and monster has a speed, which is the distance in feet that the
character or monster can walk in 1 round. This number assumes short bursts of
energetic movement in the midst of a life-threatening situation.

The following rules determine how far a character or monster can move in a
minute, an hour, or a day.

### Travel Pace (PHB 181)

While traveling, a group of adventurers can move at a normal, fast, or slow
pace, as shown on the Travel Pace table. The table states how far the party can
move in a period of time and whether the pace has any effect. A fast pace makes
characters less perceptive, while a slow pace makes it possible to sneak around
and to search an area more carefully (see the "Activity While Traveling" section
later in this chapter for more information).

**Forced March.** The Travel Pace table assumes that characters travel for 8
hours in day. They can push on beyond that limit, at the risk of exhaustion.

For each additional hour of travel beyond 8 hours, the characters cover the 
distance shown in the Hour column for their pace, and each character must make a

Constitution saving throw at the end of the hour. The DC is 10 + 1 for each hour
past 8 hours. On a failed saving throw, a character suffers one level of
exhaustion (see the appendix).

**Mounts and Vehicles.** For short spans of time (up to an hour), many animals
move much faster than humanoids. A mounted character can ride at a gallop for
about an hour, covering twice the usual distance for a fast pace. If fresh
mounts are available every 8 to 10 miles, characters can cover larger distances
at this pace, but this is very rare except in densely populated areas.

Characters in wagons, carriages, or other land vehicles choose a pace as normal.
Characters in a waterborne vessel are limited to the speed of the vessel (see
chapter 5), and they don't suffer penalties for a fast pace or gain benefits
from a slow pace. Depending on the vessel and the size of the crew, ships might
be able to travel for up to 24 hours per day.

Certain special mounts, such as a pegasus or griffon, or special vehicles, such
as a carpet of flying, allow you to travel more swiftly. The Dungeon Master's
Guide contains more information on special methods of travel.

|  Pace  | Distance Traveled per Minute | Distance Traveled per Hour | Distance Traveled per Day |                      Effect                      |
| :----: | :--------------------------: | :------------------------: | :-----------------------: | :----------------------------------------------: |
|  Fast  |           400 feet           |          4 miles           |         30 miles          | -5 penalty to passive Wisdom (Perception) scores |
| Normal |           300 feet           |          3 miles           |         24 miles          |                        -                         |
|  Slow  |           200 feet           |          2 miles           |         18 miles          |               Able to use stealth                |

### Difficult Terrain (PHB 182)

The travel speeds given in the Travel Pace table assume relatively simple
terrain: roads, open plains, or clear dungeon corridors. But adventurers often
face dense forests, deep swamps, rubble-filled ruins, steep mountains, and
ice-covered ground—all considered difficult terrain.

You move at half speed in difficult terrain—moving 1 foot in difficult terrain
costs 2 feet of speed—so you can cover only half the normal distance in a
minute, an hour, or a day.

## Special Travel Pace (DMG 242)

The rules on travel pace in the Player's Handbook assume that a group of
travelers adopts a pace that, over time, is unaffected by the individual
members' walking speeds. The difference between walking speeds can be
significant during combat, but during an overland journey, the difference
vanishes as travelers pause to catch their breath, the faster ones wait for the
slower ones, and one traveler's quickness is matched by another traveler's
endurance.

A character bestride a phantom steed, soaring through the air on a carpet of
flying, or riding a sailboat or a steam-powered gnomish contraption doesn't
travel at a normal rate, since the magic, engine, or wind doesn't tire the way a
creature does and the air doesn't contain the types of obstructions found on
land. When a creature is traveling with a flying speed or with a speed granted
by magic, an engine, or a natural force (such as wind or a water current),
translate that speed into travel rates using the following rules:

- In 1 minute, you can move a number of feet equal to your speed times 10.
- In 1 hour, you can move a number of miles equal to your speed divided by 10.
- For daily travel, multiply your hourly rate of travel by the number of hours
  traveled (typically 8 hours).
- For a fast pace, increase the rate of travel by one-third.
- For a slow pace, multiply the rate by two-thirds.

For example, a character under the effect of a wind walk spell gains a flying
speed of 300 feet. In 1 minute, the character can move 3,000 feet at a normal
pace, 4,000 feet at a fast pace, or 2,000 feet at a slow pace.

The character can also cover 20, 30, or 40 miles in an hour. The spell lasts for
8 hours, allowing the character to travel 160, 240, or 320 miles in a day.

Similarly, a phantom steed spell creates a magical mount with a speed of 100
feet that doesn't tire like a real horse. A character on a phantom steed can
cover 1,000 feet in 1 minute at a normal pace, 1,333 feet at a fast pace, or 666
feet at a slow pace. In 1 hour, the character can travel 7, 10, or 13 miles.

## Random Encounter (DMG 85-87)

Roll a d20. If the result is 18 or higher, a random encounter occurs.

# Reizen

Gooi de volgende scenario's naar de party:

- (1,2,3) Na een paar dagen reizen: Een stoet van wolvenrijders (*tribal
  warrior*, *dire wolf*) komt direct op de party aanrijden richting Riviertraan.
  De wolven zullen niet wijken. Bast moet een DC 15 Deception check halen, of
  aangevallen worden door een van de wolvenrijders ("Gedrocht!").

- (1,2,3) Na een week: Party komt een postbode van Huis Orien tegen die de nieuwste
  editie van de Kroniek van Korranberg heeft. Als zijkop: "Eldeen-ent ziek,
  onbekende oorzaak". De Aundairse Spreuk, daarentegen, heeft dit als
  krantenkop: "Corruptie in Rivierwoud: zijn de Wachters van het Woud wel
  bekwame regeerders?"

## 1. Groenhart

1100 mijl (46 dagen) reizen via de wegen, waarvan 400 mijl (3 dagen (40 gp) of 1
dag (80 gp, Huis Lyrandar)) via water, voor totaal van 30-33 dagen reizen.

800 mijl (33 dagen) reizen hemelsbreed.

Alternatief, 200 mijl (8 dagen) reizen naar Varna, en een *speaking stone* van
Huis Sivis gebruiken om een bericht te sturen naar Delethorn.

## 2. Gaard

100 mijl (4 dagen) reizen door het woud.

## 3. Varna

150 mijl (6 dagen) reizen via de weg.
