# Bast

**Attributes:** Agility d4, Smarts d6, Spirit d6, Strength d10, Vigor d8\
**Skills:** Athletics d8, Druidism d6, Fighting d8, Intimidation d6, Notice d6,
Survival d8\
**Languages:** Common d8, Sylvan d8\
**Pace:** 5; **Parry:** 6; **Toughness:** 13 (6)\
**Wounds:** 0; **Fatigue:** 0; **Power Points:** 10/10\
**Gear:** Mossy stone (Str+d8+1, Reach 1), javelin (Range 3/6/12, Damage Str+d6,
Reach 1), handaxe (Range 3/6/12, Damage Str+d6), wand of birdcalls\
**Hindrances:**

- **Bulky:** Warforged subtract 2 from Trait rolls when using equipment that
  wasn't specifically designed for warforged. Equipment costs double the listed
  price for warforged. <!-- -2 -->
- **Factory Settings:** Warforged were designed, built, and trained for a single
  purpose. Their default knowledge and abilities outside of that purpose is
  limited. They do not have Common Knowledge, Notice, Persuasion, and Stealth as
  core skills. <!-- -4 -->
- **Outsider (Major):** With a long history of being dedicated soldiers of war,
  warforged have difficulty blending into post-war Khorvaire. Most non-warforged
  strongly dislike the sight of warforged as they stand as living reminders of
  the horror that was the Last War. As such, warforged have –2 to Persuasion
  rolls when interacting with people who look down on them. Furthermore,
  although the Treaty of Thronehold grants the warforged rights, not all places
  equally abide by that provision of the treaty. <!-- -2 -->
- **Quirk:** Warforged often display an odd personality trait or two, given how
  new they are to the world. They have the Quirk Hindrance. (Answers binary
  questions with a singular binary answer.)
- **Slow (Minor):** Bast's pace is reduced by 1, and his Running Die is a d4.
- **Vow (Minor):** Bast has sworn a vow of allegiance to the Wardens of the
  Wood.
- **Wanted (Major):** Aundair considers Bast a traitor from the Last War.

**Edges:**

- **Arcane Background (Druidism)**
- **Brute (modified):** Athletics and Fighting are linked to Strength instead of
  Agility.
- **Integrated Protection:** A warforged is covered in tough plating made of
  magically-treated woods and metals. Warforged gain Armor +6. Unlike other
  natural Armor, this natural Armor counts as one layer (see **Armor** in the
  *Savage Worlds* core rules). The Minimum Strength requirement of the
  additional layer is increased by one die type, regardless of whether it is
  heavier or not. The natural Armor may be improved, replaced, or even
  removed---GM's call. <!-- +2 -->
- **Living Construct:** Warforged add +2 to recover from being Shaken, ignore
  one level of Wound modifiers, do not need to breathe, eat, or drink, and are
  immune to poison and disease. Instead of sleeping, warforged recharge in an
  inactive, motionless, but conscious state. Warforged cannot heal naturally.
  Healing requires the Repair skill, which takes one hour per current Wound
  level per attempt and is not limited to the "**Golden Hour**". Alternatively,
  the powers *healing* and *repairing* can heal a warforged. <!-- +8 -->
- **Size +1:** Warforged tower over their human makers, increasing their Size
  (and therefore Toughness) by 1. <!-- +1 -->

**Powers:**

- **Ensnare (entangle, p. 161):** PP 2; Range Smarts; Duration Instant; Target
  is Entangled, or Bound on a raise. The vines are Hardness 5.
- **Goodberry (healing, p. 162):** PP 3; Range Touch; Duration Instant; Remove
  one Wound, or two with a raise. Victim’s Wounds penalty to Druidism.
- **TODO name (smite, p. 168):** PP 2; Range Smarts; Duration 5; Weapon damage
  is increased by +2, or +4 with a raise.

**Gear:**

- Mossy Stone (6 lbs)
- Javelin (2 lbs)
- Handaxe (2 lbs)
- Backpack (2 lbs)
- Cloak (2 lbs)
- Driftglobe (1 lbs) --- When you speak a command word, the *light* spell
  emanates from the driftglobe. When you speak another command word, it floats
  and follows you.
- Maul (10 lbs)
- Potion of climbing (0.5 lbs) --- When consumed, you have +2 to all Athletics
  rolls to do with climbing. Pace is not slowed while climbing.
- Potion of healing (0.5 lbs) --- Removes one Wound when used, but causes
  Fatigue.
- Rations (7, 1 lbs)
- Rope, hemp (20 yards) (15 lbs)
- Flint and steel (1 lbs)
- Torch (one hour, 4" radius) (10, 1 lbs)
- Wand of birdcalls (1 lbs) --- When used, creates the sound of a bird.

**Advances:**

1. Arcane Background (Druidism)
2. Improve Strength

# Slablaadje

**Attributes:** Agility d4, Smarts d6, Spirit d6, Strength d4-3, Vigor d6\
**Skills:** Common Knowledge d4, Notice d4, Repair d4, Stealth d10\
**Pace:** 3; **Parry:** 2; **Toughness:** 3\
**Edges:** ---\
**Special Abilities:**

- **Rake:** Str.
- **Size -2 (Small)**

# Fee

**Attributes:** Agility d8, Smarts d6 (A), Spirit d6, Strength d8, Vigor d6\
**Skills:** Athletics d8, Fighting d6, Notice d8+2, Stealth d10\
**Pace:** 8; **Parry:** 5; **Toughness:** 4\
**Edges:** Alertness (+2 to sense-based Notice rolls)\
**Special Abilities:**

- **Bite/Claws:** Str+d6.
- **Pounce:** If a panther can leap at least 1" and makes a Wild Attack, it adds
  +4 to its damage instead of +2.
- **Size -1**
- **Speed:** d10 running die.

# Powers

## Entangle (p. 161)

**Power Points:** 2\
**Range:** Smarts\
**Duration:** Instant

Entangle allows the caster to restrain a target with vine-like Trappings
(Hardness 5). If successful, the target is Entangled. With a raise, he's Bound.

Victims may break free on their turn as detailed under Bound & Entangled on page
98.

**Modifiers:**

- **Area of Effect (+2/+3):** For +2 points the power affects everyone in a
  Medium Blast Template. For +3 points the area of effect is increased to a
  Large Blast Template.
- **Strong (+2):** The vines are more resilient. Rolls to break free are made at
  -2 and its Hardness increases to 7.

## Healing (p. 162)

**Power Points:** 3\
**Range:** Touch\
**Duration:** Instant

Healing removes Wounds less than an hour old. The penalty to the caster's arcane
skill roll is the victim's Wounds (max -3). A success removes one Wound, and a
raise removes two. The power may be cast additional times to remove additional
Wounds.

**Modifiers:**

- **Greater Healing (+10):** Can restore any Wound, including those more than an
  hour old.
- **Crippling Injuries (+20):** Heal a permanent Crippling Injury (p. 95).
  Requires an hour of preparation, and can only attempt once per injury. If it
  fails, this caster cannot heal that particular injury. If successful, the
  subject is Exhausted for 24 hours.
- **Neutralize Poison or Disease (+1):** Successful roll negates any poison or
  disease. If the poison or disease has a modifier, apply it to the arcane skill
  roll.

## Smite (p. 168)

**Power Points:** 2\
**Range:** Smarts\
**Duration:** 5

This power is cast on a weapon of some sort. If it's a ranged weapon, it affects
one entire magazine, 20 bolts, shells, or arrows, or one full "load" of
ammunition. While the power is in effect, the weapon's damage is increased by
+2, or +4 with a raise.

**Modifiers:**

- **Additional Recipients (+1):** The power may affect more than one target for
  1 additional Power Point each.
