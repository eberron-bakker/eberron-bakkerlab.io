---
title: "Tree Stride"
date: 2020-05-02T21:35:46+01:00
draft: false
---

# Tree Stride (Teleport, p. 170, WCCtK)

**Power Points:** 2\
**Range:** Smarts\
**Duration:** Instant\
**Trapping:** The target must travel via one plant to another.

*Teleport* allows a character to disappear and instantly reappear up to 12" (24
yards) distant, or double that with a raise. Teleporting to an unseen location
incurs a -2 penalty on the arcane skill roll.

Opponents adjacent to a character who teleports away don't get a free attack
(see **Withdrawing from Melee**, page 109).

If casting *teleport* on a willing subject, the caster decides where they move
to, not the target.

**Modifiers:**

- **Additional Recipients (+1)**: The power may affect more than one target for
  1 additional Power Point each.
- **Teleport Foe (+2):** Foes may be targeted by a **Touch** attack (page 108).
  This is an action, so the casting must be the second part of a Multi-Action if
  the attack is successful. The foe resists the casting with an opposed Spirit
  roll against the arcane skill total and is sent up to 12" away with a success
  and 24" with a raise. Foes may not be teleported into solid objects.
- **Transport via Plants (+20):** Instead of teleporting a short distance, the
  target is teleported a long distance. The caster spends a minute to prepare
  the teleportation at a plant, and the target is then teleported to a plant the
  caster's choice with which the caster is familiar. Both plants must be of the
  same species, and the target plant must be magically enchanted to enable this
  type of teleportation. The teleportation still incurs the -2 penalty for
  teleporting to an unseen location.
