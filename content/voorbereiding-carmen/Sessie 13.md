---
title: "Sessie 13"
date: 2020-05-02T12:09:07+01:00
draft: false
---

- Lentefeest in drie dagen: Drukte in Groenhart, word gevierd met groot
  feestmaal: Vroege wijnen, kersen, lammetjes, radijsjes, etc.

- Ontmoeting met Oalian moet wachten tot na het lentefeest. Het is immers zo
  druk.

- Een khoravar genaamd [**Leselor**](/karakter/leselor) vindt het gezelschap in
  de boomhut. Ze is een agent van de Koninklijke Ogen van Aundair, maar is super
  vriendelijk tegen het gezelschap.

  + Leselor heeft gehoord dat het gezelschap uit Riviertraan komt. Ze heeft zelf
    familie in Riviertraan, zegt ze. Nou ja, familie, ze heeft een poosje met
    **Adrianne** samengewoond in Varna toen Adrianne jonger was. Leselor weet
    dat Adrianne vreemdgaat met Cédric, maar zegt niks.

  + In werkelijkheid volgt Leselor het gezelschap al een tijdje, maar was ze
    kwijtgeraakt in Varna. Ze is zich bewust van een soort van complot rond
    Wilgspruit. Wat ze weet:

    * Wilgspruit is vergiftigd op orders van Aundair.

    * Fhazira is informant voor Aundair.

    * De shiftergemeenschap in het Rivierwoud heeft iets met de vergiftiging van
      Wilgspruit te maken.

    * Leselor houdt de Wachters van het Woud in de gaten zodat ze er niet achter
      komen dat Aundair betrokken is.

    * Heer Darro wil oorlog met Eldeen, maar Koningin Aurala wil eerst een goede
      casus belli creëren via sabotage.

- Leselor vraagt het gezelschap of ze op de dag van het Lentefeest samen feest
  willen vieren. Ze kent een paar leuke plekken, en wil graag met het gezelschap
  opstappen. Normaal neemt ze haar vriendin (**Elyra**) mee, maar die kon dit
  jaar niet.

- Feest:

  + Over heel Groenhart zijn activiteit verspreid. Er is geen centrale
    ceremonie.

  + Hrazhak --- Traditionele shifter-sport. 4/5/6/7 spelers op een veld met twee
    goals. Om te scoren moet het team de token van de tegenstander afpakken, en
    allebij de tokens in hun eigen goal plaatsen.

    * Veld is vol met obstakels.
    * Geen contactlimiet, maar geen wapens.
    * Geen spreuken.
    * Winnaar krijgt bag of holding.

  + Bloemengroeien --- Het is lente, en dan groeit alles weer. Er is een
    wedstrijd om de mooiste bloem tot bloeiing the brengen. Leselor is jurylid.
    Haar vriendin zou ook op de jury zitten, maar die is er dit jaar niet.
    Misschien wil iemand uit het gezelschap op de jury zitten? (Druidism check +
    beschrijving.) Kandidaten:

    * Een verlegen man --- Een knalrode roos.
    * Een vrouw in een zonnejurk op blote voeten --- Een zonnebloem die zich
      richt tot mooie mensen.
    * Een goblin-boer --- Een rood-paarse lotusbloem die langzaam ronddraait in
      een kom water.
    * Ĉevalo (blind) (d12, Wild Die) --- Een cactus met kleine bloemetjes tussen
      de naalden.
    * Een jong kind --- Een geplukt madeliefje.

  + Regendans --- Georganiseerd door Huis Lyrander. Sommige leden van Huis
    Lyrander gebruiken een dans om regen te veroorzaken. De dans wordt uitgelegd
    aan de aanwezigen. De dans is iets spectaculairder gemaakt, met sprongen en
    pirouetten en synchronisatie en onmenselijke lenigheid. Smarts roll om de
    choreografie te onthouden, anders -2 op Performance roll. Met raise, +2 op
    Performance roll.

    De khoravar van Huis Lyrander zorgt ervoor dat er een beetje regen komt.
    Omdat het nog fris is, is er **een klein beetje vorst aan de grond** tussen
    de grassprietjes.

  + Verrassingsverstoppen --- Een jonge man genaamd **Ari** doet mee aan
    verrassingsverstoppen. Zijn vriendin (**Aafke**) heeft hem een cryptische
    hint gegeven om zijn cadeautje te vinden, en hij kan er maar niet achter
    komen. Hij identificeert Grinvor als gnoom (fout), en neemt aan dat hij slim
    is, en hem kan helpen.

    * De hint is "Bij het watergevecht, maar niet bij de koning die failliet
      is". Het is een cryptogram voor "aquarel" (aqua-rel). "de koning die
      failliet is" betekent "vorst aan de grond" → regendans.

    * Bij het schilderen vindt Ari een prachtig aquarel-schilderij van hem en
      zijn vriendin. Zijn mond valt open, en hij bedankt het gezelschap. Het
      gezelschap krijgt een veer-token van Ari. Hij heeft het zelf van een
      handelaar uit Sharn gekregen.

- Einde feest: Leselor vraagt het gezelschap heel veel vragen over hun missie.

- In het centrum van Groenhart is de gaarde van Oalian. Bomen vormen een ring
  rond een plas spiegelglad water. In het centrum van de gaarde staat Oalian,
  een wilg van zestig meter hoog, met dwarrelende takjes en bladertjes over de
  gehele gaarde.

- Zoektocht naar Mumzok: Mumzok is een goede vriend van Oalian. Hoe of waarom
  vertelt Oalian niet. Hij was recentelijk nog in Groenhart. Een week geleden,
  of was het twee?

- Mumzok onderhoudt een poortwachterbarrière in Eldeen, niet heel ver van
  Groenhart. Een dag reizen. Hij onderhoudt de barrière samen met
  [**Anita**](/karakter/anita), een halfling doler.

- Voordat Mumzok kan vertrekken moet hij een ritueel uitvoeren om de barrière te
  vernieuwen. Hij vraagt hulp van Donna en Bast.

- Dolgrim (6x) attack + Spectator (WC)

## Spectator

**Attributes:** Agility d6, Smarts d10, Spirit d6, Strength d4, Vigor d6\
**Skills:** Common Knowledge d4, Cosmology d8, Fighting d6, Intimidation d10,
Notice d12, Spellcasting d8, Stealth d8\
**Pace:** 1; **Parry:** 5; **Toughness:** 3\
**Edges:** Alertness\
**Special Abilities:**

- **Bite/Claws:** Str+d4.
- **Flight:** Spectators have a Flying Pace of 10".
- **Low Light Vision:** Spectators ignore penalties for Dim and Dark
  Illumination.
- **Size -2 (Small):** It's a flying head.
- **Spellcasting:** 20 Power Points, can cast *bolt*, *confusion*, *fear*,
  *stun*.
- **Spell Reflection:** If a spell targeted at a spectator missed or failed to
  affect it, the spectator can target another creature within 10" to be targeted
  instead.
- **Telepathy:** Can cast communicate freely via telepathy.
