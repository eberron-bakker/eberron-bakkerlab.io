---
title: "Sessie 1"
date: 2019-10-27T12:45:20+01:00
draft: false
---

# Introductie

- Papa:

Papa, jij reist al een poosje door de provincie van Aundair. Vroeg in de ochtend
ben jij vertrokken, en je loopt al uren langs de rivier *Wynarn*. Onderweg ben
jij je liedjes aan het oefenen. Je bent verscheidene reizigers tegemoet gekomen:
een peloton arcanisten, een zwanenfamilie die de weg overstak, een
handelscaravaan die je een appel gaf voor onderweg, en een postbode-te-paard die
duidelijk haast had.

De zon staat nu op z'n hoogst, maar dat is niet bijzonder hoog. Het is 1
Zarantyr, midwinter. Ondanks dat het winter is, is het houdbaar warm vandaag.
Aangezien je al een poosje aan het reizen bent, is het bijna tijd voor een
pauze. Op de horizon denk je een dorpje te zien, dus je gaat nog even door.

> Papa, wie ben jij?

Je ziet een bordje "Riviertraan, Aundair" bij de ingang van het dorp. Het dorpje
bestaat voornamelijk uit simpele, praktische houten huisjes. Geen van de huizen
ziet er bijzonder rijk uit. Het mooiste huis is een winkel, De Eerlijke Nar, met
een leuk schilderij van een nar op een uithangbord. De winkel lijkt echter
gesloten, maar naast de deur zie je een prikbord. Op het bord zie je een krant,
de Kroniek van Korranberg, met de volgende kop:

> Koningin Auralia noemt verbranding van Aundairiërs in Thaliost "inhumaan",
> dreigt met handelsoorlog.

Het artikel beschrijft hoe Aundairse burgers in Thaliost---gebied dat ingenomen
is door Thrane tijdens de oorlog---publiekelijk verbrand werden door de
aardbisschop. Ze werden verbrand voor ketterij tegen de Kerk van de Zilveren
Vlam. De koningin van Aundair, Auralia, heeft diplomatische dreigingen gemaakt
richting Thrane. Dit is de tweede keer dat Aundair recentelijk hevig bezwaar
gemaakt heeft tegen de bezitting van Thaliost.

Vervelend nieuws. Je stapt weg van het prikbord, en kijkt wat meer om je heen.
Je merkt op dat het dorp best wel verlaten lijkt. Er loopt niemand over straat,
en alleen in de verte hoor je wat geluid.

Je volgt je oren richting het geluid. Je denkt gelach en gezang te horen, maar
het is ver weg. Je volgt de straat en ziet een stenen brug naast een wachttoren.
De brug moet zeker 80 meter breed zijn om de rivier over te kunnen steken.

Hoe dichter je bij de brug komt, hoe luider het geluid. Aan de overkant van de
rivier zie je dan allemaal brandende kandelaren en zingende en dansende mensen:
Een feestje. Je weet niet wat voor een feestje, maar je hebt je instrumenten al
klaar, en stapt de brug op.

"Haaaalt!", hoor je van achteren. Een Aundairse soldaat haalt je aan. "De brug
is kappot", zegt ze, en wijst naar het middelste gedeelte van de brug, waar een
hele sectie mist. Ze vraagt je verder naar een reisdocument. Nadat ze jouw
document geverifiëerd heeft, wijst ze je naar een aanlegplaats naast de brug,
waar een pont mensen heen en weer brengt.

Het duurt niet zo lang, en een man komt aanroeien van de andere kant. De man
lijkt bijzonder geïnteresseerd in jouw raptor en zegt: "Ach, laat die zilver
maar zitten; het is Midwinterfeest!"

Het duurt ten minste vijf minuten voordat je je raptor zover krijgt om in het
roeibootje te stappen. Je hebt geprobeerd om de raptor rustig aan te halen, te
lokken met voer, of gewoon simpelweg te sturen via de teugels. Geen van deze
pogingen slaagden, maar op een gegeven moment---na een beetje geduld---stapt de
raptor van zichzelf in het bootje.

Bij aankomst van de andere kant van de rivier is de atmosfeer een hoop warmer.
Er is gezang, gepraat, wortelsoep die de ronte doet, een paar mensen die uiterst
dronken zijn, en gelach. En als bard, meng jij je snel in de menigte.

- Mama:

Je heugt je al een poosje op het Midwinterfeest. Bij elke seizoenswisseling
houden de druïdes van Eldeen altijd een feest dat de hele dag duurt. Vandaag is
het kortste feest, omdat de dag het kortst is, maar het feest is er niet minder
om.

Je hebt de familie van Het Gebroken Glas geholpen om voorbereidingen te doen
voor dit feest. Hoewel de familie niet het druïdische geloof volgt, zijn ze toch
degenen die de meeste moeite doen voor dit driemaandelijkse feest. De hele
herberg is omgetoverd naar een winterwonderland. De stoelen en tafels zijn van
magisch ijs. Er hangen sneeuwvlokjes binnen en buiten aan de muur. Er staat een
sneeuwpop naast de deur buiten, ookal is het weer matig. Er zijn overal warme
oranje kaarslichtjes. En de familie ziet er uit en zijn verkleed als
winter-eladrin; koud-blauwe winterse elven uit het Feeënhof.

> Mama, wie ben jij?

Voor dit feest heb jij veel voedsel voorbereid. Wortelgewassen van het land heb
je verwerkt in warme wortelsoepjes en wintersalades. Al voor zonsopkomst---en
ook gisteravond---was jij in de keuken van Het Gebroken Glas, gerechten aan het
voorbereiden. Maar nu is het even pauze voor je, en je geniet van de
feestelijkheden.

- Stefan:

Je hebt al een poosje in een klein kamp in het Rivierwoud gewoond. Je
overnachtte er met andere Wachters van het Woud, als boswachters. Je hebt een
oogje op het bos gehouden; bosbranden voorkomen, dode bomen ruimen, zieke bomen
helen, verdwaalde reizigers helpen, dierpopulaties in de gaten houden, katten
uit bomen redden, en de grens van Eldeen bewaken *in het geval dat*.

Een paar dagen geleden kwam Eenpoot---een shifter-druïde uit Riviertraan---langs
met een uitnodiging voor het Midwinterfeest. Je bent al een paar dagen onderweg
met een middelgrote groep van Wachters, richting het dorp Riviertraan. Je reist
langs rivierbeekjes die door de heuvels van het bos naar beneden stromen,
richting de rivier Wynarn. Je reist langs bomen die nog steeds wat kleurrijke
herfstbladeren vasthouden, maar zeker niet meer voor lang. Gister was het
bijzonder koud en lag er sneeuw op gedeeltes van de tocht door het woud, maar
vandaag is het weer mild.

En nu zie je het dorp langs de rand van het bos.

> Stefan, wie ben jij?

De inwoners van Riviertraan verwelkomen de Wachters van het Woud met gejuich.
Iedereen wordt een soepje aangeboden, maar jij weigert, en probeert een beetje
een rustig plekje te vinden om te kunnen wennen aan de drukte van de beschaving.

Maar je hebt de kans niet, en je krijgt een ijspegel in je handen gedrukt, en
wordt meegenomen naar een dichtbijzijnd open veld. Daar zie je een menselijke
vrouw en een halfling-man staan naast een rood vlaggetje, ijspegels in de hand.
Verderop in het veld staan een gnoom-man, een khoravar-vrouw, en een menselijke
vrouw naast een blauw vlaggetje. (Pinoki, Fhazira, Adrianne)

- Mama, Papa, jullie hebben ook ijspegels in de handen geduwd gekregen (deel
  items uit).

- Spelregels:

  + Je moet de vlag van de tegenstander naar je thuisbasis brengen.
  + Je wint als je de twee vlaggen bij je thuisbasis hebt staan.
  + Als je geraakt wordt door een sneeuwbal, dan moet je 6 seconden stil staan,
    en de vlag neerzetten.
  + Geen lichaamscontact.
  + Variant-regel: Diagonaal bewegen.
  + Vlag oppakken kost actie.
  + Vlag neerzetten kost actie.
  + De thuisbasissen staan 80 voet uit elkaar.

- Na het gevecht, stop de spelers in Het Gebroken Glas om bij te komen met een
  warme glühwein.

- Na een poosje, beschrijf een sloom aardbevend geluid. Boem. Boem. Boem. Rustig
  luider wordend, en heel consistent.

- Wanneer de spelers buiten komen, beschrijf een ent (Wilgspruit) die vanuit de
  bosrand richting de rivier loopt, en zich daar "beplant".

- De inwoners van Riviertraan snappen het niet zo goed. Er is wat geroezemoes, en
  een enkeling is oprecht bang voor de ent.

- Eenpoot lijkt ernstig. Hij gaat naar Wilgspruit bij de rivier en probeert met
  de boom te communiceren. Hoewel de boom bewust is, en de aanwezigheid van
  anderen lijkt te bevestigen, weigert de boom om terug te communiceren; te moe.

- Met een DC 20 arcana check weet je wat er mis is met de boom. Alternatief,
  gebruik *detect poison and disease*.

- Eenpoot weet niet zo goed wat hij moet doen. Hij weet de volgende dingen:

  1. Oalian moet zo spoedig mogelijk ingelicht worden -> Reis naar het westen.
  2. De oorzaak moet onderzocht worden -> Reis naar gaard, ondervraag relevante
     figuren.
  3. Een geneesmiddel moet gezocht worden -> Reis naar Varna voor Huis Jorasco.

- Eenpoot vraagt de party om één van deze dingen te doen. Hij heeft geen
  beloning, maar is zeker dat de inwoners van Riviertraan best wel wat willen
  inzamelen, mocht het nodig zijn.

- Suggereer dat de party voorbereidingen doet voordat ze echt vertrekken.
