---
title: "Sessie 16"
date: 2020-06-27T15:37:54+02:00
draft: false
---

- Volgende nacht/ochtend: Bast staat buiten aan het rusten. Hij ziet Leselor
  naar buiten stappen. Ze fluit en houdt haar hand om hoog. Een zwarte raaf
  landt op haar hand, en ze fluistert het beest toe.

# Reis naar Riviertraan

- Duurt 8 dagen om te reizen naar Delethorn.

- 300 sp per persoon van Delethorn naar Riverweep. Duurt vier dagen om te reizen
  op het water. Kost extra om Dribbel en Fee mee te nemen.

# Riviertraan

- Wilgspruit is stabiel in het water. Hij wordt niet meer slechter, maar ook
  niet echt meer beter. De heler heeft z'n best gedaan en is weer vertrokken
  richting Varna.

  + Mumzok kan Wilgspruit misschien helen. Hij heeft een paar Eberronscherven
    met zich mee, maar komt er snel achter dat de Khyberscherven die gebruikt
    waren om Wilgspruit ziek te maken magisch bewerkt waren. Hij stelt dat het
    mogelijk zou kunnen zijn om de Eberronscherven magisch te bewerken om
    Wilgspruit te helen.

  + Het zou mogelijk kunnen zijn om de relevante spreuk uit te voeren in
    Arcanix.
