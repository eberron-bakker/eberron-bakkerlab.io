---
title: "Template"
date: 2020-10-05T23:34:43+02:00
draft: false
---

**Attributes:** Agility d6, Smarts d6, Spirit d6, Strength d6, Vigor d6\
**Skills:** Athletics d4, Common Knowledge d4, Fighting d4, Notice d4,
Persuasion d4, Shooting d4, Stealth d4\
**Pace:** 6; **Parry:** 4; **Toughness:** 5\
**Edges:** ---\
**Gear:** ---\
**Special Abilities:**

- TODO