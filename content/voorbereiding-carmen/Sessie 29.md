---
title: "Sessie 27"
date: 2020-10-11T14:17:44+02:00
draft: false
---

# Bestiary

## Adelaar

**Attributes:** Agility d8, Smarts d4 (A), Spirit d6, Strength d4-2, Vigor d6\
**Skills:** Athletics d8, Fighting d6, Notice d10,
Stealth d8\
**Pace:** 3; **Parry:** 5; **Toughness:** 2\
**Special Abilities:**

- **Bite/Claws:** Str+d4.
- **Flight:** Raptors fly at a Pace of 48".
- **Size -3 (Very Small)**

## IJzeren golem

**Attributes:** Agility d6, Smarts d4, Spirit d6, Strength d12+3, Vigor d12\
**Skills:** Athletics d8, Fighting d8, Notice d8, Stealth d4\
**Pace:** 4; **Parry:** 6; **Toughness:** 17 (6)\
**Edges:** Alertness\ 
**Gear:** Sword (Str+d8)\ 
**Special Abilities:**

- **Armor +6:** Iron defines this creature.
- **Slam:** Str+d6.
- **Construct:** +2 to recover from being Shaken; ignores 1 point of Wound
  penalties; does not breathe or suffer from disease or poison.
- **Immunity:** Immune to fire, psychic, and non-magical physical attacks.
- **Size +3**
- **Very Resilient:** Iron golems can take two Wounds before they're
  Incapacitated.
