---
title: "Huisregels - Droaam"
date: 2019-10-27T15:56:59+01:00
draft: false
---

# Healer's Kit Dependency (DMG p266)

A character can't spend any Hit Dice after finishing a short rest until someone
expends one use of a healer's kit to bandage and treat the character's wounds.

# Slow Natural Healing (DMG p267)

Characters don't regain hit points at the end of a long rest. Instead, a
character can spend Hit Dice to heal at the end of a long rest, just as with a
short rest.

This optional rule prolongs the amount of time that characters need to recover
from their wounds without the benefits of magical healing and works well for
grittier, more realistic campaigns.
