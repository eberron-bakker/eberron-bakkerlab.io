---
title: "Varna"
date: 2020-02-29T13:12:11Z
draft: false
tags:
- locatie
---

{{< figure src="" >}}

**Type:** Stad\
**Provincie:** Eldeen\
**Populatie:** 8.000\
**Demografie:** Een mix van mensen, khoravar, gnomen, shifters, en
dubbelgangers\
**Regering:** Aristocratie

Varna is het centrum van de handel in Eldeen. [Huis
Vadalis](/organisatie/huis-vadalis) heeft haar hoofdkwartier hier. Men grapt dat
de stallen hier mooier zijn dan de huizen.

## Regering

## Infrastructuur

## Locaties

### Jachtersgaard

De Jachtersgaard is een gigantische tempel aan Balinor, een van de Negen
Soevereinen. De tempel is vol met tamme dieren en mooie planten. Zelfs de wilde
dieren in de tempel zijn tam.

Sinds 998-02-12 is de pegasus van de tempel weg. De pegasus bevond zich altijd
rond het standbeeld van Balinor. In plaats van de pegasus vond men een
hippocampus in de tempelwateren die dag.

<!-- de wilde dieren in de tempel zijn tam, met dank aan Huis Vadalis. -->

## Handel en industrie

## Gilden en facties

## Geschiedenis

## Geografie
