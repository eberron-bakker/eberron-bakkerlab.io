---
title: "Thelanis"
date: 2020-03-07T15:36:47Z
draft: false
tags:
- locatie
---

{{< figure src="" >}}

**Type:** -\
**Provincie:** -\
**Populatie:** -\
**Demografie:** Feeën\
**Regering:** Feeënhof

TODO: Introductie

## Regering

## Infrastructuur

## Locaties

### Hegbos

Hegbos is in het centrum van het domein van de [Hofnar](/karakter/hofnar). Het
is een doolhof van heggen. In het centrum van het doolhof is het Gelukspaleis.

## Handel en industrie

## Gilden en facties

## Geschiedenis

## Geografie
