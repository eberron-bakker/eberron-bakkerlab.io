---
title: "Rivierwoud"
date: 2019-10-27T01:54:32+02:00
draft: false
tags:
- locatie
---

**Type:** Bos\
**Provincie:** Eldeen\
**Populatie:** -\
**Demografie:** Bosinwoners\
**Regering:** Wachters van het Woud

Het Rivierwoud is een groot bos langs de rivier Wynarn. Het bos is gelegen op
een heuvel, en veel kleine rivierbeekjes stromen langs de helling naar beneden
richting de Wynarn. In het woud bevinden zich verschillende gemeenschappen en
interessepunten.

## Regering

## Infrastructuur

## Locaties

### Druïdische gaard

Diep in het woud ligt een druïdische gaard; een cirkel van bomen waardoor men de
hemel kan zien. De gaard wordt gebruikt voor druïdische rituelen en ontmoetingen
voor verscheidene gemeenschappen in de omgeving.

Rondom de gaard loopt een beekje van 10 tot 15 voet breed. De gaard heeft een
diameter van ongeveer 90 voet.

[Pririla](/karakter/pririla) is de meest permanente verzorger van de gaard.


<!--
De dieren van het woud verdedigen de gaard. Degenen die kunnen demonstreren dat
ze druïdische gaven hebben mogen blijven. Gasten mogen ook mee. Anderen worden
geweerd. Niet per se gewelddadig. Zie XGE p25 en XGE p97 voor een lijst van
boswezens.

De gaard is onder effect van *druid grove* (XGE). Er is een dikke mist van 10
voet hoog. De grond heeft grijpend onkruid en doornstruikgewas. Vier bomen in de
gaard ontwaken als iemand toetreedt zonder toestemming.

In de boomtoppen kan men slaapplekken vinden. Bij een van de bomen leidt een
soort natuurlijk trapje omhoog rond de boom om bij de slaapplekken te komen. Er
liggen warme vachten waar de druïdes tussen slapen.

Er is een moestuintje waar wortelpenen in groeien.
-->

<!-- ### Kamp Represaille

Kamp Represaille is een verlaten kamp dat tijdens de Laatste Oorlog bewoond werd
door strijdgesmedene deserteurs uit het leger van Aundair. Deze strijdgesmedenen verzetten
zich tegen het misbruik van hun soort. Ze deden represaille-aanvallen tegen
Aundairse legereenheden, en probeerden om meer strijdgesmedenen uit het leger van
Aundair te lokken met propaganda.

Het kamp is goed verborgen in een natuurlijke grot. Er liggen nog steeds een
paar lijken van strijdgesmedenen. -->

### De Zilveren Heuvel

De Zilveren Heuvel is de grootste heuvel in het Rivierwoud. Het komt boven de
boomtoppen uit, en men heeft een goed zicht over de rest van het bos.

<!-- Er zit zilver in de grond onder de Zilveren Heuvel, maar niemand heeft de heuvel
kunnen exploiteren: De bosgemeenschappen staan het niet toe. -->

## Handel en industrie

## Gilden en facties

### Stam van de schemermaan

De [Stam van de Schemermaan](/organisatie/stam-van-de-schemermaan) is een stam
van nomadische shifters in het Rivierwoud.

### Eldeen-wolven

De [Eldeen-wolven](/organisatie/eldeen-wolven) zijn soms in het Rivierwoud.

## Geschiedenis

## Geografie
