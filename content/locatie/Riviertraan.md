---
title: "Riviertraan"
date: 2019-10-27T01:07:43+02:00
draft: false
tags:
- locatie
---

![](/img/Riviertraan.jpg)

**Type:** Dorp\
**Provincie:** Eldeen en Aundair\
**Populatie:** 1.000\
**Demografie:** Een mix van mensen, khoravar, gnomen, shifters, en
dubbelgangers\
**Regering:** Dorpsraad

Riviertraan is een agrarisch dorp aan de rivier Wynarn, op de grens tussen
Eldeen en Aundair. Het grenst aan het [Rivierwoud](/locatie/rivierwoud). Eén
helft van het dorp ligt aan de westerzijde van de rivier, en de andere helft aan
de oosterzijde. Een brug verbindt de twee helften, maar de brug is al vele jaren
kapot.

## Regering

De westelijke helft van het dorp wordt geleid door een raad van invloedrijke
inwoners:

- [Eenpoot](/karakter/eenpoot)
- [Fhazira](/karakter/fhazira)
- [Cédric d'Vadalis](/karakter/cédric)
- Een dubbelganger van [Het Gebroken Glas](#het-gebroken-glas)
- Een paar boeren

Voormalig waren oosterlingen ook onderdeel van de dorpsraad.

De oostelijke helft is eigendom van Barones [Urielor](/karakter/urielor).
Urielor woont niet in Riviertraan, en is bijna nooit aanwezig.
[Pinoki](/karakter/pinoki) regelt de zaken in de oostelijke helft in Urielors
afwezigheid.

<!-- Pinoki wordt soms verstoord door [Folduin](/karakter/folduin). -->

## Infrastructuur

### Brug

De stenen brug die de twee helfden van het dorp verbindt is iets minder dan 100
meter lang. De brug werd lang gebruikt om handel tussen Eldeen en Aundair
makkelijker te maken; voornamelijk het transporteren van voedsel naar het
oosten. Deze brug zorgde voor een kortere route richting Schoonhaven in plaats
van het reizen via Varna.

De middelste twintig meter van de brug zijn verwijderd of opgeblazen door
verzetstrijders uit Eldeen. Dit maakte het moeilijker, maar niet onmogelijk, om
troepen te verplaatsen tussen de twee provincies.

Hedendaags worden roeiboten gebruikt om tussen de twee dorphelften te reizen. Er
is echter een probleem: Wachters aan de oosterzijde van de rivier verwachten een
paspoort en/of reispapieren voor toelating. Inwoners van Eldeen gebruiken
doorgaans geen identificatiedocumenten, en het is daarom moeilijk voor hen om
het oosten te bezoeken.

### Wachttoren

Aundair blijft militair aanwezig in Riviertraan. In de wachttoren naast de brug
zijn een aantal soldaten <!-- *soldier* --> gestationeerd. De soldaten zijn
hedendaags voornamelijk grenswachters.

[Folduin](/karakter/folduin) is de commandant van de soldaten. <!-- Hij heeft een
hekel aan druïdes, en zal ze slecht behandelen. -->

De grenswachters laten reizigers toe tot de oostelijke helft van het dorp als:

- ze in de oostelijke helft van het dorp wonen;
- ze identiteitspapieren van Aundair hebben;
- ze identiteitspapieren van Eldeen hebben die zeggen dat ze in Riviertraan
  wonen;
- ze reispapieren hebben om naar Aundair of Riviertraan te reizen;
<!-- - ze bereid zijn om 1 gp per dag te betalen als omkoping. -->

<!-- De soldaten komen nooit in het westen, zelfs niet als er een noodsituatie is. -->

## Locaties

### Dierentuin

Huis Vadalis is aanwezig op een boerderij in de westerhelft van Riviertraan. Ze
onderhouden een dierenfokkerij die een "dierentuin" genoemd worden door de rest
van de inwoners.

[Cédric d'Vadalis](/karakter/cédric) en zijn man,
[Tarlec](/karakter/tarlec), beheren de dierenfokkerij samen.

De fokkerij heeft de volgende dieren:

- Paarden
- Ponies
- Ezels
- Koeien
- Biggen
- Kippen
- Herdershonden
- Knipperhond (*blink dog*)
- Postduiven
<!-- - Een paar velociraptors, recentelijk uit Varna -->

### De Eerlijke Nar

De Eerlijke Nar is een algemene winkel in de oosterzijde van Riviertraan. Een
gnoom, [Pinoki](/karakter/pinoki), is de eigenaar van de winkel.

Omdat westerlingen soms moeite hebben met het kopen van waren uit het oosten,
gaat Pinoki twee keer per week naar de overkant van de rivier, waar hij wat
veelgekochte waren meeneemt en orders aanneemt. De dagen zijn *Wir* en *Sul*.

### Het Gebroken Glas

Het Gebroken Glas is een herberg aan de weg die langs de westerkant van de
rivier loopt. Een gemeenschap van dubbelgangers werken in deze herberg, en het
thema van de herberg verandert elke week.

<!--
- Dwergthema: Kleine tafels en stoelen van steen en hout, open haard, veel bier,
  soep, brood, champignons, taart.

- Cyrethema: Haute couture, vloeiende kleren, fijne handschoenen, juwelen,
  magische lantaarns, een *cleansing stone*, schilderijen, poëzie, Cyrische
  *tago*-dans, wijn, specerijen.

- Lhazaarthema: Houten interieur dat lijkt op de binnenkant van een schip,
  shanty's, rum, gokken, ooglapjes en houten poten, een houten buste van Lucia,
  een aapje en een papagaai.

- Eldeenthema: Houten interieur, veel planten, veel kleine bosdieren, fey, een
  overvloed van groente, vlees, melkproducten.

- Khyberthema: Stenen interieur, tieflings, horror, vuur, enkel vlees.

- Winterthema: Stoelen en tafels van magisch ijs, sneeuwvlokjes, sneeuwpoppen,
  warme oranje kaarsen, koorgezang, wortelsoep, koekjes, wintersalade,
  winter-eladrin.
-->

<!-- De dubbelgangergemeenschap bestaat uit ongeveer een dozijn dubbelgangers. -->

### Houtbewerker

[Thubar](/karakter/thubar) is de houtbewerker van Riviertraan.

### Tranentempel

De Tranentempel is geweid aan de zeemeermin [Lucia](/karakter/lucia), en aan de
Negen Soevereinen, vooral Arawai. De tempel heeft tien zijden: Eén voordeur aan
de voorkant, en negen ramen op de overige zijden. Negen treden reizen tot een
platform achter in de tempel, waarop een podium en een altaar staan. Naast het
altaar staat een kristallen standbeeld van de zeemeermin op een voetstuk. 

De priester is een jonge khoravar genaamd [Fhazira](/karakter/fhazira). Vroeger
waren er meer priesters, maar ze onderhoudt de tempel nu zelf.

De Negen Soevereinen houden zich officieel niet bezig met nationale overheden,
dus de Tranentempel was officieel neutraal tijdens de bezetting.

<!--
Praktisch was de tempel aan de zijde van Aundair, omdat Eldeen niet zo veel
gelovigen van de Negen Soevereinen kent. De tempel misbruikte haar positie als
neutrale autoriteit om informatie door te sluisen aan Aundair.
 -->

Slechts een kwart van het dorp komt soms naar de tempel.

## Handel en industrie

## Gilden en facties

### Wachters van het Woud

[Eenpoot](/karakter/eenpoot) is de druïde van het dorp.

Meer dan de helft van de inwoners van het dorp volgt de druïdische religie.

## Geschiedenis

Riviertraan dankt haar naam aan een folklore. Volgens legende woonde er ooit een
zeemeerminnenstam in Meer Galifar; sommigen zeggen dat die stam er nog steeds
woont. Eén lid van de stam, de prachtige [Lucia](/karakter/lucia), was verliefd
op een kapitein die veel goederen verscheepte tussen de kuststeden van het meer.
Ze zwom altijd met het schip mee, om altijd dicht bij de man te zijn die ze van
een afstandje liefhield. Tijdens één reis was het weer uitermate slecht, en
dreigde het schip te zinken. Om dit lot te voorkomen, gebruikte Lucia haar magie
om het regenwater te absorberen en het schip veilig te houden. En hoewel het
schip veilig door kon varen, had zij de natuur ontregeld, en werd daardoor
verbannen door de stam. Met de kennis dat zij haar geliefde kapitein nooit meer
zou zien, barstte ze in huilen uit. Al het regenwater dat ze geabsorbeerd had
werd uitgehuild, en creëerde de rivier Wynarn, waarlangs ze vertrok richting het
noorden. Halverwege de rivier stopte ze, uitgeput, en vormde ze zich tot een
kristal. Toen mensen later op die plek gingen bouwen, vonden vissers de
gekristalliseerde zeemeermin, en is er een tempel in haar eer gebouwd.

Tijdens de Laatste Oorlog lag Riviertraan aan de frontlinie tussen Eldeen en
Aundair. De legers van Aundair gebruikten Riviertraan als verzamelplaats voor
militaire operaties, mede mogelijk gemaakt door de stenen brug die over de
rivier gaat. Voor het grootste deel van de oorlog is Riviertraan bezet geweest
door Aundair, hoewel de Wachters van het Woud af en toe een tegenoffensief
gelanceerd hebben. Als betuig van verzet hebben de inwoners van Riviertraan de
brug kapot gemaakt, om het moeilijker te maken voor de troepen van Aundair om
de rivier over te steken. Als resultaat werd het veel moeilijker voor families
aan beide kanten van de rivier om elkaar te zien.

Na het Verdrag van Troonburcht vertrokken de troepen van Aundair uit de
westelijke helft van Riviertraan. Het oosten is echter in de handen gebleven
van Aundair, omdat de grens tussen Eldeen en Aundair langs de rivier loopt.
Niemand is blij met deze oplossing, maar ten minste is de bezetting afgelopen en
de oorlog gestopt. De brug wordt vooralsnog niet gerepareerd; het is te duur
voor de inwoners, en noch Eldeen noch Aundair wil ervoor betalen.

## Geografie
