---
title: "Journaal - Droaam"
date: 2019-10-27T15:56:59+01:00
draft: false
toc: true
---

# 998-05-07

## Sessie 1 - 2020-07-26

- Tedrialis vierde haar vierjarig bestaan.

- Puwakloo leidde de centrale ceremonie. Hij kondigde een viswedstrijd aan:
  Degene met de beste vangst mag het hoofdoffer doen bij de toedrage aan Tedria.

- Puwakloo vroeg om persoonlijke toevoegingen aan het hoofdoffer.

- To-Yi leende zijn zeilboot uit aan het gezelschap.

- Het gezelschap ging het meer op om een vis te vangen.

- Het gezelschap kwam een haai tegen.

- Fluister werd van de boot getrokken door de haai.

- Fluister ging bijna dood, maar vermoordde de haai met schaduwkrachten.

- Het gezelschap voer terug naar Tedrialis.

- De haai die gevangen was door het gezelschap was effectief de grootste vangst.

- Een ceremonie met offers aan Tedria begon.

  + Asev gaf een eeuwig brandende lantaarn. Dit symboliseert de ontrafeling van
    nieuwe mysteries.

  + To Yi gaf een sieradenkistje voor de bescherming van ieders waarde.

  + Een etin gaf een pijp voor het altaar voor de vrije geest. "Luister niet
    naar de man; fuck Breland".

  + Fluister gaf een boek over de Duistere Zes, voor Tedrias toetreding tot dit
    pantheon.

  + Beau gaf een stukje stof van de kleding van zijn zus Neami, zodat de dader
    gevonden zal worden.

  + Lyvea gaf een grote veer uit haar vleugel als symbool voor de vrijheid.

  + Een oger (Garok) gaf een perkamentrol voor het delen van kennis en wijsheid.

  + Een kobold (Ketyr) gaf een mand edelstenen. "Glimsteen voor meer glimsteen."

- De modrons brachten de giftes naar Tedria, en de giftes "verspreidden" zich
  door Tedria. Twee van de kristallen die gegeven werden door de kobold
  "absorbeerden" gedeeltes van Tedrias bepantsering, en werden modrons.
  
  + Eén van de modrons zwom weg als een soort zeester.

  + De ander werd een vierkanten doos met vleugels. Het liet een krijs uit, en
    alle andere modrons stopten met werken.

## Sessie 2 - 2020-07-31

- De modrons vielen de dorpelingen aan.

- De modrons, en de vierkante modron, werden verslagen door het gezelschap.

- Een zeester-achtige modron zwom---aan het eind van het gevecht---weg over het
  Zilvermeer.

- De overige modrons werden weer rustig.

## Sessie 3 - 2020-09-27

- Het gezelschap kwam bij in een gebouw.

- Beau legde uit aan Puwakloo dat de kristallen van de kobold geabsorbeerd
  werden in Tedria.

- Lyvea leidde de kobold naar buiten met *friends*, en ondervraagde de kobold
  (Ketyr) over de glimsteen. Ketyr had niet begrepen dat een offer *geven* is,
  in plaats van vragen om "meer glimsteen". Ketyr legde uit dat de glimstenen
  die problemen veroorzaakten van hem waren, maar leek niet te weten hoe of
  waarom. Hij had de stenen gekocht.

- Lyvea suggereerde om naar binnen te gaan. Ketyr wilde dat niet, en nam een
  dolk. Lyvea probeerde de dolk af te nemen, maar dat lukte niet.

- Ketyr rende weg.

- Lyvea vertelde de oger (Garok) wat ze deed met de kobold. Garrok was hier niet
  blij mee, en liep weg.

- Lyvea vloog richting Tedria om te zoeken naar kristallen. Ze vond enkele
  kristallen in de bepantsering van Tedria. Ze wrikte een kristal los, en de
  bepantsering van Tedria heelde zichzelf.

- Ionys porde in de vierkante modron om te zoeken naar kristallen. In het
  "torso" van de modron vond ze een donkerblauw kristal die uitstak.

- De kristallen zijn "ruw".

- Het kristal in de torso van de modron lijkt niet op de kristal die Lyvea vond.
  Lyvea's kristal lijkt meer op een smaragd.

- Lyvea haalde het donkerblauwe kristal uit de modron. De modron heelde zichzelf
  niet.

- Het gezelschap bracht de kristallen naar Puwakloo. Puwakloo wist niet wat de
  kristallen waren, maar vond ze een "rare gloed" hebben. Puwakloo suggereerde
  om Asev te vragen om een parel.

- Asev wilde niet afstand doen van een parel. Hij suggereerde om een parel te
  zoek in het Zilvermeer.

- Het gezelschap ging naar To Yi (visser) voor hulp met het vissen naar een
  parel.

- Het gezelschap ging het Zilvermeer op. To Yi en Beau sprongen het water in om
  mosselen te plukken. Ionys en Lyvea hielden de zwemmers aan een touwtje.

- To Yi en Beau plukten 35 mosselen.

- Vijf vis-zeehond-wezens komen naar de boot, en dreigen met speren. "Wat doen
  jullie hier?" en "jullie zijn in het territorium van de Conflux". De Conflux
  eist het Zilveren Meer op sinds recentelijk.

- De Conflux eiste belasting. 10 goudstukken én het verspreiden van het nieuws
  dat de Conflux de nieuwste provincie van Droaam is/wil worden.

- "We nemen contact", zeiden de wezens van de Conflux, en zwommen weg.

- Het gezelschap keerde terug naar Tedrialis, en vond een enkele parel in één
  van de mosselen.

- Het gezelschap gaf de parel aan Puwakloo, en vertelde het verhaal. Puwakloo
  zei dat de wezens waarschijnlijk "locathah" waren. Hij kende de Conflux niet.
  Hij vond het vreemd dat de locathah territoriaal gedrag vertoonden.

- Puwakloo vertelde dat de diplomaten van de heksen Garok en Ketyr zijn.

- Puwakloo voerde met de parel een spreuk uit om de kristallen te inspecteren.
  Het donkerblauwe kristal is een bewerkte khyberscherf. Het groene kristal is
  niet bijzonder. Hij opperde dat iemand misschien controle wilde uitoefenen
  over Tedria. Puwakloo denkt niet dat de heksen hiervoor verantwoord zijn.

- Garrok is een lid van Maenyas vuist, een elite-eenheid handhavers van Sora
  Maenya. Ketyr is lid van Katras Stem, de diplomaten van Sora Katra.

- Garrok en Ketyr werden als niet-volledig-te-vertrouwen geacht.

- Plan: Volgende ochtend met To Yi over het Zilvermeer zeilen om te zoeken naar
  de vermiste modron.

- Tijdens het avondeten gaf Asev 10 goudstukken terug aan Lyvea.

# 998-05-08

- Bezek de spuier (postbode-soldaat) kwam langs tijdens het ontbijt. Hij zei dat
  er tijdens de nacht meer modrons wegtrokken over het water.

- Het gezelschap ging het water op met To Yi.

- Lyvea haalde een kaart van het Zilvermeer op bij Asev.

## Sessie 4 - 2020-10-04

- Het gezelschap vaarde voor een paar uur richting het oosten.

- Beau zag slanke schimmen op de grens van zijn zicht in het water.

- De schimmen beweegden op het ritme van Beau's "trommelen" op het water.

- Beau gooide een gedroogd koekje het water in.

- Eén van de schimmen zwom naar boven---een zeemeermin. Ze legde haar handen op
  de rand van de boot en bracht het koekje terug naar Beau.

- De zeemeermin sprak gebrekkig Common---iets over een vraag tussen de relatie
  tussen het trommelen en slechte wezens.

- De zeemeermin wilde het gezelschap brengen naar de laatste plek waar ze de
  modron gezien hadden.

- De zeemerminnen bleken het trommelen als prettig te ervaren.

- "Groep Koalinth (water-goblins) kwam naar ons met offer voor vrede en
  samenkomst in Zilvermeer." ... "[De water0goblins wilden] toetreding and
  zwering aan de Conflux." De zeemeerminnen hebben dit gedaan.

- Merrow zouden niet meer aanvallen bij toetreding tot de Conflux. Aanvallen
  zijn gestopt.

- Merrow zijn als zeemeerminnen, maar wild. Jagers.

- Het gezelschap volgde de zeemeerminnen richting het oosten. Hun koers ging
  langs de kust van Breland.

- Het gezelschap ging ankeren rusten.

- De zeemeerminnen kwamen aan boord om mee te rusten. Ze konden hun staart
  veranderen naar benen.

- In de nacht kwamen vier figuren onverwachts het schip op. Het waren
  "zeemeerminnen" met grote kaken en haaiachtige tanden (merrow).

- Lyvea schoot een waarschuwingsschot. Een merrow stak Ionys.

- Er brak een gevecht uit. De merrows stierven.

- Verklaring van zeemeermin: "Merrow patrouilleren vaarroutes. Ze vielen aan
  omdat de zeemeerminnen aan boord waren." Misschien dachten de merrow dat de
  zeemeerminnen gevangen genomen waren? De zeemeerminnen probeerden het
  gevecht te stoppen, maar het gevecht begon te vroeg.

- Zeemeermin: "De merrow richten zich tegenwoordig op landbewoners."

- Het gezelschap liet de lijken van de merrow op het schip liggen.

## Sessie 4 - 2020-MM-DD

# 998-05-09
