---
title: "Lucia"
date: 2019-10-27T01:52:35+02:00
draft: false
tags:
- karakter
---

**Ras:** Zeemeermin\
**Gender:** Vrouw\
**Leeftijd:** -\
**Associatie:** -

<!-- stat block: TODO -->

Lucia is een legendarische zeemeermin. Haar legende is
[hier](/locatie/riviertraan#geschiedenis) beschreven.

## Beschrijving

## Biografie

## Relaties

## Trivia
