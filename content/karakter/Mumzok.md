---
title: "Mumzok"
date: 2020-04-26T13:13:44+01:00
draft: false
tags:
- karakter
---

{{< figure src="/img/Mumzok.jpg" width="250px" >}}

**Ras:** Ork\
**Gender:** Man\
**Leeftijd:** -\
**Associatie:** Poortwachters

<!--
**Attributes:** Agility d6, Smarts d6, Spirit d10, Strength d8, Vigor d8\
**Skills:** Athletics d6, Common Knowledge d6, Druidism d8, Fighting d6,
Intimidation d8, Notice d6, Persuasion d6, Shooting d6, Stealth d6\
**Pace:** 6; **Parry:** 6 (1); **Toughness:** 8 (1)\
**Edges:** ---\
**Gear:** Staff (Str+d4, Parry +1, Reach 1, two hands; Range 12/24/48, Damage
2d6), leather armor (+1)\
**Special Abilities:**

- **Size 1**
- **Low Light Vision:** Ignore penalties for Dim and Dark Illumination.
- **Spellcasting:** 20 Power Points, can cast *detect/conceal arcana*,
  *entangle*, *havoc*, *healing*, and *shape change*.
-->

TODO: Introductie

## Beschrijving

## Biografie

## Relaties

## Trivia
