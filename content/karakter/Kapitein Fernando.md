---
title: "Kapitein Fernando"
date: 2020-01-05T17:59:21+01:00
draft: false
tags:
- karakter
---

{{< figure src="/img/Fernando.jpg" width="300px" >}}

**Ras:** Mens\
**Gender:** Man\
**Leeftijd:** -\
**Associatie:** [Wolkenwrekers](/organisatie/wolkenwrekers)

<!-- stat block: TODO -->

Kapitein Fernando is de kapitein van het Vliegende Fortuin.

## Beschrijving

## Biografie

## Relaties

## Trivia
