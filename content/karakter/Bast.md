---
title: "Bast"
date: 2020-03-07T15:12:08Z
draft: false
tags:
- karakter
---

{{< figure src="" width="200px" >}}

**Ras:** Strijdgesmedene\
**Gender:** -\
**Leeftijd:** -\
**Associatie:** [Wachters van het Woud](/organisatie/wachters-van-het-woud)

<!-- stat block: TODO -->

TODO: Introductie

<!--
Served in Aundair military.
Fought in the Eldeen Reaches.
During the campaign, one of the warforged started plotting a desertion.
The warforged deserted the Aundair army, and brought other radicalised warforged with him.
Deep inside of Eldeen, the warforged set up camp to plan terrorist attacks against Aundair.
On one mission away from the camp (finding resources or whatever, TODO), the character returned to a trashed and abandoned camp, with some dead warforged.
Following this incident, the character set out alone, to live a self-sufficient life.
During his stay in the Riverwood, he became somewhat of a "rumour", of a "thing" living in the forest.
One day, a treant came across the warforged, who at this point looked much more tree-like than metallic-like. The treant thought the warforged a lost sapling, and took him on a journey to Greenheart.
In Greenheart, the warforged was presented to the Great Druid Oalian.
Oalian was amused by the warforged, and wished to induct him into the Wardens of the Wood.
After initiation, the warforged was sent to train.
-->

## Beschrijving

## Biografie

## Relaties

## Trivia
