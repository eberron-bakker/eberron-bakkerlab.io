---
title: "Diana ir'Wynarn"
date: 2020-08-07T14:13:46+02:00
draft: false
tags:
- karakter
---

{{< figure src="/img/Diana.jpg" width="350px" >}}

**Ras:** Mens\
**Gender:** Vrouw\
**Leeftijd:** -\
**Associatie:** Arcanix, Aundair

<!-- stat block: TODO -->

Diana is de dochter van [Minister en Eerste Generaal Adal](/karakter/adal).

Ze is student van alchemie in Arcanix. 

Diana is al twee jaar goede vrienden met [Paulus](/karakter/paulus).

## Beschrijving

## Biografie

## Relaties

## Trivia
