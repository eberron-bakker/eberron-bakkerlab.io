---
title: "Tom"
date: 2019-10-27T12:45:20+01:00
draft: false
tags:
- karakter
---

{{< figure src="" width="200px" >}}

**Ras:** Khoravar\
**Gender:** Man\
**Leeftijd:** -\
**Associatie:** -

<!-- stat block: commoner -->

Mede-eigenaar van boomkwekerij in [Riviertraan](/locatie/riviertraan)

## Beschrijving

Fors en sterk, met rood wild haar en een stoppelbaardje.

## Biografie

## Relaties

Man van [Mona](/karakter/mona).

Vader van [Donna](/karakter/lin).

## Trivia

- Harde werker. Helpt iedereen die zijn hulp nodig heeft in raad en daad.
