---
title: "Pririla"
date: 2019-10-31T15:15:27+01:00
draft: false
tags:
- karakter
---

{{< figure src="/img/Pririla.png" width="300px" >}}

**Ras:** Khoravar\
**Gender:** Vrouw\
**Leeftijd:** 26\
**Associatie:** [Wachters van het Woud](/organisatie/wachters-van-het-woud)

<!-- stat block: druid -->

Pririla is een druïde die veel in de buurt blijft van [de gaard in het
Rivierwoud](/locatie/rivierwoud#druïdische-gaard).

## Beschrijving

Pririla heeft blauwe gezichtstatoeage over de bovenhelft van haar gezicht. Ze
draagt haar haar in vele lange vlechten. Haar gezicht heeft fee-achtige trekjes.
Als kleding draagt ze groengekleurd leer en vacht, en heeft altijd een houten
toverstokje in haar riem gestoken. Ze draagt geen schoenen.

## Biografie

## Relaties

## Trivia

<!-- Pririla is meestal in dierenvorm. Ze gedraagt zich in khoravar-form
tamelijk fee-achtig. -->
