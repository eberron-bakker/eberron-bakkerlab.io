---
title: "Kardinaal Taya"
date: 2020-10-16T03:12:49+02:00
draft: false
tags:
- karakter
---

{{< figure src="/img/Taya.png" width="350px" >}}

**Ras:** Mens\
**Gender:** Vrouw\
**Leeftijd:** -\
**Associatie:** Thrane, Kerk van de Zilveren Vlam

<!--
**Attributes:** Agility d6, Smarts d6, Spirit d10, Strength d6, Vigor d6\
**Skills:** Academics d8, Athletics d4, Common Knowledge d8, Cosmology d8,
Faith d8, Fighting d6, Healing d4, Notice d6, Persuasion d8, Stealth d4\
**Pace:** 6; **Parry:** 5; **Toughness:** 5\
**Edges:** ---\
**Special Abilities:**

- **Spells:** Taya has 20 Power Points and knows the following powers:
  *Healing*, *relief*, *smite*
-->

TODO: Introductie

## Beschrijving

## Biografie

## Relaties

## Trivia
