---
title: "Lifra"
date: 2019-10-27T02:10:10+02:00
draft: false
tags:
- karakter
---

{{< figure src="/img/Lifra.jpg" width="200px" >}}

**Ras:** Mens\
**Gender:** Vrouw\
**Leeftijd:** 30\
**Associatie:** [Eldeen-wolven](/organisatie/eldeen-wolven)

<!--
**Attributes:** Agility d8, Smarts d6, Spirit d6, Strength d10, Vigor d10\
**Skills:** Athletics d8, Common Knowledge d6, Fighting d12,
Intimidation d10, Notice d6, Persuasion d8, Riding d12, Shooting d8,
Stealth d6\
**Pace:** 6; **Parry:** 8; **Toughness:** 9 (1)\
**Edges:** ---\
**Gear:** Great axe (Str+d10), leather armor (+1)\
**Special Abilities:**

- **Size 1**
- **Sweep:** Fighting roll at -2 to hit all targets in weapon's Reach, no more
  than once per turn.
-->

Lifra is de leider van de Eldeen-wolven.

## Beschrijving

Lifra is een sterke vrouw met een boze blik. Ze draagt een lendendoek van
berenvacht, en gebruikt modder als lichaamsverf over haar torso, armen, en
gezicht.

## Biografie

## Relaties

## Trivia
