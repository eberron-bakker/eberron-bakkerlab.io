---
title: "Heer Darro ir'Lain"
date: 2020-06-27T18:04:55+02:00
draft: false
tags:
- karakter
---

{{< figure src="/img/Darro.jpg" width="200px" >}}

**Ras:** Mens\
**Gender:** Man\
**Leeftijd:** -\
**Associatie:** Aundair, [Ridders Arcane](/organisatie/ridders-arcane)

<!--
**Attributes:** Agility d8, Smarts d10, Spirit d10, Strength d8, Vigor d8\
**Skills:** Academics d6, Athletics d6, Common Knowledge d6, Fighting d10,
Intimidation d6, Notice d6, Persuasion d8, Riding d10, Shooting d6,
Spellcasting d10, Stealth d4\
**Pace:** 6; **Parry:** 7; **Toughness:** 10 (4)\
**Edges:** Aristocrat, Soldier\
**Gear:** Wand (Range 6/12/24, Damage 2d6), arming sword (Str+d8), plate armor
(+4)\
**Special Abilities:**

- **Spellcasting:** Darro has 20 Power Points and knows the following powers:
  *Blast*, *bolt*, *burst*, *detect/conceal arcana*, *dispel*, *intangibility*,
  *protection*, *slumber*, *stun*.
-->

Heer Darro ir'Lain is de Tweede Generaal van Aundair en commandant van de
Ridders Arcane. Hij is lid van het Triumviraat van Aundair.

## Beschrijving

## Biografie

## Relaties

## Trivia
