---
title: "Tarlec"
date: 2019-10-27T01:37:31+02:00
draft: false
tags:
- karakter
---

{{< figure src="/img/Tarlec.png" width="250px" >}}

**Ras:** Mens\
**Gender:** Man\
**Leeftijd:** 37\
**Associatie:** -

<!-- stat block: commoner -->

TODO.

## Beschrijving

## Biografie

## Relaties

Tarlec is de man van [Cédric d'Vadalis](/karakter/cédric).

## Trivia
