---
title: "Paulus"
date: 2020-08-07T14:06:47+02:00
draft: false
tags:
- karakter
---

{{< figure src="/img/Paulus.png" width="350px" >}}

**Ras:** Mens\
**Gender:** Man\
**Leeftijd:** -\
**Associatie:** Arcanix

<!-- stat block: TODO -->

Paulus is een getalenteerde alchemist, maar zijn gebrek aan focus heeft ervoor
gezorgd dat hij achterloopt. Hij heeft twee jaar op rij zijn toelatingsexamen
gefaald.

Paulus is al twee jaar goede vrienden met [Diana](/karakter/diana).

<!--
Paulus is verliefd op Diana.

Paulus is angstig dat hij zijn toelatingsexamen opnieuw faalt voordat Diana
afstudeert.
-->

## Beschrijving

## Biografie

Paulus komt uit Riviertraan, en reisde aan het eind van de oorlog naar Arcanix
om gecertificeerde tovenaar te worden.

## Relaties

## Trivia
