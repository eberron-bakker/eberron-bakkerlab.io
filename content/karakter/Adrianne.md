---
title: "Adrianne"
date: 2019-10-27T01:50:27+02:00
draft: false
tags:
- karakter
---

{{< figure src="/img/Adrianne.png" width="200px" >}}

**Ras:** Mens\
**Gender:** Vrouw\
**Leeftijd:** 21\
**Associatie:** -

<!-- stat block: commoner -->

Adrianne is de boerenknecht op de [dierenfokkerij in
Riviertraan](/locatie/riviertraan#dierentuin).

## Beschrijving

## Biografie

## Relaties

<!-- Adrianne heeft een stiekeme relatie met Cédric d'Vadalis. -->

## Trivia
