---
title: "Wilgspruit"
date: 2019-10-28T10:46:04+01:00
draft: false
tags:
- karakter
---

{{< figure src="" width="200px" >}}

**Ras:** Ent\
**Gender:** -\
**Leeftijd:** -\
**Associatie:** -

<!-- stat block: treant -->

Wilgspruit is een nomadische ent uit het [Rivierwoud](/locatie/rivierwoud). Op 1
Zarantyr 998 kwam hij ziek aan in [Riviertraan](/locatie/riviertraan), waar hij
in de rivier ging zitten.

## Beschrijving

Wilgspruit is een ent van ongeveer drie verdiepingen hoog, met lange slierten
aan bladeren die naar beneden hangen.

Hij heeft de harde, grove bast van een oude, stervende boom. Hij heeft kleine
paarsblauwe dotjes in de bladen en ledematen.

Wilgspruit heeft een oud littekken.

## Biografie

<!-- Wilgspruit heeft Bast meegenomen naar Groenhart. -->

## Relaties

## Trivia
