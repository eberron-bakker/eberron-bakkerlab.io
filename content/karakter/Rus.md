---
title: "Rus"
date: 2019-10-27T12:51:03+01:00
draft: false
tags:
- karakter
---

{{< figure src="" width="200px" >}}

**Ras:** Dubbelganger\
**Gender:** Vrouw\
**Leeftijd:** -\
**Associatie:** -

<!-- stat block: commoner -->

Rus is de "moeder overste" van de dubbelgangergemeenschap in
[Riviertraan](/locatie/riviertraan). Ze is de de facto eigenaar van [Het
Gebroken Glas](/locatie/riviertraan#het-gebroken-glas).

## Beschrijving

## Biografie

## Relaties

## Trivia

- Rus is lief en heeft heel veel geduld vooral met kinderen---van elk ras.

- Regelt van alles en weet alles van iedereen. Ook wel bekent als "De
  Dorpskrant"

