---
title: "Koningin Aurala ir'Wynarn"
date: 2020-10-16T02:31:35+02:00
draft: false
tags:
- karakter
---

{{< figure src="/img/Aurala.jpg" width="350px" >}}

**Ras:** Mens\
**Gender:** Vrouw\
**Leeftijd:** -\
**Associatie:** Aundair

<!--
**Attributes:** Agility d6, Smarts d10, Spirit d12, Strength d6, Vigor d6\
**Skills:** Academics d12, Athletics d4, Common Knowledge d10, Fighting d4,
Intimidation d8, Notice d10, Persuasion d12, Riding d6, Shooting d4, Stealth d6,
Taunt d10\
**Pace:** 6; **Parry:** 4; **Toughness:** 8 (3)\
**Edges:** Alertness, Aristocrat, Attractive, Charismatic, Connections\
**Gear:** Amulet of protection (+3), glamerweave
-->

Aurala is de ambitieuze koningin van Aundair.

## Beschrijving

## Biografie

## Relaties

## Trivia
