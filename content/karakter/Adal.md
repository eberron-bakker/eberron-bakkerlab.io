---
title: "Minister Adal ir'Wynarn"
date: 2020-07-25T00:33:16+02:00
draft: false
tags:
- karakter
---

{{< figure src="/img/Adal.jpg" width="350px" >}}

**Ras:** Mens\
**Gender:** Man\
**Leeftijd:** -\
**Associatie:** Aundair, Magisch Congres

<!-- stat block: archmage -->

Adal ir'Wynarn is de Eerste Generaal van Aundair en koninklijke minister van
magie. Hij is lid van het Triumviraat van Aundair. Hij is tevens de broer van
Koningin Aurala.

<!--
Adal zoekt naar een apocalyptische spreuk waarmee hij Thaliost kan terugnemen.
Hij wil niks te maken hebben met Eldeen, en beschouwt oorlog met Eldeen als een
verspilling van bloed totdat de Vijf Naties eerst neer zijn.
-->


## Beschrijving

## Biografie

## Relaties

## Trivia
