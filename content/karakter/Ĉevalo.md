---
title: "Ĉevalo"
date: 2020-04-25T23:43:54+01:00
draft: false
tags:
- karakter
---

{{< figure src="/img/Ĉevalo.jpg" width="600px" >}}

**Ras:** Centaur\
**Gender:** Man\
**Leeftijd:** Oud\
**Associatie:** 

<!-- stat block: TODO -->

Ĉevalo is een stokoude, mogelijk blinde centaur uit het Torenhoge Woud.

<!-- Ĉevalo is een behendige druïde. -->

## Beschrijving

## Biografie

## Relaties

## Trivia
