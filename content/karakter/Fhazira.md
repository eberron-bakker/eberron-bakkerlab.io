---
title: "Fhazira"
date: 2019-10-27T01:44:26+02:00
draft: false
tags:
- karakter
---

{{< figure src="/img/Fhazira.jpg" width="200px" >}}

**Ras:** Khoravar\
**Gender:** Vrouw\
**Leeftijd:** 24\
**Associatie:** De Negen Soevereinen

<!-- stat block: acolyte -->

Fhazira is de priester van de Negen Soevereinen in de [Tranentempel in
Riviertraan](/locatie/riviertraan#tranentempel).

<!-- Fhazira is een informant voor de Koninklijke Ogen van Aundair. -->

## Beschrijving

## Biografie

## Relaties

## Trivia
