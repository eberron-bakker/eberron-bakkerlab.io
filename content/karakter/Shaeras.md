---
title: "Hoge Koningin Shaeras Vadallia"
date: 2020-10-16T03:03:26+02:00
draft: false
tags:
- karakter
---

{{< figure src="/img/Shaeras.jpg" width="350px" >}}

**Ras:** Elf\
**Gender:** Vrouw\
**Leeftijd:** -\
**Associatie:** Valenar

<!--
**Attributes:** Agility d12, Smarts d6, Spirit d12, Strength d10, Vigor d8\
**Skills:** Athletics d12, Battle d12, Common Knowledge d6, Fighting d12,
Intimidation d8, Notice d8, Persuasion d6, Riding d10, Shooting d10, Stealth d8,
Taunt d8\
**Pace:** 6; **Parry:** 10; **Toughness:** 10 (4)\
**Edges:** Ambidextrous, Aristocrat, Combat Reflexes, Frenzy (Imp), Killer
Instinct, Nerves of Steel (Imp), Soldier, Trademark Weapon (Imp), Two-Fisted\
**Gear:** Dual scimitars (Str+d8+1), plate armor (+4)\
**Special Abilities:**

- **Fey Ancestry:** Elves receive a +2 bonus to resist magical effects that
  alter their mood or feelings, and magic cannot put them to sleep.
- **Low Light Vision:** Elves ignore penalties for Dim and Dark
  Illumination.
- **Trance:** Elves don't need to sleep. Instead, they meditate deeply,
  remaining semi-conscious. This trance lasts approximately three hours.
-->

TODO: Introductie

## Beschrijving

## Biografie

## Relaties

## Trivia
