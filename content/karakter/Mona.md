---
title: "Mona"
date: 2019-10-27T12:43:40+01:00
draft: false
tags:
- karakter
---

{{< figure src="" width="200px" >}}

**Ras:** Khoravar\
**Gender:** Vrouw\
**Leeftijd:** -\
**Associatie:** -

<!-- stat block: commoner -->

Mede-eigenaar van boomkwekerij in [Riviertraan](/locatie/riviertraan)

## Beschrijving

Tenger figuur, lang zwart haar met een streep grijs haar aan de rechter kant.

## Biografie

## Relaties

Vrouw van [Tom](/karakter/tom).

Moeder van [Donna](/karakter/lin).

## Trivia

- Veel liefde voor de natuur.
