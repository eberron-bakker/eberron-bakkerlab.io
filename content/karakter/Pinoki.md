---
title: "Pinoki"
date: 2019-10-27T01:46:33+02:00
draft: false
tags:
- karakter
---

{{< figure src="/img/Pinoki.jpg" width="200px" >}}

**Ras:** Gnoom\
**Gender:** Man\
**Leeftijd:** 56\
**Associatie:** -

<!-- stat block: commoner-->

Pinoki is de eigenaar van [De Eerlijke Nar in
Riviertraan](/locatie/riviertraan#de-eerlijke-nar).

## Beschrijving

## Biografie

## Relaties

## Trivia

Pinoki vindt het leuk om grappen uit te halen met zijn klanten.
