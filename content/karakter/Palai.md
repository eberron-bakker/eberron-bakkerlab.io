---
title: "Palai"
date: 2019-10-27T02:03:05+02:00
draft: false
tags:
- karakter
---

{{< figure src="/img/Palai.jpg" width="200px" >}}

**Ras:** Shifter (hondachtig)\
**Gender:** Man\
**Leeftijd:** 40\
**Associatie:** -

<!-- stat block: cult fanatic + shifter -->

Palai is een hondachtige shifter die het stamhoofd is van de [Stam
van de Schemermaan](/organisatie/stam-van-de-schemermaan).

<!-- Hij heeft het liefste weinig te maken heeft met de buitenwereld. -->

<!-- Palai is overgenomen door een *intellect devourer*. Toen zijn stam een verkenner
van Aundair tegenkwam, stemde hij in om mee te werken aan het plan. Zie
Aundair-plot. -->

## Beschrijving

## Biografie

## Relaties

## Trivia
