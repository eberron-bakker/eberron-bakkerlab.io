---
title: "Imhotep"
date: 2020-01-05T18:09:08+01:00
draft: false
tags:
- karakter
---

{{< figure src="/img/Imhotep.jpg" width="250px" >}}

**Ras:** Mens\
**Gender:** Man\
**Leeftijd:** -\
**Associatie:** [Wolkenwrekers](/organisatie/wolkenwrekers)

<!-- stat block: TODO -->

Imhotep is een piraat aan boord van het Vliegende Fortuin. Hij is een schipper
en krijgskracht.

## Beschrijving

## Biografie

## Relaties

Broer van [Neithotep](/karakter/neithotep).

## Trivia
