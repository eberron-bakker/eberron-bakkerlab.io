---
title: "Thubar"
date: 2019-10-27T12:48:40+01:00
draft: false
tags:
- karakter
---

{{< figure src="" width="200px" >}}

**Ras:** Half-orc\
**Gender:** Man\
**Leeftijd:** -\
**Associatie:** -

<!-- stat block: commoner -->

Thubar is de houtbewerker van [Riviertraan](/locatie/riviertraan).

## Beschrijving

Groot en sterk.

## Biografie

Thubar heeft in de Laatste Oorlog gevochten als huurling.

<!-- Zijn "cover" was het maken van meubels, maar hij voorzag tevens het leger en het
verzet van pijlen,bogen, schilden, knotsen, enz. -->

## Relaties

## Trivia
