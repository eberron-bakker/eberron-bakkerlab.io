---
title: "Hofnar"
date: 2019-12-15T15:37:44+01:00
draft: false
tags:
- karakter
---

{{< figure src="/img/Hofnar-lente.png" width="300px" >}}

**Ras:** Eladrin (opperfee)\
**Gender:** -\
**Leeftijd:** -\
**Associatie:** Feeënhof

<!-- stat block: TODO -->

Het is onduidelijk wat de Hofnar wil, maar het is duidelijk dat hij veel plezier
beleeft aan het grapjes uithalen met mensen. Het wereldbeeld van de Hofnar lijkt
een beetje nihilistisch: Niets is wat het lijkt, en alles is zoals het is. In de
werkelijkheid zoekt de Hofnar een hoger doel dan grapjes uithalen: Hij
symboliseert het geluk, en is aardsvijand van verdrietigheid.

<!--
Echter is de Hofnar manisch-depressief. Hij heeft buien van groot geluk, en
buien van diep verdried. Het is de taak van zijn volgelingen om hem op te
vrolijken wanneer hij verdrietig is. Wanneer de Hofnar vrolijk is, dan beloont
hij zijn volgelingen.

De Hofnar is een eladrin. Zijn seizoen geeft zijn humeur aan: Lente en zomer
zijn manisch, herfst en winter zijn depressief.
-->

## Beschrijving

## Biografie

## Relaties

## Trivia
