---
title: "Silvia"
date: 2020-01-05T18:11:00+01:00
draft: false
tags:
- karakter
---

{{< figure src="/img/Silvia.jpg" width="250px" >}}

**Ras:** Mens\
**Gender:** Vrouw\
**Leeftijd:** -\
**Associatie:** [Wolkenwrekers](/organisatie/wolkenwrekers)

<!-- stat block: TODO -->

Silvia is een piraat aan boord van het Vliegende Fortuin. Ze is hoofdzakelijk
verantwoordelijk voor het schipsonderhoud. Ze is ook verantwoordelijk voor
onderhoud van personeel in de vorm van eerste hulp.

## Beschrijving

## Biografie

## Relaties

## Trivia
