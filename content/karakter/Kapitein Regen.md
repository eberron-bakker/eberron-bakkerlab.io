---
title: "Kapitein Regen"
date: 2020-04-11T13:27:40+01:00
draft: false
tags:
- karakter
---

{{< figure src="/img/Regen.jpg" width="200px" >}}

**Ras:** Water-genasi\
**Gender:** -\
**Leeftijd:** -\
**Associatie:** Zeedraken

<!-- stat block: TODO -->

Regen is de kapitein van de Sirene, een schip van de Zeedraken van Prins Ryger
uit Vorstenpoort. Regen is zeer formeel en "vorstelijk".

## Beschrijving

## Biografie

## Relaties

## Trivia
