---
title: "Minister Moranna ir'Wynarn"
date: 2020-10-16T02:59:20+02:00
draft: false
tags:
- karakter
---

{{< figure src="/img/Moranna.jpg" width="350px" >}}

**Ras:** Mens\
**Gender:** Vrouw\
**Leeftijd:** -\
**Associatie:** Karrnath

<!--
**Attributes:** Agility d6, Smarts d10, Spirit d8, Strength d12, Vigor d6\
**Skills:** Academics d10, Athletics d4, Common Knowledge d8, Gambling d4,
Intimidation d10, Notice d10, Persuasion d12, Research d8, Riding d6,
Spellcasting d10, Stealth d6, Taunt d6\
**Pace:** 6; **Parry:** 2; **Toughness:** 7\
**Hindrances:** Ruthless, Ugly\
**Edges:** Aristocrat, Connections, Menacing\
**Special Abilities:**

- **Bite:** Str+d4.
- **Change Form:** As an action, a vampire can change into a bat with a Smarts
  roll at -2. Changing back into humanoid form requires a Smarts roll.
- **Charm:** Vampires can use the *puppet* power on those attracted to them
  using their Smarts as their arcane skill. They can cast and maintain the power
  indefinitely, but may only affect one target at a time.
- **Invulnerability:** Vampires can only be slain by sunlight or a stake through
  the heart (see those **Weaknesses**, below). They may be Shaken by other
  attacks, but never Wounded.
- **Mist:** Vampires have the ability to turn into mist. Doing so (or returning
  to human form) requires an action and a Smarts roll at -2.
- **Spells:** Moranna has 30 Power Points and can cast: *Bolt*, *darkness*,
  *drain power points*, *fear*, *lower trait*, *puppet*, *sloth*, *zombie*.
- **Sire:** Anyone slain by a vampire has a 50% chance of rising as a vampire in
  1d4 days.
- **Undead:** +2 Toughness; +2 to recover from being Shaken; no additional
  damage from Called Shots; ignores 1 point of Wound penalties; doesn't breathe;
  immune to disease and poison.
- **Weakness (Holy Symbol):** A character may keep a vampire at bay by
  displaying a holy symbol. A vampire who wants to directly attack the victim
  must bear her in an opposed Spirit roll.
- **Weakness (Holy Water):** A vampire sprinkled with holy water is Fatigued. If
  immersed, she combusts as if it were direct sunlight.
- **Weakness (Invitation Only):** Vampires cannot enter a private dwelling
  without being invited. They may enter public domains as they please.
- **Weakness (Stake Through the Heart):** A vampire hit with a Called Shot to
  the heart (-4) must make a Vigor roll versus the damage total. If successful,
  it takes damage normally. If it fails, it disintegrates to dust.
- **Weakness (Sunlight):** Vampires burn in sunlight. They take 2d4 damage per
  round until they are ash. Armor protects normally.

-->

Voormalig regent van Karrnath, minister van Buitenlandse Zaken.

## Beschrijving

## Biografie

## Relaties

## Trivia
