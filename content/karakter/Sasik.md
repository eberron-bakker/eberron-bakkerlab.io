---
title: "Koning-Consort Sasik d'Vadalis"
date: 2020-10-16T02:48:30+02:00
draft: false
tags:
- karakter
---

{{< figure src="/img/Sasik.jpg" width="350px" >}}

**Ras:** Mens\
**Gender:** Man\
**Leeftijd:** -\
**Associatie:** Aundair, Huis Vadalis

<!--
**Attributes:** Agility d8, Smarts d6, Spirit d8, Strength d6, Vigor d6\
**Skills:** Academics d6, Athletics d8, Common Knowledge d8, Fighting d6, Focus
d8, Notice d8, Persuasion d8, Riding d10+1, Shooting d6, Stealth d6\
**Pace:** 6; **Parry:** 6; **Toughness:** 5\
**Edges:** Aristocrat, Beast Master, Connections\
**Gear:** Wooden staff (Range 12/24/48, 2d6, Parry +1)\
**Special Abilities:**

- **Spells:** Sasik has 10 Power Points and knows the following powers: *Beast
  friend*, *boost trait* (animals only), *empathy* (animals only), *slumber*
  (animals only).
-->

Consort van Koningin [Aurala](/karakter/aurala), voormalige patriarch van Huis
Vadalis.

## Beschrijving

## Biografie

## Relaties

## Trivia
