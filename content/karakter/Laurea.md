---
title: "Laurea Valu-Nelo"
date: 2020-01-03T18:48:32+01:00
draft: false
tags:
- karakter
---

{{< figure src="/img/Laurea.jpg" width="350px" >}}

**Ras:** Goliath\
**Gender:** Vrouw\
**Leeftijd:** -\
**Associatie:** [Wolkenwrekers](/organisatie/wolkenwrekers)

**Attributes:** Agility d10, Smarts d4, Spirit d6, Strength d10, Vigor d8\
**Skills:** Athletics d10, Boating d4, Common Knowledge d4, Fighting d10,
Gambling d4, Intimidation d8, Language (Common) d6, Language (Giant) d8, Notice
d4, Performance d4, Persuasion d4, Stealth d4, Taunt d4\
**Pace:** 6; **Parry:** 8 (1); **Toughness:** 10 (2); **Size:** 1\
**Gear:** Hoe (Str+d6, Reach 1, +1 Parry), dagger (Range 3/6/12, Damage Str+d4),
leather jacket (+2), leather leggings (+2), flint and steel, hemp rope (20
yards), torch (one hour, 8-yard radius), slijpsteen\
**Hindrances:**

- **Big:** Goliaths subtract 2 from Trait rolls when using equipment that wasn't
  specifically designed for their size. Equipment costs double the listed price
  for goliaths. <!-- -2 -->
- **Driven (Minor):** Laurea wants to support her brother with gold to pay for his
  treatment.
- **Outsider (Minor):** Goliaths are strangers from a distant land. They
  subtract 2 from Persuasion rolls with people who do not trust goliaths. <!--
  -1 -->
- **Stubborn (Minor):** Laurea always wants her way and never admits when she's
  wrong.
- **Survival of the Fittest:** Goliaths come from a culture of survival. They
  live by a code of fitness, where those who cannot take care of themselves are
  often expelled. Most goliaths would rather die at the peak of their strength
  than while in slow decline. Goliaths who do not follow this code subtract 2
  from Persuasion rolls with orthodox goliaths. <!-- -2 -->
- **Wanted (Major):** Laurea is a wanted pirate.

**Edges:**

- **Mountain Born:** Goliaths are acclimated to high altitude. They receive a +4
  bonus to resist the cold. Cold damage is reduced by 4. <!-- +1 -->
- **Powerful Build:** Goliaths are large. Their size (and therefore Toughness)
  increases by 1, and they treat their Strength as one die type higher when
  determining **Encumbrance** and **Minimum Strength** to use armor, weapons,
  and equipment without a penalty.
- **Stone's Endurance:** Goliaths have tough skin, increasing their base
  Toughness by +1. <!-- +1 -->

Laurea is een goliath uit Stormhaven in Xen'drik, het continent van de reuzen.
Ze was een tuinder samen met haar familie totdat haar broertje ziek werd met een
ernstige spierziekte. Binnen goliath-gemeenschappen is het gewoonlijk dat de
sterken overleven en de zwakken afsterven, maar Laurea kon niet niks doen
terwijl haar broertje langzaam verging.

Behandeling was mogelijk in Sharn, maar bijzonder duur. Om de behandeling van
haar broertje te kunnen betalen werd Laurea piraat. Aanvankelijk werd ze
belachelijk gemaakt als tuinder, maar ze omhelsde de spot. Ze nam de schoffel
als haar wapen---geslepen tot een speer aan één eind, en bijzonder scherp staal
aan het andere eind.

Aanvankelijk was ze een piraat rond Stormhaven, maar er bleek snel meer geld te
verdienen in de Lhazaar Prinsdommen. Laurea kreeg een reputatie als meedogenloze
krijger, hoewel ze haar werk in principe voor een onbaatzuchtige reden doet.

## Beschrijving

## Biografie

## Relaties

- **Vagan Valu-Nelo** is het broertje van Laurea. Hij werkt als technicus voor
  Huis Cannith.

## Trivia

Laurea keert af en toe terug naar Sharn om goud te geven aan haar broertje.

Laurea heeft een hekel aan Huis Jorasco, die onredelijk hoge prijzen vraagt voor
hun heelkunde.
