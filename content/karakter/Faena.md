---
title: "Faena Grauwmorgen"
date: 2020-08-03T18:07:23+02:00
draft: false
tags:
- karakter
---

{{< figure src="/img/Faena.png" width="350px" >}}

**Ras:** Khoravar\
**Gender:** Vrouw\
**Leeftijd:** 180\
**Associatie:** Wachters van het Woud

<!--
**Attributes:** Agility d4-1, Smarts d10, Spirit d12, Strength d4-1, Vigor d6-1\
**Skills:** Academics d10, Athletics d4, Common Knowledge d8, Cosmology d8,
Druidism d12, Fighting d4, Notice d10, Persuasion d10, Riding d6, Science d8,
Shooting d8, Stealth d6, Survival d10\
**Pace:** 5; **Parry:** 5; **Toughness:** 6 (1)\
**Hindrances:** Elderly\
**Edges:** ---\
**Gear:** Staff (Range 12/24/48, 2d6, Parry +1), soft leather armor (+1)\
**Special Abilities:**

- **Low Light Vision:** Half-elves ignore penalties for Dim and Dark
  Illumination.
- **Spellcasting:** Faena has 25 Power Points and knows the following powers:
  *Banish*, *beast friend*, *burrow*, *detect/conceal arcana*, *divination*,
  *entangle*, *environmental protection*, *farsight*, *havoc*, *healing*,
  *light/darkness*, *mind link*, *shape change*, *slumber*, *summon ally*, and
  *teleport* (via trees).
-->

Faena is de rechterhand van Oalian, en voert het meeste werk uit in de regie van
de Wachters van het Woud. Ze is een oorlogsveteraan en een echte Eldeener. Door
haar oude leeftijd is ze niet zo sterk meer, maar nog steeds even wijs.

<!-- De staf van Faena is een tak van Oalian. Met de tak kan ze direct met
Oalian praten met *mind link*. -->

<!-- Faena vertrouwt Aundair *niet*. -->

## Beschrijving

## Biografie

## Relaties

## Trivia
