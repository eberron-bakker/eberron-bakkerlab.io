---
title: "Cédric d'Vadalis"
date: 2019-10-27T01:28:46+02:00
draft: false
tags:
- karakter
---

{{< figure src="/img/Cédric.png" width="200px" >}}

**Ras:** Mens\
**Gender:** Man\
**Leeftijd:** 40\
**Associatie:** [Huis Vadalis](/organisatie/huis-vadalis)

<!-- stat block: noble-->

Cédric d'Vadalis is de eigenaar van de [dierenfokkerij in
Riviertraan](/locatie/riviertraan#dierentuin).

## Beschrijving

## Biografie

## Relaties

Cédric is de man van [Tarlec](/karakter/tarlec).

<!-- Omdat Cédric zijn drakenmerk wil overdragen, heeft hij stiekem een relatie met
[Adrianne](/karakter/adrianne), een veel jongere boerenknecht. -->

## Trivia
