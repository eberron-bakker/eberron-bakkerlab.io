---
title: "k'Lung"
date: 2020-01-03T23:48:49+01:00
draft: false
tags:
- karakter
---

{{< figure src="/img/k'Lung.png" width="300px" >}}

**Ras:** Grung\
**Gender:** Man\
**Leeftijd:** -\
**Associatie:** [Wolkenwrekers](/organisatie/wolkenwrekers)

**Attributes:** Agility d8, Smarts d8, Spirit d6, Strength d6, Vigor d4\
**Skills:** Athletics d8, Common Knowledge d4, Fighting d10, Gambling d6,
Language (Common) d6, Language (Grung) d8, Notice d6, Persuasion d6, Stealth d4,
Taunt d8\
**Pace:** 5; **Running Die:** d4; **Parry:** 10 (3); **Toughness:** 4 (1);
**Size:** -1\
**Gear:** Rapier (Str+d4, Parry +1), dagger (Range 3/6/12, Damage Str+d4), cloth
jacket (+1), cloth leggings (+1), buckler (+2 Parry)\
**Hindrances:**

- **Arrogant (Major):** k'Lung doesn't think he's the best---he knows he is. He
  flaunts his swordsmanship every chance he gets. He's the kind of fighter who
  disarms an opponent in a duel just to pick the sword up and hand it back with
  a smirk.
- **Outsider (Minor):** Grungs are strangers from a distant land. They subtract
  2 from Persuasion rolls with people who do not trust grungs. <!-- -1 -->
- **Reduced Pace:** Decrease the character's Pace by 1 and their running die one
  die type. <!-- -1 -->
- **Size -1:** Grungs average about three feet tall, reducing their Size (and
  therefore Toughness) by 1. <!-- -1 -->
- **Wanted (Major):** k'Lung is a wanted pirate.
- **Water Dependency:** Grungs must immerse themselves in water one hour out of
  every 24 or become automatically Fatigued each day until they Incapacitated.
  The day after Incapacitation from dehydration, they perish. Each hour spent in
  water restores one level of Fatigue. <!-- -2 -->

**Edges:**

- **Amphibious:** Grungs can breathe underwater. <!-- +1 -->
- **Extraction:** When moving away from adjacent foes, one of them (player's
  choice) doesn't get his free Fighting attack.
- **Natural Climber:** Grungs can climb vertical surfaces as though walking
  normally. <!-- +1 -->
- **Poison Immunity:** Grungs are immune to poison. <!-- +1 -->
- **Poisonous Skin:** Any non-grung that comes into direct contact with a
  grung's skin must roll Vigor or suffer the effects of Mild Poison. On a
  Critical Failure, or if the victim comes into contact with the poison again
  within a minute, she suffers an additional effect for 2d6 rounds dependent on
  the grung's skin color. <!-- +2 -->
  + **Gold:** The victim regards the grung as a friend and can speak Grung. The
    grung gains +1 to Intimidation, Persuasion, Performance, or Taunt rolls
    against the victim.
- **Provoke:** Once per turn, when your hero uses Taunt for a Test and gets a
  raise, he may Provoke the foe. In addition to all the usual effects of the
  success and raise, the enemy suffers a -2 penalty to affect any other target
  besides the one who provoked her. This stacks with Distracted but not further
  instances of Provoked. Provoke lasts until a Joker is drawn, someone else
  Provokes the target, or the encounter ends. Provoke can affect multiple
  targets, and may be combined with Rabble Rouser.
- **Leaper:** Grungs can jump twice as far as listed under **Movement**
  (see *Savage Worlds* core rules). In addition, grungs add +4 to damage when
  leaping as part of a Wild Attack instead of the usual +2 (unless in a closed
  or confined space where they cannot leap horizontally or vertically---GM's
  call). <!-- +2 -->

k'Lung is een intelligente grung van het eiland Krag in de Lhazaar Prinsdommen.
Hij was een van de stamhoofden van een grung-gemeenschap op het eiland, maar
werd verstoten uit zijn stam nadat hij weigerde om mee te gaan in een nieuwe
religieuze stroming; het Bloed van Vol. k'Lung voelde dat hem onrecht was
aangedaan, en zwoor om beter te zijn dan zijn voormalige stam.

k'Lung kwam aan bij de piraatnederzetting "Poort Krez" op het eiland, en werd
piraat. De arrogantie in zijn nieuwe beroep steeg een beetje naar zijn hoofd,
maar hij blijft desondanks een eerlijke tegenstander.

k'Lung is tegenwoordig eerste stuurman van het Vliegende Fortuin.

## Beschrijving

## Biografie

## Relaties

## Trivia
