---
title: "Swift"
date: 2019-10-27T12:55:22+01:00
draft: false
tags:
- karakter
---

{{< figure src="" width="200px" >}}

**Ras:** Shifter (panter)\
**Gender:** Man\
**Leeftijd:** -\
**Associatie:** -

<!-- stat block: TODO -->

Ingewijdene van de [Wachters van het Woud](/organisatie/wachters-van-het-woud).

## Beschrijving

Donkere huid met pantertrekjes.

## Biografie

## Relaties

Beste vriend van [Lin](/karakter/lin). Staan in
[Riviertraan](/locatie/riviertraan) bekend als onafscheidelijke tweeling.

Swift heeft z'n eigen gezin. Zijns levenspartner is op 998-01-01 twee maanden
zwanger.

## Trivia
