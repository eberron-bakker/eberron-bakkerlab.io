---
title: "Prins Kor ir'Wynarn"
date: 2020-10-16T02:52:23+02:00
draft: false
tags:
- karakter
---

{{< figure src="/img/Kor.jpg" width="350px" >}}

**Ras:** Mens\
**Gender:** Man\
**Leeftijd:** -\
**Associatie:** Breland

<!--
**Attributes:** Agility d8, Smarts d8, Spirit d10, Strength d8, Vigor d8\
**Skills:** Academics d6, Athletics d6, Common Knowledge d6, Fighting d8,
Intimidation d6, Notice d10, Persuasion d8, Riding d6, Shooting d6, Stealth d6\
**Pace:** 6; **Parry:** 6; **Toughness:** 10 (4)\
**Edges:** Alertness, Aristocrat, Soldier\
**Gear:** Sword (Str+d8), plate armor (+4)
-->

Broer van Koning Boranel, hoofdadviseur, commandant van de Citadel.

## Beschrijving

## Biografie

## Relaties

## Trivia
