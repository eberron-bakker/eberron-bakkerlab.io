---
title: "Elion"
date: 2020-05-23T12:28:36+01:00
draft: false
tags:
- karakter
---

{{< figure src="" width="200px" >}}

**Ras:** Elf\
**Gender:** Vrouw\
**Leeftijd:** -\
**Associatie:** Wachters van het Woud

<!-- stat block: TODO -->

Elion was een leider in de onafhankelijkheidsoorlog van Eldeen. Tegenwoordig is
ze vaak te vinden bij het Houten Zwaard in Groenhart.

## Beschrijving

## Biografie

## Relaties

## Trivia
