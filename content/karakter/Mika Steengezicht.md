---
title: "Mika Steengezicht"
date: 2020-01-04T21:40:13+01:00
draft: false
tags:
- karakter
---

{{< figure src="" width="200px" >}}

**Ras:** Dwerg\
**Gender:** Vrouw\
**Leeftijd:** -\
**Associatie:** [Wolkenwrekers](/organisatie/wolkenwrekers)

<!-- stat block: TODO -->

Prinses Mika Steengezicht is de leider van de
[Wolkenwrekers](/organisatie/wolkenwrekers) vanuit Poort Krez.

## Beschrijving

## Biografie

## Relaties

## Trivia
