---
title: "Leselor"
date: 2020-05-02T13:14:40+01:00
draft: false
tags:
- karakter
---

{{< figure src="/img/Leselor.jpg" width="200px" >}}

**Ras:** Khoravar\
**Gender:** Vrouw\
**Leeftijd:** -\
**Associatie:** -

<!--
**Attributes:** Agility d8, Smarts d8, Spirit d8, Strength d6, Vigor d6\
**Skills:** Academics d8, Athletics d6, Common Knowledge d8, Fighting d6, Healing d4,
Intimidation d6, Notice d10, Persuasion d10, Riding d6, Shooting d8,
Spellcasting d10, Stealth d10, Survival d4, Taunt d6, Thievery d6\
**Pace:** 6; **Parry:** 5; **Toughness:** 8 (3)\
**Edges:** Combat Reflexes (+2 to recover from Shaken or Stunned)\ 
**Gear:** Wand (Range 6/12/24, Damage 2d6), dagger (Range 3/6/12, Damage
Str+d4), ring mail armor (+3, chest), shiftweave\
**Special Abilities:**

- **Low Light Vision:** Half-elves ignore penalties for Dim and Dark
  Illumination.
- **Spellcasting:** Leselor has 15 Power Points and knows the following powers:
  *Blind*, *detect/conceal arcana*, *disguise*, *dispel*, *illusion*,
  *lock/unlock*, *invisibility*, *scrying*.
-->

TODO: Introductie

<!--
Leselor is een agent voor de Koninklijke Ogen van Aundair.

Officieel Leselor ir'Qarin.
-->

## Beschrijving

## Biografie

## Relaties

## Trivia

<!--
Raven familiar

**Attributes:** Agility d8, Smarts d6 (A), Spirit d6, Strength d4-3, Vigor d6\
**Skills:** Athletics d6, Fighting d4, Notice d10+2, Stealth d8\
**Pace:** 3; **Parry:** 4; **Toughness:** 1
**Edges:** Alertness (+2 to sense-based Notice rolls)\
**Special Abilities:**

- **Bite/Claws:** Str.
- **Flight:** Ravens fly at a Pace of 36".
- **Low Light Vision:** Ravens ignore penalties for Dim and Dark Illumination.
- **Size -4 (Tiny)**
- -->
