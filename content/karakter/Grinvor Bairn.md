---
title: "Grinvor Bairn"
date: 2019-10-28T09:46:06+01:00
draft: false
slug: grinvor
tags:
- karakter
---

{{< figure src="/img/Grinvor.jpg" width="250px" >}}

**Ras:** Halfling\
**Gender:** Man\
**Leeftijd:** -\
**Associatie:** -

<!-- stat block: bard player -->

Grinvor Bairn is een wereldreiziger en een bard.

## Beschrijving

## Biografie

Grinvor komt uit de Talenta Velden. In Therendor 993 begon hij met een
wereldreis.

Als eerst ging Grinvor naar het zuiden. Hij is door de Messenwoestijn gegaan
richting Valenar. Hoewel er oorlogsbendes doolden door Valenar is Grinvor altijd
veilig gebleven: Vriend of vijand, iedereen vindt een muzikant leuk. In [Taer
Valaestas](/locatie/taer-valaestas) heeft Grinvor geleerd hoe hij een zwaard
moet gebruiken.

In Lharvion 993 was Grinvor in [Metrol](/locatie/metrol) in Cyre, een
metropolis van cultuur. Hij is hier vier maanden gebleven tot Aryth 993.

Met de trein is Grinvor naar het noorden gegaan, op tocht door Karrnath. In
[Vedykar](/locatie/vedykar) ging hij van de trein af, en te voet verder ten
zuiden van het Nachtwoud. Daar ging hij naar [Karrlakton](/locatie/karrlakton),
de tweede grootste stad van Karrnath.

Op 20 Olarune 994---de Dag van de Rouw---kon Grinvor zien hoe een ziekelijke
grijze mist zich over heel Cyre ontfermde. De dagen daarna trokken veel
vluchtelingen naar Karrlakton, en Grinvor besloot om te vertrekken.

Lang is Grinvor niet in Karrnath gebleven. In Therendor 994 kwam hij aan in
Korth, en vanuit Korth heeft hij de boot genomen naar
[Vlammenburcht](/locatie/vlammenburcht).

Vanuit Vlammenburcht is Grinvor op lange doorreis naar [Sharn](/locatie/sharn)
gegaan. Via [Vathirond](/locatie/vathirond),
[Starilaskur](/locatie/starilaskur), en [Wroat](/locatie/wroat) kwam hij een
jaar later, in Eyre 995, in Sharn aan.

Sharn is een megastad. Je kan hier een jaar ronddolen, en je hebt nog niet alles
gezien. Grinvor heeft hier twee jaar door de stad gerezen, tot Lharvion 997.
Daarna vertrok hij naar het noorden, om de vijfde natie van de Vijf Naties te
bezoeken.

In Barrakas 997 kwam hij aan in [Passage](/locatie/passage), een grote stad aan
Meer Galifar. Hij heeft hier een bootje gehuurd om een pleziervaart over het
meer te doen, en in Sypheros 997 kwam hij aan in [Varna](/locatie/varna), een
kleine stad in Eldeen.

Vanuit Varna is hij naar het noorden getrokken, waarna hij op 1 Zarantyr 998
aankwam in [Riviertraan](/locatie/riviertraan).

## Relaties

Grinvor heeft een raptor genaamd Dribbel met wie hij samen reist. Dribbel is
gekocht in [Varna](/locatie/varna) van [Huis
Vadalis](/organisatie/huis-vadalis).

## Trivia
