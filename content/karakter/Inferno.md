---
title: "Inferno"
date: 2020-01-04T16:17:08+01:00
draft: false
tags:
- karakter
---

{{< figure src="/img/Inferno.jpg" width="550px" >}}

**Ras:** Shifter (tijgerachtig)\
**Gender:** Man\
**Leeftijd:** -\
**Associatie:** [Wolkenwrekers](/organisatie/wolkenwrekers)

**Attributes:** Agility d8, Smarts d8, Spirit d6, Strength d6, Vigor d6\
**Skills:** Athletics d8, Boating d6, Common Knowledge d4, Fighting d8, Language
(Common) d8, Notice d6, Persuasion d4, Spellcasting d8, Stealth d4\
**Pace:** 6; **Parry:** 6; **Toughness:** 7 (2); **Size:** 0\
**Gear:** Sword (Str+d6), dagger (Range 3/6/12, Damage Str+d4), wand, leather
jacket (+2), leather leggings (+2), flint and steel, hemp rope (20 yards)\
**Hindrances:**

- **Impulsive (Major):** The daredevil almost always leaps before he looks. He
  rarely thinks things through before taking action.
- **Outsider (Minor):** Due to their lycanthropic ancestry, many people are
  suspicious or even fearful of shifters. Shifters subtract 2 from Persuasion
  rolls with people who do not trust them. <!-- -1 -->
- **Wanted (Major):** Inferno is a wanted pirate.

**Edges:**

- **Arcane Background (Magic):** Inferno has 15 Power Points and knows the
  following spells:
  + **Firebolt (bolt):** 1 Power Point. Range 16. 2d6 ranged attack.
  + **Dragon Breath (burst):** 2 Power Points. Cone-shaped attack for 2d6
    damage.
  + **Fiery Weapon (smite):** 2 Power Points. Range 8. 5 rounds. Increase a
    weapon's damage by +2 (+4 with a raise).
- **Bite:** Shifters have fangs that cause Strength+d4 damage. See **Natural
  Weapons** in the *Savage Worlds* core rules. <!-- +1 -->
- **Low Light Vision:** Shifters ignore penalties for Dim and Dark Illumination.
  <!-- +1 -->
- **Shifting:** Shifters can tap into their lycanthropic heritage. Shifting is a
  free action and lasts 10 rounds or until dismissed as a free action. While in
  this animal-like form, the shifter can ignore 1 Wound penalty. When the
  shifting ends, the shifter must wait 4 hours before shifting again or suffer
  Fatigue. The type of shifting is determined by their subrace. <!-- +2 -->
  + **Longtooth:** The face of a longtooth shifter distorts, growing a muzzle
    full of sharp teeth that cause Str+d6 damage. Additionally, the shifter may
    ignore 2 points of Multi-Action penalties on each turn if he uses one action
    to bite.

Inferno komt uit Vorstenpoort, de onofficiële hoofdstad van de Lhazaar
Prinsdommen. Hij was gemaakt voor het leven als piraat. Van jongs af aan is
Inferno al enthousiast over schepen en de zee. Maar zijn favoriete zee is niet
de Lhazaarzee, maar de vuurzee: Inferno heeft een ongewoon sterke liefde voor
het element vuur.

Stafslingers zijn een relatief nieuw concept van de laatste decennia, maar
werden razendpopulair in Vorstenpoort. Inferno kwam goedkoop in het bezit van
een toverstaf, en wist na veel oefening vuur te creëren.

## Beschrijving

## Biografie

## Relaties

## Trivia

Inferno heeft een kort lontje.
