---
title: "Bel'Ataka"
date: 2020-03-12T22:06:12Z
draft: false
tags:
- karakter
---

{{< figure src="/img/Bel'Ataka.png" width="200px" >}}

**Ras:** Elf (Tairnadal)\
**Gender:** Vrouw\
**Leeftijd:** 3.000+\
**Associatie:** Feeënhof

<!-- stat block: Gladiator (14 AC, two attacks, 1d6+4) -->

Bel'Ataka is een voorouder van de Tairnadal. Lang geleden was ze de generaal van
een leger in Aerenal. Samen met twintig elven verdedigde ze de hoofdstad tegen
een aanval van drie draken. De elven reden op griffioenen.

<!--
Hoe belandde Bel'Ataka in Thelanis? Ze aanbad Leylandra als opperfee. Door de
heldendaden van Bel'Ataka mocht ze haar strijdlust voortzetten in Thelanis.
 -->

## Beschrijving

## Biografie

## Relaties

## Trivia
