---
title: "Eenpoot"
date: 2019-10-27T01:41:23+02:00
draft: false
tags:
- karakter
---

**Ras:** Shifter (beerachtig)\
**Gender:** Man\
**Leeftijd:** 35\
**Associatie:** Wachters van het Woud

<!--
**Attributes:** Agility d6, Smarts d6, Spirit d10, Strength d6, Vigor d8\
**Skills:** Athletics d6, Common Knowledge d6, Druidism d8, Fighting d6,
Notice d6, Persuasion d6, Shooting d6, Stealth d6\
**Pace:** 6; **Parry:** 6 (1); **Toughness:** 6 (1)\
**Edges:** ---\
**Gear:** Staff (Str+d4, Parry +1, Reach 1, two hands; Range 12/24/48, Damage
2d6), leather armor (+1)\
**Special Abilities:**

- **Claw:** Str+d4.
- **Low Light Vision:** Ignore penalties for Dim and Dark Illumination.
- **Shifting:** Shifting is a free action and lasts 10 rounds or until dismissed
  as a free action. While in this animal-like form, the shifter can ignore 1
  Wound penalty, and gains Armor +6. When the shifting ends, the shifter must
  wait 4 hours before shifting again or suffer Fatigue. 
- **Spellcasting:** 20 Power Points, can cast *beast friend*, *detect/conceal
  arcana*, *entangle*, *havoc*, *healing*, and *shape change*.
-->

Eenpoot is een beerachtige shifter die dient als druïde van de Wachters van het
Woud in [Riviertraan](/locatie/riviertraan). Als druïde heeft hij een rol
vergelijkbaar met een priester. Hij helpt de inwoners met het volgen van de
druïdische religie, en heeft een netwerk van dichtbije druïdes waarmee hij eens
per seizoen een ontmoeting heeft. Soms reist hij naar Groenhart voor een
audiëntie met Oalian, of voor een *groot conclaaf* van alle Wachters.

Eenpoot gebruikt zijn woonkamer voor ontmoetingen. Voor grote ontmoetingen
gebruikt hij het Gebroken Glas of een veld naast de herberg.

Dieper in het [Rivierwoud](/locatie/rivierwoud) bevindt zich een *gaard* waar
druïdische rituelen plaatsvinden. De gaard wordt gedeeld met nabije dorpen.

## Beschrijving

## Biografie

## Relaties

## Trivia
