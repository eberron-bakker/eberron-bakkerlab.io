---
title: "Zarud"
date: 2020-10-16T02:58:11+02:00
draft: false
tags:
- karakter
---

{{< figure src="/img/Zarud.png" width="350px" >}}

**Ras:** Half-ork\
**Gender:** Man\
**Leeftijd:** -\
**Associatie:** Droaam

<!--
**Attributes:** Agility d6, Smarts d10, Spirit d6, Strength d4, Vigor d4\
**Skills:** Academics d6, Athletics d4, Common Knowledge d6, Fighting d4, Notice
d8, Persuasion d6, Research d8, Stealth d6, Thievery d6\
**Pace:** 6; **Parry:** 4; **Toughness:** 5 (1)\
**Edges:** Alertness, Investigator\
**Gear:** Light armor (+1), sword (Str+d4)\
**Special Abilities:**

- **Low Light Vision:** Half-orcs ignore penalties for Dim and Dark
  Illumination.
-->

TODO: Introductie

## Beschrijving

## Biografie

## Relaties

## Trivia
