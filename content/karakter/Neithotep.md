---
title: "Neithotep"
date: 2020-01-05T18:19:32+01:00
draft: false
tags:
- karakter
---

{{< figure src="/img/Neithotep.jpg" width="250px" >}}

**Ras:** Mens\
**Gender:** Vrouw\
**Leeftijd:** -\
**Associatie:** [Wolkenwrekers](/organisatie/wolkenwrekers)

<!-- stat block: TODO -->

Neithotep is een piraat aan boord van het Vliegende Fortuin. Ze is een schipper
en krijgskracht.

## Beschrijving

## Biografie

## Relaties

Zus van [Imhotep](/karakter/imhotep).

## Trivia
