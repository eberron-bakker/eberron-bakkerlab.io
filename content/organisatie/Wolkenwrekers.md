---
title: "Wolkenwrekers"
date: 2020-01-04T21:39:06+01:00
draft: false
tags:
- organisatie 
---

**Leider:** [Prinses Mika Steengezicht](/karakter/mika-steengezicht)\
**Theater:** Lhazaar Prinsdommen

Het Wolkenwreker-prinsdom is een groep piraten onder leiding van Prinses Mika
Steengezicht. Ze opereren vanuit Poort Krez in het zuiden van de Lhazaar
Prinsdommen, en staan bekend als de minst vriendelijke piraten van de
Prinsdommen. De Wolkenwrekers hebben toegang to snelle schepen die makkelijk
vrachtschepen kunnen inhalen en invallen.

Eén van de schepen van de Wolkenwrekers is het Vliegende Fortuin.

## Geschiedenis

## Locaties

## Relaties

## Leden

Bemanning van het Vliegende Fortuin:

- [Kapitein Fernando](/karakter/kapitein-fernando)
- [k'Lung](/karakter/klung)
- [Inferno](/karakter/inferno)
- [Laurea "Spotschoffel" Valu-Nelo](/karakter/laurea)
- [Droop](/karakter/droop)
- [Imhotep](/karakter/imhotep)
- [Neithotep](/karakter/neithotep)
- [Silvia](/karakter/silvia)

### Pirate stats

**Attributes:** Agility d6, Smarts d4, Spirit d6, Strength d6, Vigor d6\
**Skills:** Athletics d6, Boating d8, Common Knowledge d4, Fighting d6, Gambling
d6, Intimidation d4, Notice d6, Persuasion d4, Shooting d6, Stealth d4, Taunt
d6, Thievery d6+1\
**Pace:** 6; **Parry:** 5; **Toughness:** 6 (1)\
**Hindrances:** Wanted (Major)\
**Edges:** Thief (+1 to Athletics to climb and Stealth in urban environments)\
**Gear:** Leather armor (+1), Scimitar (Str+d8)

Some pirates have Repair d6, Healing d6, and/or Survival d6.

## Trivia
