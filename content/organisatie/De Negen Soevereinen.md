---
title: "De Negen Soevereinen"
date: 2019-12-01T15:54:48+01:00
draft: false
tags:
- organisatie 
---

**Leider:** -\
**Theater:** Khorvaire


## Geschiedenis

## Locaties

## Relaties

## Trivia

De Negen Soevereinen kennen twee kerndoctrines:

- Doctrine van universele soevereiniteit: *"Als de wereld, zo de goden. Als de
  goden, zo de wereld"*.

- Doctrine van de goddelijke eenheid: *"De Negen Soevereinen zijn één naam die
  met één stem spreken. De goden zijn de letters van die naam en de geluiden van
  die stem"*.
