---
title: "Eldeen-wolven"
date: 2019-10-27T02:08:33+02:00
draft: false
tags:
- organisatie 
---

**Leider:** [Lifra](/karakter/lifra)\
**Theater:** [Riverwoud](/locatie/rivierwoud) en west-Aundair.

De Eldeen-wolven zijn een groep fanatiekelingen die een hekel hebben aan
beschaving. Ze horen bij de sekte van de *asgebondenen*, maar richten zich op
Aundair aan de andere kant van de rivier. De Eldeen-wolven plunderen vaak dorpen
in Aundair, of vallen reizigers aan die over de wegen lopen. Ze laten de dorpen
in Eldeen vooralsnog met rust.

De Eldeen-wolven zijn mensen en shifters die op reuzenwolven rijden.

## Geschiedenis

## Locaties

## Relaties

## Trivia
