---
title: "Stam van de Schemermaan"
date: 2019-10-27T01:59:09+02:00
draft: false
tags:
- organisatie 
---

**Leider:** [Palai](/karakter/palai)\
**Theater:** -

De Stam van de Schemermaan is een stam van shifters in het
[Rivierwoud](/locatie/rivierwoud).

<!-- De stam vertrouwt buitenstaanders niet. Als een buitenstaander zilver of een
vlam bij zich draagt, dan zal de stam vechten of vluchten. De stam heeft een
hekel aan de Kerk van de Zilveren Vlam, die meer dan honderd jaar geleden een
kruistocht voerde tegen de shifters. -->

## Geschiedenis

Meer dan een eeuw geleden was Schemermaan een dorp met een shiftergemeenschap
ten noorden van Riviertraan. Door de kruistocht is het dorp vernietigd, en waren
de shifters onder dwang ontheemd. De gemeenschap is nooit echt hersteld.

<!-- De stam kreeg ondersteuning en verdediging van de Poortwachters, op de
voorwaarde dat de stam een oeroude eed af zou leggen dat zij een van de poorten
die Eberron afschermt van Khyber, de Wisselpoort, zouden handhaven. -->

## Locaties

De stam is enkel aanwezig in (of rondom) het Rivierwoud.

## Relaties

## Trivia
