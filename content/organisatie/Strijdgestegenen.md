---
title: "Strijdgestegenen"
date: 2020-04-05T10:58:06+01:00
draft: false
tags:
- organisatie 
---

**Leider:** Muiter\
**Theater:** Aundair, Eldeen, [Thelanis](/locatie/thelanis)

De strijdgestegenen zijn een groep van voormalige strijdgesmedenen die voor
Aundair in de oorlog gevochten hebben. Ze verlietten het leger van Aundair nadat
ze een bewustheid ontwikkelden over hun slavernij als oorlogmachines.

## Geschiedenis

Deze groep van voormalige strijdgesmedenen vocht voor Aundair in Eldeen. Ze
verlietten het leger, en bouwden hun eigen strijdeenheid. De strijdgestegenen
hadden twee voornamelijke doelen: Het bevrijden van strijdgesmedenen van de
tirannie van de Vijf Naties, en het vermoeilijken van Aundairs operaties in
Eldeen.

De voornamelijkste middelen van de strijdgestegenen waren infiltratie, sabotage,
en strategische aanvallen.

De strijdgestegenen gebruikten het Rivierwoud als hun basis. Ze hadden een
informeel akkoord van alliantie met de Wachters van het Woud, maar hadden geen
actieve onderlinge communicatie.

Op een dag verdwenen alle strijdgestegenen. Iedereen behalve Bast; die was
bevoorrading aan het halen. Ze verlietten Eberron voor Thelanis, het Feeënhof.

Muiter was en is een volgeling van de fee [Leylandra](/karakter/leylandra), maar
was hier niet open over. Leylandra is de opperfee van de oorlog. Op de Rouwdag
trok Leylandra de strijdgestegenen naar haar hof in Thelanis, ter bescherming
tegen de Rouw. Leylandra wist echter niet dat de Rouw zou stoppen in Cyre.

Omdat Leylandra niet meer contacten in Eberron heeft, en omdat tijd raar werkt
in Thelanis, wist Leylandra niet dat Eberron nog bestaat. Voor zover de
strijdgestegenen bewust waren was Aundair---samen met de rest van
Eberron---vernietigd.

## Locaties

## Relaties

## Leden

- Muiter --- leider
- [Bast](/karakter/bast) --- schermutselaar
- Krik --- ingenieur
- Signaal --- saboteur
- Zang --- infiltrant

## Trivia
