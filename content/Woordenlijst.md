---
title: "Woordenlijst"
date: 2019-10-30T20:57:01+01:00
draft: false
tags:
- woordenlijst
---

|           Engels           |          Nederlands          | Commentaar |
| :------------------------: | :--------------------------: | :--------: |
|      Arcane Congress       |       Magisch Congres        |            |
|          Archfey           |           Opperfee           |            |
|          Ashbound          |         Asgebondenen         |            |
|     Aundairian Scroll      |       Aundairse Spreuk       |            |
|            Bard            |          Troubadour          |            |
|         Blink dog          |         Knipperhond          |            |
|        Blood of Vol        |        Bloed van Vol         |            |
|         Changeling         |   Dubbelganger/wisselkind    |            |
|     Children of Winter     |     Kinderen des Winters     |            |
| Church of the Silver Flame |  Kerk van de Zilveren Vlam   |            |
|           Cleric           |         Geestelijke          |            |
|          Conclave          |           Conclaaf           |            |
|          Dark Six          |         Duistere Zes         |            |
|      Day of Mourning       |           Rouwdag            |            |
|        Demon Wastes        |       Demoonwoestenij        |            |
|         Dragonmark         |          Drakenmerk          |            |
|        Dragonshard         |         Drakenscherf         |            |
|           Druid            |            Druïde            |            |
|          Duskwood          |         Schemerwoud          |            |
|           Dwarf            |            Dwerg             |            |
|       Eldeen Reaches       |            Eldeen            |            |
|   Expeditious messenger    |     Vlotte boodschapper      |            |
|         Fairhaven          |         Schoonhaven          |            |
|            Fey             |             Fee              |            |
|    Feywild/Faerie Court    |           Feeënhof           |            |
|        Gatekeepers         |        Poortwachters         |            |
|      Gatekeeper seal       |     Poortwachterbarrière     |            |
|           Gnome            |            Gnoom             |            |
|          Graywall          |          Grauwmuur           |            |
|         Greenheart         |          Groenhart           |            |
|        Greensingers        |         Groenzangers         |            |
|           Grove            |            Gaard             |            |
|           Human            |             Mens             |            |
|    Korranberg Chronicle    |    Kroniek van Korranberg    |            |
|   Lhazaar Principalities   |     Lhazaar Prinsdommen      |            |
|         Magewright         |         Magiewerker          |            |
|          Merfolk           |    Zeemeermin/zeemeerman     |            |
|         Mournland          |           Rouwland           |            |
|         Mror Holds         |       Mror Eigendommen       |            |
|         Newthrone          |          Nieuwtroon          |            |
|       Path of Light        |        Pad der Licht         |            |
|           Ranger           |            Doler             |            |
|          Redleaf           |           Roodblad           |            |
|         Regalport          |         Vorstenpoort         |            |
|         Riverweep          |         Riviertraan          |            |
|         Riverwood          |          Rivierwoud          |            |
|           Rogue            |            Schurk            |            |
|   Royal Eyes of Aundair    | Koninklijke Ogen van Aundair |            |
|       Shadow Marches       |       Schaduwmoerassen       |            |
|       Sovereign Host       |      Negen Soevereinen       |            |
|         Stormreach         |          Stormhaven          |            |
|        Summer Court        |           Zomerhof           |            |
|       Talenta Plains       |        Talenta Velden        |            |
|        The Devourer        |        De Verslinder         |            |
|          The Fury          |           De Woede           |            |
|         The Keeper         |          De Houder           |            |
|        The Last War        |      De Laatste Oorlog       |            |
|        The Mockery         |        De Bespotting         |            |
|        The Mourning        |           De Rouw            |            |
|         The Shadow         |          De Schaduw          |            |
|        The Traveler        |         De Reiziger          |            |
|         Thronehold         |         Troonburcht          |            |
|       Towering Wood        |        Torenhoge Woud        |            |
|           Treant           |             Ent              |            |
|    Treaty of Thronehold    |   Verdrag van Troonburcht    |            |
|       Undying Court        |      Onsterfelijke Hof       |            |
|            Wand            |    Toverstaf/toverstokje     |            |
|        Wandslinger         |         Stafslinger          |            |
|    Wardens of the Wood     |    Wachters van het Woud     |            |
|         Warforged          |        Strijdgesmeden        |            |
|        Winter Court        |          Winterhof           |            |
